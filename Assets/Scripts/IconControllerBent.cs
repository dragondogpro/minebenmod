using System;
using InterfacesBent;
using ServerBent;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class IconControllerBent : MonoBehaviour
{
    public string devicePathBent;
    public string iconPathBent;
    public string iconNameBent;
    public string urlBent;
    public Texture2D textureBent;
    public GameObject TextureGameObjectBent;

    public void OnBecameVisible()
    {
        if (textureBent == null)
        {

            var firebaseStorageControllerBentBent = new FirebaseStorageControllerBent();
            firebaseStorageControllerBentBent.FirebaseLinkBent(iconPathBent, linkBent =>
                SetTextureOnCardBent(linkBent.ToString()));
        }
        else
        {
            TextureGameObjectBent.GetComponent<RawImage>().enabled = true;
        }
    }

    private void OnBecameInvisible()
    {
        TextureGameObjectBent.GetComponent<RawImage>().enabled = false;
    }

    private async void SetTextureOnCardBent(string urlBent)
    {
        this.urlBent = urlBent;
        var downloadTextureUniTaskBent = new DownloadTextureUniTaskBent();
        
        textureBent =
            await downloadTextureUniTaskBent.GetTextureBent(devicePathBent, iconPathBent, iconNameBent, urlBent);
        
        if (iconPathBent.Split(IKeyStorageHelperBent.SlashSeparatorBent)[0] ==
            ICategoryTypeHelperBent.SkinsCategoryNameBent.ToLower())
            gameObject.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).GetComponent<RawImage>()
                .texture = textureBent;
        else
            gameObject.transform.GetChild(0).transform.GetChild(0).GetComponent<RawImage>().texture = textureBent;

        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        gameObject.transform.GetChild(1).gameObject.SetActive(false);
        gameObject.transform.GetChild(1).GetComponent<Animator>().enabled = false;
        TextureGameObjectBent.GetComponent<RawImage>().enabled = true;
        gameObject.GetComponent<Button>().enabled = true;
    }
}
