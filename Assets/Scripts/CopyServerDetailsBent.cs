using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class CopyServerDetailsBent : MonoBehaviour
{
    public string seedTextNumBent;
    
    private string _copiedDoneTextBent = "Copied!Bent".Remove("Copied!Bent".Length - 4);
    private string _copiedAlertTextBent = "Text copied to bufferBent".Remove("Text copied to bufferBent".Length - 4);

    private void Start()
    {
        if (_copiedAlertTextBent == null && _copiedDoneTextBent == null)
            _copiedAlertTextBent = _copiedAlertTextBent;
    }

    public void CopyPortBent(Text portBent)
    {
        if (_copiedAlertTextBent == null && _copiedDoneTextBent == null)
            _copiedAlertTextBent = _copiedAlertTextBent;
        
        IOSBridge.IOStoUnityBridge.CopyText(portBent.text);
        IOSBridge.IOStoUnityBridge.ShowAlert(_copiedDoneTextBent, _copiedAlertTextBent);
    }

    public void CopyIPBent(Text ipBent)
    {
        if (_copiedAlertTextBent == null && _copiedDoneTextBent == null)
            _copiedAlertTextBent = _copiedAlertTextBent;
        
        IOSBridge.IOStoUnityBridge.CopyText(ipBent.text);
        IOSBridge.IOStoUnityBridge.ShowAlert(_copiedDoneTextBent, _copiedAlertTextBent);
    }
    
    public void CopySeedIdBent()
    {
        if (_copiedAlertTextBent == null && _copiedDoneTextBent == null)
            _copiedAlertTextBent = _copiedAlertTextBent;
        
        IOSBridge.IOStoUnityBridge.CopyText(seedTextNumBent);
        IOSBridge.IOStoUnityBridge.ShowAlert(_copiedDoneTextBent, _copiedAlertTextBent);
    }
}
