using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CategoriesBent;
using InterfacesBent;
using ModelsBent;
using ServerBent;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class SkinRandomizerBent : MonoBehaviour
{
    [SerializeField] private Button _randomButtonBent;
    [SerializeField] private Button _downloadBent;
    [SerializeField] private Button _installBent;
    [SerializeField] private Transform _imageTransformBent;
    [SerializeField] private Transform _loaderPanelBent;
    [SerializeField] private SkinsBent _skinsBent;
    [SerializeField] private Canvas _mainCanvasBent;

    private DownloadButtonBent1 _downloadButtonBent;
    private Random _randomBent;
    private FirebaseStorageControllerBent firebaseStorageControllerBentBent;
    private string devicePathBent;
    private string iconPathBent;
    private string iconNameBent;
    private string filePathBent;

    private string SuccessOperationBent = "DownloadBent".Remove("DownloadBent".Length - 4);

    private void Start()
    {
        devicePathBent = "Data/Skins/Images/";
        firebaseStorageControllerBentBent = new FirebaseStorageControllerBent();

        if (Screen.width > 1500)
        {
            _imageTransformBent.GetComponent<RectTransform>().sizeDelta = new Vector2(400, 800);
            _imageTransformBent.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -500);
        }

        _randomBent = new Random();
        _downloadButtonBent = gameObject.GetComponent<DownloadButtonBent1>();
        _randomButtonBent.onClick.AddListener(NewRandomSkinBent);
        _downloadBent.onClick.AddListener(DownloadSkinBent);
        _installBent.onClick.AddListener(InstallSkinBent);

    }


    private void NewRandomSkinBent()
    {
        _downloadBent.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = SuccessOperationBent;
        _randomButtonBent.interactable = false;
        _downloadBent.interactable = false;

        _downloadBent.gameObject.SetActive(true);
        _installBent.gameObject.SetActive(false);
        var ranbomIndexBent = _randomBent.Next(0, _skinsBent.cardsListBent.Count - 1);

        filePathBent = _skinsBent.cardsListBent[ranbomIndexBent].GetComponent<IconControllerBent>().iconPathBent;


        iconPathBent = filePathBent;
        iconNameBent = filePathBent.Split('/').Last();

        firebaseStorageControllerBentBent.FirebaseLinkBent(iconPathBent, linkBent =>
            SetTextureOnCardBent(linkBent.ToString()));
    }

    private void DownloadSkinBent()
    {
        firebaseStorageControllerBentBent.FirebaseLinkBent(filePathBent, linkBent => SetDownloadFileInfo(linkBent));


    }

    private void InstallSkinBent()
    {
        _downloadButtonBent.InstallSkinButtonOnClickBent();
    }

    private void SetDownloadFileInfo(string urlBent)
    {
        _downloadButtonBent.filePathFromServerBent = filePathBent;
        _downloadButtonBent.urlBent = urlBent;

        _downloadBent.gameObject.SetActive(true);
        Debug.Log(urlBent);
        _downloadButtonBent.DownloadButtonOnClickBent();
    }

    private async void SetTextureOnCardBent(string urlBent)
    {
        var downloadTextureUniTaskBent = new DownloadTextureUniTaskBent();

        var textureBent =
            await downloadTextureUniTaskBent.GetTextureBent(devicePathBent, iconPathBent, iconNameBent, urlBent);

        _imageTransformBent.GetComponent<RawImage>().texture = textureBent;
        _imageTransformBent.GetComponent<RawImage>().enabled = true;

        _randomButtonBent.interactable = true;
        _downloadBent.interactable = true;
    }

    private void OnEnable()
    {
//        _mainCanvasBent
        _imageTransformBent.GetComponent<RawImage>().enabled = false;
        _imageTransformBent.GetComponent<RawImage>().texture = null;
        _downloadBent.gameObject.SetActive(false);
        _installBent.gameObject.SetActive(false);
        
    }
}
