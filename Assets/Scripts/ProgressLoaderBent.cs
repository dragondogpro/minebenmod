using UnityEngine;
using UnityEngine.UI;

public class ProgressLoaderBent : MonoBehaviour
{
    [SerializeField] private GameObject _dataBent;
    [SerializeField] private GameObject _mainCanvasBent;
    [SerializeField] private GameObject _mainScreenBent;
    [SerializeField] private Image _loaderBarBent;

    private void Start()
    {
        DoAnimationSCD();
    }

    private void DoAnimationSCD()
    {
        LeanTween.value(_loaderBarBent.gameObject, 0, 1, 3f)
            .setOnUpdate((float value) => { _loaderBarBent.fillAmount = value; })
            .setEaseLinear()
            .setOnComplete(FinishFakeLoadSCD);
    }

    private void FinishFakeLoadSCD()
    {
        var _jsonSCD = ContainerBent.GetBent<JsonControllerBent>();
        
        _dataBent.SetActive(true);
        _mainScreenBent.SetActive(true);
        
        _jsonSCD.StartWorkingBent();
        Destroy(gameObject);
        
        //_mainCanvasBent.SetActive(false);
    }
}
