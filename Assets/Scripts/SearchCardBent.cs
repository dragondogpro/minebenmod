using System;
using System.Collections.Generic;
using System.Linq;
using ModelsBent;
using UnityEngine;
using UnityEngine.UI;

public class SearchCardBent : MonoBehaviour
{
    [SerializeField] private CheckCategoryBent categoryBentObject;
    [SerializeField] private string _categoryNameBent;
    [SerializeField] private InputField _inputBent;
    [SerializeField] private CategoryButtonChangeBent categoryBent;

    [SerializeField] private GameObject _gameObjectBent;
    [SerializeField] private Transform _gridTransformBent;
    [SerializeField] private List<Transform> _wikiListBent;
    
    [SerializeField] private GameObject _scrollRectForPrompt;
    
    private string _inputSearchBent;
    private string _subcategoryBent;
    private List<Transform> _categoryTestBent = new List<Transform>();
    public List<Transform> _subCatBent = new List<Transform>();
    public List<Transform> searchResultBent;
    
    private List<Transform> _favoriteCardsListBent = new List<Transform>();
    private int _countBent;
    
    private void OnEnable()
    {
        _inputBent.onEndEdit.AddListener(OnPointerClick);
        _inputBent.onValueChanged.AddListener(OnPointerClick);
    }
    
    private void OnDisable()
    {
        if(_subcategoryBent != string.Empty)
            _subcategoryBent = string.Empty;
        _inputBent.onEndEdit.RemoveAllListeners();
    }


    private void OnPointerClick(string inputBent)
    {
        _countBent = 10;
        _inputSearchBent = inputBent;
        
        
        searchResultBent = _subCatBent.Where(x => x.name.IndexOf(_inputSearchBent,
            StringComparison.OrdinalIgnoreCase) >= 0).ToList();
        
        foreach (var subBent in _subCatBent)
            subBent.gameObject.SetActive(false);

        if (_gridTransformBent.childCount > 1)
            for (int i = 1; i < _gridTransformBent.childCount; i++)
                Destroy(_gridTransformBent.GetChild(i).gameObject);

        if (_inputSearchBent != "")
        {
            if (searchResultBent.Count < _countBent)
                _countBent = searchResultBent.Count;

            foreach (var subBent in searchResultBent.GetRange(0, _countBent))
            {
                var objectCardBent = Instantiate(_gameObjectBent, _gridTransformBent);
                objectCardBent.name = subBent.name;
                objectCardBent.transform.GetChild(0).gameObject.GetComponent<Text>().text = subBent.name;
                objectCardBent.SetActive(true);
            }
        }


        foreach (var subBent in searchResultBent)
            subBent.gameObject.SetActive(true);

        _scrollRectForPrompt.SetActive(_inputSearchBent != "");
        if (searchResultBent.Count == 0)
            _scrollRectForPrompt.SetActive(false);

        if (_inputSearchBent == "" && searchResultBent.Count == 0)
            foreach (var itemBent in _subCatBent)
                itemBent.gameObject.SetActive(_inputSearchBent == "" && searchResultBent.Count == 0);
    }

    public void DisabledSearchBent()
    {
        _inputBent.text = "";
        _subCatBent.Clear();
    }

    public void AddToListBent()
    {
        _subCatBent.Clear();
        _subcategoryBent = categoryBent.SelecetedCategoryBent.ToLower();
        _categoryTestBent = categoryBentObject.GetSubcategoryListBent(_subcategoryBent, _categoryNameBent);

        foreach (var subcatBent in _categoryTestBent)
        {
            if (subcatBent.gameObject.GetComponent<CardDefaultBent>().cardTypeBent == _categoryNameBent)
            {
                _subCatBent.Add(subcatBent);
            }
        }
    }

    public void AddToWikiListBent()
    {
        _subCatBent.Clear();
        
        _categoryTestBent = _wikiListBent;
       
        foreach (var subcatBent in _categoryTestBent)
            _subCatBent.Add(subcatBent);
    }


    public void InsertTextBent(Text cardNameBent)
    {
        _inputBent.text = cardNameBent.text;
        _inputSearchBent = cardNameBent.text;
        
        _scrollRectForPrompt.SetActive(false);
    }
}
