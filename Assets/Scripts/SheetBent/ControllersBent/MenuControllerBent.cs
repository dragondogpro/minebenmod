using UnityEngine;
using System;
using UnityEngine.Serialization;

public class MenuControllerBent : BaseControllerBent
{
    [FormerlySerializedAs("_mainCanvasSCD")] [SerializeField] private GameObject _mainCanvasBent;

    public Action OnMenuChangedBent;
    [FormerlySerializedAs("CurrentItemSCD")] public MenuItemBent CurrentItemBent;

    protected RectTransform _curMainRectBent;

    private GameObject _currentPressedBent;
    private GameObject[] _currentPanelsBent;

    public override void Awake()
    {
        base.Awake();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void Start()
    {
        _curMainRectBent = _mainCanvasBent.GetComponent<RectTransform>();
        CurrentItemBent = MenuItemBent.Skins3dBent;
        _currentPanelsBent = ContainerBent.GetBent<Skins3dControllerBent>().CategoryPanelsBent;

        
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    [Serializable]
    public enum MenuItemBent
    {
        Skins3dBent = 0
    }

    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<MenuControllerBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<MenuControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
