using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using System;
using UnityEngine.Serialization;

public class CreatorControllerBent : BaseControllerBent
{
    [FormerlySerializedAs("_skinAnimSCD")] [SerializeField] private SkinLiftingBent skinAnimBent;
    [SerializeField] private GameObject _mainCanvasBent;
    [FormerlySerializedAs("_skinEditorSCD")]
    [Space]
    [SerializeField] private SkinEditorSettingsBent skinEditorBent;
    [FormerlySerializedAs("_skinMakerSCD")] [SerializeField] public SkinMakerSettingsBent skinMakerBent;
    [FormerlySerializedAs("_previewPanelSCD")] [SerializeField] private PreviewPanelSCD _previewPanelBent;
    [FormerlySerializedAs("_editorButtonSCD")]
    [Space]
    [SerializeField] private Button _editorButtonBent;
    [FormerlySerializedAs("_makerButtonSCD")] [SerializeField] private Button _makerButtonBent;
    [FormerlySerializedAs("_headSCD")]
    [Space]
    [SerializeField] private GameObject _headBent;
    [FormerlySerializedAs("_bodySCD")] [SerializeField] private Renderer _bodyBent;
    [FormerlySerializedAs("_leftArmSCD")] [SerializeField] private Renderer _leftArmBent;
    [FormerlySerializedAs("_rightArmSCD")] [SerializeField] private Renderer _rightArmBent;
    [FormerlySerializedAs("_leftLegCD")] [SerializeField] private Renderer _leftLeBent;
    [FormerlySerializedAs("_rightLegSCD")] [SerializeField] private Renderer _rightLegBent;
    [FormerlySerializedAs("_accesoriesHeadSCD")]
    [Space]
    [SerializeField] private Transform _accesoriesHeadBent;
    [FormerlySerializedAs("_accesoriesBackSCD")] [SerializeField] private Transform _accesoriesBackBent;
    [FormerlySerializedAs("_accessoriesHandsSCD")] [SerializeField] private Transform _accessoriesHandsBent;
    [FormerlySerializedAs("_backButtonSCD")]
    [Space]
    [SerializeField] private Button _backButtonBent;
    [FormerlySerializedAs("_optionsButtonSCD")] [SerializeField] private Button _optionsButtonBent;
    [FormerlySerializedAs("_optionsSCD")] [SerializeField] private OptionsMenuBent optionsBent;
    [FormerlySerializedAs("_onExitAlertSCD")] [SerializeField] private GameObject _onExitAlertBent;
    [FormerlySerializedAs("_cancButtonAlertSCD")] [SerializeField] private Button _cancButtonAlertBent;
    [FormerlySerializedAs("_yesButtonAlertSCD")] [SerializeField] private Button _yesButtonAlertBent;
    [FormerlySerializedAs("_defMaterialSCD")]
    [Space]
    [SerializeField] private Material _defMaterialBent;
    [FormerlySerializedAs("_defSkinColSCD")] [SerializeField] private Color _defSkinColBent;

    [FormerlySerializedAs("_previewCreatorSCD")] public PreviewCreatorBent previewCreatorBent;
    public Action<bool> OnFavoriteChangeBent;
    public Action OnEditExitBent;
    public Action OnDeleteSkinBent;

    private Skin3DviewBent _skinToDisplayBent;
    private Skin3DviewBent _toCheckTestBent;
    private int _currCardIndxBent;
    [FormerlySerializedAs("zeroStepSCD")] public bool zeroStepBent = false;
    public Skins3DdataBent SkinDataEditBent = null;
    private JsonControllerBent _jsonBent = null;
    private DrawingControllerBent _drawControllerBent = null;
    private Skins3dControllerBent _skin3dMainBent = null;
    private bool IsAlreadyCreatedBent = false;
    private AssetBundle _contenBent;

    public override void Awake()
    {
        base.Awake();

        _backButtonBent.onClick.AddListener(() => HideCreatorBent(false));
        _editorButtonBent.onClick.AddListener(() => ChangeCreatorModeBent(false));
        _makerButtonBent.onClick.AddListener(() => ChangeCreatorModeBent(true));
        _cancButtonAlertBent.onClick.AddListener(() => _onExitAlertBent.SetActive(false));
        _yesButtonAlertBent.onClick.AddListener(() => HideCreatorBent(true));
        _optionsButtonBent.onClick.AddListener(() => { optionsBent.gameObject.SetActive(true); _drawControllerBent._currentEditTypeBent = EditTypeBent.AbleRotBent; });

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void Start()
    {
        skinEditorBent.gameObject.SetActive(false);
        this.gameObject.SetActive(false);
        //_mainCanvasBent.gameObject.SetActive(false);
    }
    private void InitCreatorBent()
    {
        if (_jsonBent == null )
            _jsonBent = ContainerBent.GetBent<JsonControllerBent>();
        if (_drawControllerBent == null)
            _drawControllerBent = ContainerBent.GetBent<DrawingControllerBent>();
        if (_skin3dMainBent == null)
            _skin3dMainBent = ContainerBent.GetBent<Skins3dControllerBent>();
        if (_contenBent == null)
            _contenBent = _jsonBent._contentAssetsBent;
        if (_skinToDisplayBent != null)
        {
            SkinDataEditBent = new Skins3DdataBent();
            SkinDataEditBent = _skinToDisplayBent.SkinToDisplayBent;
            RebuildSkinBent(SkinDataEditBent);
            IsAlreadyCreatedBent = true;
        }
        else
        {
            
            SkinDataEditBent = new Skins3DdataBent();
            int uniqIdBent = 0;
            SkinDataEditBent.SkinIdBent = uniqIdBent;
            while (_jsonBent._saved3dSkinsBent.Exists(someDataBent => someDataBent.SkinIdBent == SkinDataEditBent.SkinIdBent))
            {
                SkinDataEditBent.SkinIdBent = uniqIdBent++;
            }
            SkinDataEditBent.SkinNameBent = "New skinBent".Replace("Bent", "");
            int indxMupltiBent = 1;
            while (_jsonBent._saved3dSkinsBent.Exists(someDataBent => someDataBent.SkinNameBent == SkinDataEditBent.SkinNameBent))
            {
                SkinDataEditBent.SkinNameBent = $"New skin ({indxMupltiBent++})Bent".Replace("Bent", "");
            }
            SkinDataEditBent.IsFavoriteBent = false;
            SetDefaultModelBent();
            IsAlreadyCreatedBent = false;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ShowCreatorBent(Skin3DviewBent _skinBent)
    {
        this.gameObject.SetActive(true);
        skinMakerBent.gameObject.SetActive(true);
        _skinToDisplayBent = _skinBent;
        _toCheckTestBent = _skinBent;
        _currCardIndxBent = _skinBent.CardIdBent;
        InitCreatorBent();
        skinMakerBent.InitMakerBent(GetDataDuplicateBent(_skinToDisplayBent.SkinToDisplayBent));
        skinMakerBent.UpdateActiveStepsBent();
        skinMakerBent.IsCurrentPanelBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ShowCreatorBent()
    {
        _skinToDisplayBent = null;
        _toCheckTestBent = null;
        this.gameObject.SetActive(true);
        skinMakerBent.gameObject.SetActive(true);
        skinEditorBent.gameObject.SetActive(false);
        InitCreatorBent();
        skinMakerBent.InitMakerBent(GetDataDuplicateBent(SkinDataEditBent));
        skinMakerBent.UpdateActiveStepsBent();
        skinMakerBent.IsCurrentPanelBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void HideCreatorBent(bool trueIfWantExitBent)
    {
        if (!trueIfWantExitBent)
        {
            _onExitAlertBent.SetActive(true);
            SaveSkinBent();
        }
        else
        {
            _onExitAlertBent.SetActive(false);
            this.gameObject.SetActive(false);
            ChangeCreatorModeBent(true);
            OnEditExitBent?.Invoke();
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void ChangeCreatorModeBent(bool trueIfItsMakerBent)
    {
        OnEditExitBent?.Invoke();
        if (trueIfItsMakerBent)
        {
            if (skinMakerBent.gameObject.activeSelf)
                return;
            skinEditorBent.DelTextBent();
            skinMakerBent.gameObject.SetActive(true);
            _makerButtonBent.transform.GetChild(0).gameObject.SetActive(true);
            skinEditorBent.gameObject.SetActive(false);
            _editorButtonBent.transform.GetChild(0).gameObject.SetActive(false);
            skinMakerBent.InitMakerBent(GetDataDuplicateBent(SkinDataEditBent));
            skinMakerBent.UpdateActiveStepsBent();
            skinMakerBent.IsCurrentPanelBent = true;
            skinAnimBent.LiftModelBent(0, 0.2f);
        }
        else
        {
            if (skinEditorBent.gameObject.activeSelf)
                return;
            skinEditorBent.gameObject.SetActive(true);
            _editorButtonBent.transform.GetChild(0).gameObject.SetActive(true);
            skinMakerBent.gameObject.SetActive(false);
            _makerButtonBent.transform.GetChild(0).gameObject.SetActive(false);

            skinEditorBent.ResetStepLogBent(GetNewOriginsBent());
            skinMakerBent.IsCurrentPanelBent = false;
            skinAnimBent.LiftModelBent(50, 0.2f);
        }
        SaveSkinBent();
        _drawControllerBent._currentEditTypeBent = EditTypeBent.AbleRotBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private List<OriginsDataBent> GetNewOriginsBent()
    {
        List<OriginsDataBent> _tempOriginsBent = new List<OriginsDataBent>();
        Renderer headRendBent = _headBent.transform.GetChild(1).GetComponent<Renderer>();

        OriginsDataBent orHeadBent = new OriginsDataBent();
        orHeadBent.orRendBent = headRendBent; orHeadBent.orTextBent = Instantiate(headRendBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orHeadBent);

        OriginsDataBent orLeftArmBent = new OriginsDataBent();
        orLeftArmBent.orRendBent = _leftArmBent; orLeftArmBent.orTextBent = Instantiate(_leftArmBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orLeftArmBent);

        OriginsDataBent orRightArmBent = new OriginsDataBent();
        orRightArmBent.orRendBent = _rightArmBent; orRightArmBent.orTextBent = Instantiate(_rightArmBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orRightArmBent);

        OriginsDataBent orLeftLegBent = new OriginsDataBent();
        orLeftLegBent.orRendBent = _leftLeBent; orLeftLegBent.orTextBent = Instantiate(_leftLeBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orLeftLegBent);

        OriginsDataBent orRightLegBent = new OriginsDataBent();
        orRightLegBent.orRendBent = _rightLegBent; orRightLegBent.orTextBent = Instantiate(_rightLegBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orRightLegBent);

        OriginsDataBent orBodyBent = new OriginsDataBent();
        orBodyBent.orRendBent = _bodyBent; orBodyBent.orTextBent = Instantiate(_bodyBent.material.mainTexture as Texture2D);
        _tempOriginsBent.Add(orBodyBent);

        return _tempOriginsBent;
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    public async void SaveSkinBent()
    {
        await Task.CompletedTask;
        var savedBent = _jsonBent._saved3dSkinsBent;
        
        if (!Directory.Exists(Path.GetDirectoryName(Application.persistentDataPath + $"/Saved3dSkinsSCD/{SkinDataEditBent.SkinNameBent}/Bent".Replace("Bent", ""))))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(Application.persistentDataPath + $"/Saved3dSkinsSCD/{SkinDataEditBent.SkinNameBent}/Bent".Replace("Bent", "")));
        }
        string pathToSaveImgBent = Application.persistentDataPath + $"/Saved3dSkinsSCD/{SkinDataEditBent.SkinNameBent}/Bent".Replace("Bent", "");
        var headBent = _headBent.transform.GetChild(1).GetComponent<Renderer>().material.mainTexture as Texture2D;
        string n1Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.HeadPathBent);
        if (SkinDataEditBent.HeadPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n1Bent = SkinDataEditBent.HeadPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n1Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), headBent.EncodeToJPG());
        Texture2D bodyBent = _bodyBent.material.mainTexture as Texture2D;
        string n2Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.BodyPathBent);
        if (SkinDataEditBent.BodyPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n2Bent = SkinDataEditBent.BodyPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n2Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), bodyBent.EncodeToJPG());
        var lArmBent = _leftArmBent.material.mainTexture as Texture2D;
        string n3Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.LeftArmPathBent);
        if (SkinDataEditBent.LeftArmPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n3Bent = SkinDataEditBent.LeftArmPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n3Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), lArmBent.EncodeToJPG());
        var rArmBent = _rightArmBent.material.mainTexture as Texture2D;
        string n4Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.RightArmPathBent);
        if (SkinDataEditBent.RightArmPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n4Bent = SkinDataEditBent.RightArmPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n4Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), rArmBent.EncodeToJPG());
        var lLegBent = _leftLeBent.material.mainTexture as Texture2D;
        string n5Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.LeftLegPathBent);
        if (SkinDataEditBent.LeftLegPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n5Bent = SkinDataEditBent.LeftLegPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n5Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), lLegBent.EncodeToJPG());
        var rLegBent = _rightLegBent.material.mainTexture as Texture2D;
        string n6Bent = Path.GetFileNameWithoutExtension(SkinDataEditBent.RightLegPathBent);
        if (SkinDataEditBent.RightLegPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent)).Length > 1)
            n6Bent = SkinDataEditBent.RightLegPathBent.Split(StrHoldeBent.GetEditedStrBent(StrHoldeBent.CloneBent))[0];
        /*await*/ File.WriteAllBytes(pathToSaveImgBent + n6Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent), rLegBent.EncodeToJPG());
        
        SkinDataEditBent.IsEditedBent = true;
        SkinDataEditBent.HeadPathBent = pathToSaveImgBent + n1Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        Debug.Log("When saving    " + SkinDataEditBent.HeadPathBent);
        SkinDataEditBent.BodyPathBent = pathToSaveImgBent + n2Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        SkinDataEditBent.LeftArmPathBent = pathToSaveImgBent + n3Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        SkinDataEditBent.RightArmPathBent = pathToSaveImgBent + n4Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        SkinDataEditBent.LeftLegPathBent = pathToSaveImgBent + n5Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        SkinDataEditBent.RightLegPathBent = pathToSaveImgBent + n6Bent + StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent);
        string forFastCheckBent = SkinDataEditBent.SkinNameBent;
        savedBent.RemoveAll(someDataSCD => someDataSCD.SkinIdBent == SkinDataEditBent.SkinIdBent);
        savedBent.Add(SkinDataEditBent);
        
        string dataBent = JsonConvert.SerializeObject(savedBent);
        /*await*/ File.WriteAllText(Application.persistentDataPath + "/JsonSCD/Saved3dSkinsSCDBent".Replace("Bent", ""), dataBent);

        Debug.Log("If false - add new card in scrol Bent:   " + IsAlreadyCreatedBent);

        if (!IsAlreadyCreatedBent)
        {
            _skinToDisplayBent = _skin3dMainBent.InitNiwSkinBent(SkinDataEditBent);
            IsAlreadyCreatedBent = true;
        } 
        else
            _skinToDisplayBent.InitBent(SkinDataEditBent, false);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void DeleteSkinBent()
    {
        _jsonBent.StubPictureBent.SetActive(true);
        await _previewPanelBent.HideTaskBent();
        var savedBent = _jsonBent._saved3dSkinsBent;
        
        if (_skinToDisplayBent != null)
        {
            int toRemoveBent = _skin3dMainBent._skinCardsBent.FindIndex(oBent => oBent.SkinToDisplayBent.SkinIdBent == _skinToDisplayBent.SkinToDisplayBent.SkinIdBent);
            Destroy(_skin3dMainBent._skinCardsBent[toRemoveBent].gameObject);
            _skin3dMainBent._skinCardsBent.RemoveAt(toRemoveBent);
        }
        if (_jsonBent.SavedFavoritesBent.ContainsKey(SkinDataEditBent.SkinIdBent.ToString()))
        {
            _jsonBent.SavedFavoritesBent.Remove(SkinDataEditBent.SkinIdBent.ToString());
            _jsonBent.SaveDataBent();
        }
        HideCreatorBent(true);
        _jsonBent.StubPictureBent.SetActive(false);
        _skin3dMainBent.CategoryChangedBent?.Invoke();
        if (savedBent.Exists(oBent => oBent.SkinIdBent == SkinDataEditBent.SkinIdBent))
        {
            int indxToRemBent = savedBent.FindIndex(oBent => oBent.SkinIdBent == SkinDataEditBent.SkinIdBent);
            savedBent.RemoveAt(indxToRemBent);
            string dataBent = JsonConvert.SerializeObject(savedBent);
            await File.WriteAllTextAsync(Application.persistentDataPath + "/JsonSCD/Saved3dSkinsSCDBent".Replace("Bent", ""), dataBent);
            if (Directory.Exists(Application.persistentDataPath + $"/JsonSCD/Saved3dSkinsSCD/{SkinDataEditBent.SkinNameBent}/Bent".Replace("Bent", "")))
                Directory.Delete(Application.persistentDataPath + $"/JsonSCD/Saved3dSkinsSCD/{SkinDataEditBent.SkinNameBent}/Bent".Replace("Bent", ""));
        }
        
        

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void SetDefaultModelBent()
    {

        SkinDataEditBent.HeadPathBent = StrHoldeBent._headsPathBent + "simp.jpgBent".Replace("Bent", "");
        SkinDataEditBent.BodyPathBent = "assets/resources/3d/simp 1.jpgBent".Replace("Bent", "");
        SkinDataEditBent.LeftArmPathBent = "assets/resources/3d/simp 2.jpgBent".Replace("Bent", "");
        SkinDataEditBent.RightArmPathBent = "assets/resources/3d/simp 3.jpgBent".Replace("Bent", "");
        SkinDataEditBent.LeftLegPathBent = "assets/resources/3d/simp 4.jpgBent".Replace("Bent", "");
        SkinDataEditBent.RightLegPathBent = "assets/resources/3d/simp 5.jpgBent".Replace("Bent", "");
        _bodyBent.material.mainTexture = null;
        _leftArmBent.material.mainTexture = null;
        _rightArmBent.material.mainTexture = null;
        _leftLeBent.material.mainTexture = null;
        _rightLegBent.material.mainTexture = null;

        RebuildSkinBent(SkinDataEditBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void RebuildSkinBent(Skins3DdataBent dataBentBent)
    {
        if (_headBent.transform.childCount > 1)
            Destroy(_headBent.transform.GetChild(1).gameObject);
        string fbxFileBent = dataBentBent.HeadPathBent.Remove(dataBentBent.HeadPathBent.Length - 4);
        //if (dataSCD.IsEditedSCD && Path.HasExtension(Path.GetFileName(dataSCD.HeadPathSCD)))
        //    fbxFileSCD = StrHoldeSCD._headsPathSCD + Path.GetFileNameWithoutExtension(dataSCD.HeadPathSCD);
        if (fbxFileBent.Split("/")[0] != "assetsBent".Replace("Bent", ""))
            fbxFileBent = StrHoldeBent._headsPathBent + Path.GetFileNameWithoutExtension(dataBentBent.HeadPathBent);
        //GameObject newHeadSCD = Instantiate(Resources.Load<GameObject>(fbxFileSCD), _headSCD.transform);
        
        GameObject newHeadBent = Instantiate(_contenBent.LoadAsset<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", "")) as GameObject, _headBent.transform);
        newHeadBent.SetActive(false);
        newHeadBent.layer = 3;
        newHeadBent.tag = "AbleToRotSCDBent".Replace("Bent", "");
        //newHeadSCD.AddComponent<BakeMeshColiderSCD>();

        Texture2D textureBent = new Texture2D(2, 2);
        if (dataBentBent.HeadPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
            textureBent = new Texture2D(compressedBent.width, compressedBent.height);
            textureBent.SetPixels(compressedBent.GetPixels());
            textureBent.Apply();
        }
        else
        {
            textureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.HeadPathBent).Result, false);
        }
        Renderer headRendBent = newHeadBent.GetComponent<Renderer>();
        headRendBent.material = Instantiate(_defMaterialBent);
        headRendBent.material.color = Color.white;
        headRendBent.material.mainTexture = Instantiate(textureBent);
        newHeadBent.transform.localScale = Vector3.one;
        newHeadBent.transform.localRotation = Quaternion.Euler(Vector3.zero);
        newHeadBent.SetActive(true);

        string f1bxFileBent = dataBentBent.LeftLegPathBent/*.Remove(dataSCD.LeftLegPathSCD.Length - 4)*/;
        Texture2D t1extureBent = new Texture2D(2, 2);
        if (dataBentBent.LeftLegPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f1bxFileBent);
            t1extureBent = new Texture2D(compressedBent.width, compressedBent.height);
            t1extureBent.SetPixels(compressedBent.GetPixels());
            t1extureBent.Apply();
        }  
        else
            t1extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.LeftLegPathBent).Result, false);
        if (_leftLeBent.material.mainTexture != null)
            Destroy(_leftLeBent.material.mainTexture);
        _leftLeBent.material.mainTexture = Instantiate(t1extureBent);
        _leftLeBent.material.color = Color.white;
        string f2bxFileBent = dataBentBent.RightLegPathBent/*.Remove(dataSCD.RightLegPathSCD.Length - 4)*/;
        Texture2D t2extureBent = new Texture2D(2, 2);
        if (dataBentBent.RightLegPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f2bxFileBent);
            t2extureBent = new Texture2D(compressedBent.width, compressedBent.height);
            t2extureBent.SetPixels(compressedBent.GetPixels());
            t2extureBent.Apply();
        }
        else
            t2extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.RightLegPathBent).Result, false);
        if (_rightLegBent.material.mainTexture != null)
            Destroy(_rightLegBent.material.mainTexture);
        _rightLegBent.material.mainTexture = Instantiate(t2extureBent);
        _rightLegBent.material.color = Color.white;

        string f3bxFileBent = dataBentBent.LeftArmPathBent/*.Remove(dataSCD.LeftArmPathSCD.Length - 4)*/;
        Texture2D t3extureBent = new Texture2D(2, 2);
        if (dataBentBent.LeftArmPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f3bxFileBent);
            t3extureBent = new Texture2D(compressedBent.width, compressedBent.height);
            t3extureBent.SetPixels(compressedBent.GetPixels());
            t3extureBent.Apply();
        }
        else
            t3extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.LeftArmPathBent).Result, false);
        if (_leftArmBent.material.mainTexture != null)
            Destroy(_leftArmBent.material.mainTexture);
        _leftArmBent.material.mainTexture = Instantiate(t3extureBent);
        _leftArmBent.material.color = Color.white;
        string f4bxFileBent = dataBentBent.RightArmPathBent/*.Remove(dataSCD.RightArmPathSCD.Length - 4)*/;
        Texture2D t4extureBent = new Texture2D(2, 2);
        if (dataBentBent.RightArmPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f4bxFileBent);
            t4extureBent = new Texture2D(compressedBent.width, compressedBent.height);
            t4extureBent.SetPixels(compressedBent.GetPixels());
            t4extureBent.Apply();
        }
        else
            t4extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.RightArmPathBent).Result, false);
        if (_rightArmBent.material.mainTexture != null)
         Destroy(_rightArmBent.material.mainTexture);
        _rightArmBent.material.mainTexture = Instantiate(t4extureBent);
        _rightArmBent.material.color = Color.white;

        string f5bxFileBent = dataBentBent.BodyPathBent/*.Remove(dataSCD.BodyPathSCD.Length - 4)*/;
        Texture2D t5extureBent = new Texture2D(2, 2);
        if (dataBentBent.BodyPathBent.Contains("assets/Bent".Replace("Bent", "")))
        {
            Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f5bxFileBent);
            t5extureBent = new Texture2D(compressedBent.width, compressedBent.height);
            t5extureBent.SetPixels(compressedBent.GetPixels());
            t5extureBent.Apply();
        }
        else
            t5extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.BodyPathBent).Result, false);
        if (_bodyBent.material.mainTexture != null)
            Destroy(_bodyBent.material.mainTexture);
        _bodyBent.material.mainTexture = Instantiate(t5extureBent);
        _bodyBent.material.color = Color.white;

        if (_accesoriesBackBent.childCount != 0)
            Destroy(_accesoriesBackBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesBackPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesBackPathBent.Remove(dataBentBent.AccessoriesBackPathBent.Length - 4);
            Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accesoriesBackBent);
        }
        if (_accessoriesHandsBent.childCount != 0)
            Destroy(_accessoriesHandsBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHandPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesHandPathBent.Remove(dataBentBent.AccessoriesHandPathBent.Length - 4);
            Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accessoriesHandsBent);
        }
        if (_accesoriesHeadBent.childCount != 0)
            Destroy(_accesoriesHeadBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHeadPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesHeadPathBent.Remove(dataBentBent.AccessoriesHeadPathBent.Length - 4);
            Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accesoriesHeadBent);
        }
        

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void SetMakerChangesBent(PartUiViewBent partToSetBent)
    {
        var modelDataBent = partToSetBent.ModelDataBent;
        if (!string.IsNullOrEmpty(modelDataBent.HeadPathBent))
            SkinDataEditBent.HeadPathBent = modelDataBent.HeadPathBent;
        if (!string.IsNullOrEmpty(modelDataBent.BodyPathBent))
            SkinDataEditBent.BodyPathBent = modelDataBent.BodyPathBent;
        if (!string.IsNullOrEmpty(modelDataBent.LeftArmPathBent))
        {
            SkinDataEditBent.LeftArmPathBent = modelDataBent.LeftArmPathBent;
            SkinDataEditBent.RightArmPathBent = modelDataBent.RightArmPathBent;
        }
        if (!string.IsNullOrEmpty(modelDataBent.LeftLegPathBent))
        {
            SkinDataEditBent.LeftLegPathBent = modelDataBent.LeftLegPathBent;
            SkinDataEditBent.RightLegPathBent = modelDataBent.RightLegPathBent;
        }
        if (!string.IsNullOrEmpty(modelDataBent.AccessoriesBackPathBent))
            SkinDataEditBent.AccessoriesBackPathBent = modelDataBent.AccessoriesBackPathBent;
        if (!string.IsNullOrEmpty(modelDataBent.AccessoriesHandPathBent))
            SkinDataEditBent.AccessoriesHandPathBent = modelDataBent.AccessoriesHandPathBent;
        if (!string.IsNullOrEmpty(modelDataBent.AccessoriesHeadPathBent))
            SkinDataEditBent.AccessoriesHeadPathBent = modelDataBent.AccessoriesHeadPathBent;

        if (skinMakerBent.IsCurrentPanelBent && skinMakerBent.IsNotStepjuorneyBent)
        {
            if (skinMakerBent.CurrStepMakerBent + 1 != skinMakerBent._makerStepsBent.Count)
            {
                skinMakerBent._makerStepsBent.RemoveRange(skinMakerBent.CurrStepMakerBent + 1, skinMakerBent._makerStepsBent.Count - (skinMakerBent.CurrStepMakerBent + 1));
                skinMakerBent.UpdateActiveStepsBent();
            }
            
            AddStepBent(SkinDataEditBent);
        }
        RebuildSkinBent(SkinDataEditBent);
        

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public Skins3DdataBent GetDataDuplicateBent(Skins3DdataBent origBent)
    {
        Skins3DdataBent dataDuplBent = new Skins3DdataBent();
        dataDuplBent.HeadPathBent = origBent.HeadPathBent;
        dataDuplBent.BodyPathBent = origBent.BodyPathBent;
        dataDuplBent.LeftArmPathBent = origBent.LeftArmPathBent;
        dataDuplBent.RightArmPathBent = origBent.RightArmPathBent;
        dataDuplBent.LeftLegPathBent = origBent.LeftLegPathBent;
        dataDuplBent.RightLegPathBent = origBent.RightLegPathBent;
        dataDuplBent.AccessoriesBackPathBent = origBent.AccessoriesBackPathBent;
        dataDuplBent.AccessoriesHandPathBent = origBent.AccessoriesHandPathBent;
        dataDuplBent.AccessoriesHeadPathBent = origBent.AccessoriesHeadPathBent;
        dataDuplBent.IsFavoriteBent = origBent.IsFavoriteBent;
        dataDuplBent.SkinNameBent = origBent.SkinNameBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        return dataDuplBent;
    }
    private void AddStepBent(Skins3DdataBent someBent)
    {
        skinMakerBent._makerStepsBent.Add(GetDataDuplicateBent(someBent));
        skinMakerBent.CurrStepMakerBent++;
        skinMakerBent.UpdateActiveStepsBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<CreatorControllerBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<CreatorControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}

