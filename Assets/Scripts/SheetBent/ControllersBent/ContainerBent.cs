using System;
using System.Collections.Generic;

public class ContainerBent
{
    private static Dictionary<Type, BaseControllerBent> _containerBent = new Dictionary<Type, BaseControllerBent>();

    public static void AddBent<T>(T controllerBent) where T : BaseControllerBent
    {
        _containerBent.Add(controllerBent.GetType(), controllerBent);

        // trash ------------------------------------------------------
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash ------------------------------------------------------

    }

    public static T GetBent<T>() where T : BaseControllerBent
    {
        if (_containerBent.ContainsKey(typeof(T)))
        {
            BaseControllerBent foundedBent = _containerBent[typeof(T)];
            return foundedBent as T;
        }

        // trash ------------------------------------------------------
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash ------------------------------------------------------

        return null;
    }

    public static void RemoveBent<T>() where T : BaseControllerBent
    {
        if (_containerBent.ContainsKey(typeof(T)))
        {
            _containerBent.Remove(typeof(T));
        }

        // trash ------------------------------------------------------
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash ------------------------------------------------------

    }

    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
