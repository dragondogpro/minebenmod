public class Skins3DdataBent
{
    public string SkinNameBent { get; set; }
    public bool IsFavoriteBent { get; set; }
    public string HeadPathBent{ get; set; }
    public string BodyPathBent { get; set; }
    public string LeftArmPathBent { get; set; }
    public string RightArmPathBent { get; set; }
    public string LeftLegPathBent { get; set; }
    public string RightLegPathBent { get; set; }
    public string AccessoriesHeadPathBent { get; set; }
    public string AccessoriesBackPathBent { get; set; }
    public string AccessoriesHandPathBent { get; set; }
    public int SkinIdBent { get; set; }
    public bool IsEditedBent = false;
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
