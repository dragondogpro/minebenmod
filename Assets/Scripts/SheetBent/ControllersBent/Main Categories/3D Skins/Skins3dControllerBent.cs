using System.Collections.Generic;
using UnityEngine;
using System;
using System.Threading.Tasks;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.Serialization;

public class Skins3dControllerBent : BaseControllerBent
{
    [FormerlySerializedAs("_contentTransformSCD")] [SerializeField] private RectTransform _contentTransformBent;
    [FormerlySerializedAs("_mainCanvas")] [SerializeField] private GameObject _mainCanvasBent;
    [FormerlySerializedAs("_cardPrefabSCD")]
    [Space]
    [SerializeField] private Skin3DviewBent _cardPrefabBent;
    [FormerlySerializedAs("_skinCreatorSCD")] [SerializeField] private CreatorControllerBent skinCreatorBent;
    //[Space]

    public Action CategoryChangedBent;
    [FormerlySerializedAs("CategoryPanelsSCD")] public GameObject[] CategoryPanelsBent;

    [FormerlySerializedAs("_skinCardsSCD")] public List<Skin3DviewBent> _skinCardsBent = new List<Skin3DviewBent>();
    public bool IsCurrentPanelSCD = false;

    [FormerlySerializedAs("_contentScrollReseterSCD")] public ScrollResetterBent contentScrollReseterBent;

    private int _curInitedCardsBent = 0;

    public override void Awake()
    {
        base.Awake();
        CategoryChangedBent += (() => contentScrollReseterBent.CallToResetScrollBent(false));

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public Skin3DviewBent InitNiwSkinBent(Skins3DdataBent dataBentBent)
    {
        Skin3DviewBent skinCardBent;
        if (_curInitedCardsBent + 1 < _skinCardsBent.Count)
        {
            skinCardBent = _skinCardsBent[_curInitedCardsBent + 1];
            skinCardBent.gameObject.SetActive(true);
            skinCardBent.InitBent(dataBentBent, true);
            skinCardBent.CardIdBent = _skinCardsBent.Count + 1;
            skinCardBent.On3DskinClickBent += SkinCard_OnSkinClickedBent;
            skinCardBent.IsNotEmptyBent = true;
        }
        else
        {
            skinCardBent = Instantiate(_cardPrefabBent, _contentTransformBent);
            skinCardBent.gameObject.SetActive(true);
            skinCardBent.InitBent(dataBentBent, true);
            skinCardBent.CardIdBent = _skinCardsBent.Count;
            skinCardBent.On3DskinClickBent += SkinCard_OnSkinClickedBent;
            skinCardBent.IsNotEmptyBent = true;
            _skinCardsBent.Add(skinCardBent);
            _curInitedCardsBent = _skinCardsBent.Count + 1;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        return skinCardBent;
    }
    public void InitBent(List<Skins3DdataBent> dataBent)
    {
        
        Skin3DviewBent skinCardBent;
        for (int iBent = 0; iBent < dataBent.Count; iBent++)
        {
            if (iBent < _skinCardsBent.Count)
            {
                skinCardBent = _skinCardsBent[iBent];
                skinCardBent.gameObject.SetActive(true);
                skinCardBent.InitBent(dataBent[iBent], true);
                skinCardBent.CardIdBent = iBent;
                skinCardBent.On3DskinClickBent += SkinCard_OnSkinClickedBent;
                skinCardBent.IsNotEmptyBent = true;
            }
            else
            {
                skinCardBent = Instantiate(_cardPrefabBent, _contentTransformBent);
                skinCardBent.gameObject.SetActive(true);
                skinCardBent.InitBent(dataBent[iBent], true);
                skinCardBent.CardIdBent = iBent;
                skinCardBent.On3DskinClickBent += SkinCard_OnSkinClickedBent;
                skinCardBent.IsNotEmptyBent = true;
                _skinCardsBent.Add(skinCardBent);
            }
            _curInitedCardsBent = iBent;
        }

        _skinCardsBent.RemoveAll(skinCardBent => skinCardBent.IsNotEmptyBent == false);
        InitCategoryBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void InitCategoryBent()
    {
        IsCurrentPanelSCD = true;
        foreach (var itemBent in _skinCardsBent)
        {
            itemBent.gameObject.SetActive(true);
        }


        _mainCanvasBent.SetActive(false);
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void SkinCard_OnSkinClickedBent(Skin3DviewBent objectClickedBent)
    {
        skinCreatorBent.ShowCreatorBent(objectClickedBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<Skins3dControllerBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<Skins3dControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
