using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Linq;
using UnityEngine.Serialization;

public class Skin3DviewBent : MonoBehaviour
{
    [FormerlySerializedAs("_cardButtonSCD")] [SerializeField] private Button _cardButtonBent;
    [FormerlySerializedAs("_headSCD")]
    [Space]
    [SerializeField] private GameObject _headBent;
    [FormerlySerializedAs("_bodySCD")] [SerializeField] private Renderer _bodyBent;
    [FormerlySerializedAs("_leftArmSCD")] [SerializeField] private Renderer _leftArmBent;
    [FormerlySerializedAs("_rightArmSCD")] [SerializeField] private Renderer _rightArmBent;
    [FormerlySerializedAs("_leftLegCD")] [SerializeField] private Renderer _leftLegBent;
    [FormerlySerializedAs("_rightLegSCD")] [SerializeField] private Renderer _rightLegBent;
    [FormerlySerializedAs("_accesoriesHeadSCD")]
    [Space]
    [SerializeField] private Transform _accesoriesHeadBent;
    [FormerlySerializedAs("_accesoriesBackSCD")] [SerializeField] private Transform _accesoriesBackBent;
    [FormerlySerializedAs("_accessoriesHandsSCD")] [SerializeField] private Transform _accessoriesHandsBent;
    [FormerlySerializedAs("_skinNameSCD")]
    [Space]
    [SerializeField] private TextMeshProUGUI _skinNameBent;

    [FormerlySerializedAs("_previeImageSCD")]
    [Space]
    [SerializeField] private RawImage _previeImageBent;

    public delegate void OnCardClickedBent(Skin3DviewBent cardClickedBent);
    public event OnCardClickedBent On3DskinClickBent;
    [FormerlySerializedAs("IsNotEmptySCD")] public bool IsNotEmptyBent = false;
    public Skins3DdataBent SkinToDisplayBent;
    [FormerlySerializedAs("CardIdSCD")] public int CardIdBent;

    private JsonControllerBent _jsonBent;
    private bool IsTextureDestroyedBent = true;

    private void Awake()
    {
        _cardButtonBent.onClick.AddListener(() => On3DskinClickBent(this));

        _headBent.transform.GetChild(1).GetComponent<Renderer>().material.mainTexture = null;
        _bodyBent.material.mainTexture = null;
        _leftArmBent.material.mainTexture = null;
        _rightArmBent.material.mainTexture = null;
        _leftLegBent.material.mainTexture = null;
        _rightLegBent.material.mainTexture = null;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void InitBent(Skins3DdataBent dataBentBent, bool isNewCarsBent)
    {
        IsTextureDestroyedBent = false;
        if (_jsonBent == null)
            _jsonBent = ContainerBent.GetBent<JsonControllerBent>();

        SkinToDisplayBent = dataBentBent;
        
        AssetBundle contBent = _jsonBent._contentAssetsBent;    
        _skinNameBent.text = SkinToDisplayBent.SkinNameBent;

        Destroy(_headBent.transform.GetChild(1).gameObject);
        string fbxFileBent = StrHoldeBent._headsPathBent + Path.GetFileNameWithoutExtension(dataBentBent.HeadPathBent);
        GameObject newHeadBent = Instantiate(contBent.LoadAsset<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", "")), _headBent.transform);
        newHeadBent.SetActive(false);
        Texture2D textureBent = new Texture2D(2, 2);
        string bodyPartBentPath = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.HeadPathBent);
        byte[] bodyPartBent = await File.ReadAllBytesAsync(bodyPartBentPath);
        textureBent.LoadImage(bodyPartBent, false);
        Renderer headRendBent = newHeadBent.GetComponent<Renderer>();
        headRendBent.material.color = Color.white;
        headRendBent.material.mainTexture = Instantiate(textureBent);
        Destroy(textureBent);
        newHeadBent.transform.localScale = Vector3.one;
        newHeadBent.transform.localRotation = Quaternion.Euler(Vector3.zero);
        newHeadBent.SetActive(true);

        string f1bxFileBent = dataBentBent.LeftLegPathBent.Remove(dataBentBent.LeftLegPathBent.Length - 4);
        Texture2D t1extureBent = new Texture2D(2, 2);
        string bodyPartSCDPath3Bent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.LeftLegPathBent);
        t1extureBent.LoadImage(File.ReadAllBytes(bodyPartSCDPath3Bent), false);
        if (_leftLegBent.material.mainTexture != null)
            Destroy(_leftLegBent.material.mainTexture);
        _leftLegBent.material.mainTexture = Instantiate(t1extureBent);
        Destroy(t1extureBent);
        _leftLegBent.material.color = Color.white;
        string f2bxFileBent = dataBentBent.RightLegPathBent.Remove(dataBentBent.RightLegPathBent.Length - 4);
        Texture2D t2extureBent = new Texture2D(2, 2);
        string bodyPartSCDPath1Bent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.RightLegPathBent);
        t2extureBent.LoadImage(File.ReadAllBytes(bodyPartSCDPath1Bent), false);
        if (_rightLegBent.material.mainTexture != null)
            Destroy(_rightLegBent.material.mainTexture);
        _rightLegBent.material.mainTexture = Instantiate(t2extureBent);
        Destroy(t2extureBent);
        _rightLegBent.material.color = Color.white;

        string f3bxFileBent = dataBentBent.LeftArmPathBent.Remove(dataBentBent.LeftArmPathBent.Length - 4);
        Texture2D t3extureBent = new Texture2D(2, 2);
        string bodyPartSCDPath0Bent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.LeftArmPathBent);
        t3extureBent.LoadImage(File.ReadAllBytes(bodyPartSCDPath0Bent), false);
        if (_leftArmBent.material.mainTexture != null)
            Destroy(_leftArmBent.material.mainTexture);
        _leftArmBent.material.mainTexture = Instantiate(t3extureBent);
        Destroy(t3extureBent);
        _leftArmBent.material.color = Color.white;
        string f4bxFileBent = dataBentBent.RightArmPathBent.Remove(dataBentBent.RightArmPathBent.Length - 4);
        Texture2D t4extureBent = new Texture2D(2, 2);
        string bodyPartSCDPath2Bent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.RightArmPathBent);
        t4extureBent.LoadImage(File.ReadAllBytes(bodyPartSCDPath2Bent), false);
        if (_rightArmBent.material.mainTexture != null)
            Destroy(_rightArmBent.material.mainTexture);
        _rightArmBent.material.mainTexture = Instantiate(t4extureBent);
        Destroy(t4extureBent);
        _rightArmBent.material.color = Color.white;

        string f5bxFileBent = dataBentBent.BodyPathBent.Remove(dataBentBent.BodyPathBent.Length - 4);
        Texture2D t5extureBent = new Texture2D(2, 2);
        string bodyPartSCDPath4Bent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + dataBentBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(dataBentBent.BodyPathBent);
        t5extureBent.LoadImage(File.ReadAllBytes(bodyPartSCDPath4Bent), false);
        if (_bodyBent.material.mainTexture != null)
            Destroy(_bodyBent.material.mainTexture);
        _bodyBent.material.mainTexture = Instantiate(t5extureBent);
        Destroy(t5extureBent);
        _bodyBent.material.color = Color.white;

        if (_accesoriesBackBent.childCount != 0)
            Destroy(_accesoriesBackBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesBackPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesBackPathBent/*.Remove(dataSCD.AccessoriesBackPathSCD.Length - 4)*/;
            Instantiate(contBent.LoadAsset<GameObject>(f6bxFileBent), _accesoriesBackBent);
        }
        if (_accessoriesHandsBent.childCount != 0)
            Destroy(_accessoriesHandsBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHandPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesHandPathBent/*.Remove(dataSCD.AccessoriesHandPathSCD.Length - 4)*/;
            Instantiate(contBent.LoadAsset<GameObject>(f6bxFileBent), _accessoriesHandsBent);
        }
        if (_accesoriesHeadBent.childCount != 0)
            Destroy(_accesoriesHeadBent.GetChild(0).gameObject);
        if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHeadPathBent))
        {
            string f6bxFileBent = dataBentBent.AccessoriesHeadPathBent/*.Remove(dataSCD.AccessoriesHeadPathSCD.Length - 4)*/;
            Instantiate(contBent.LoadAsset<GameObject>(f6bxFileBent), _accesoriesHeadBent);
        }
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }

    }
    public async void SetImageBent()
    {
        if (!IsTextureDestroyedBent)
            return;
        IsTextureDestroyedBent = false;
        var headBent = _headBent.transform.GetChild(1).GetComponent<Renderer>();
        if (headBent != null && headBent.material.mainTexture == null)
        {
            Texture2D textureBent = new Texture2D(2, 2);
            string bodyPartSCDPathBent = Application.persistentDataPath + "/Saved3dSkinsSCD/Bent".Replace("Bent", "") + SkinToDisplayBent.SkinNameBent + "/Bent".Replace("Bent", "") + Path.GetFileName(SkinToDisplayBent.HeadPathBent);
            byte[] bodyPartBent = await File.ReadAllBytesAsync(bodyPartSCDPathBent);
            textureBent.LoadImage(bodyPartBent, false);
            headBent.material.mainTexture = null;
        }
        if (_bodyBent.material.mainTexture == null)
        {
            _bodyBent.material.mainTexture = null;
        }
        if (_leftArmBent.material.mainTexture == null)
        {
            _leftArmBent.material.mainTexture = null;
        }
        if (_rightArmBent.material.mainTexture == null)
        {
            _rightArmBent.material.mainTexture = null;
        }
        if (_leftLegBent.material.mainTexture == null)
        {
            _leftLegBent.material.mainTexture = null;
        }
        if (_rightLegBent.material.mainTexture == null)
        {
            _rightLegBent.material.mainTexture = null;
        }
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }

    }
    public void DestroyTexturesBent()
    {
        if (IsTextureDestroyedBent)
            return;
        IsTextureDestroyedBent = true;

        
        var headBent = _headBent.transform.GetChild(1).GetComponent<Renderer>();
        if (headBent != null && headBent.material.mainTexture != null)
        {
            Destroy(headBent.material.mainTexture);
            headBent.material.mainTexture = null;
        }
        if (_bodyBent.material.mainTexture != null)
        {
            Destroy(_bodyBent.material.mainTexture);
            _bodyBent.material.mainTexture = null;
        }
        if (_leftArmBent.material.mainTexture != null)
        {
            Destroy(_leftArmBent.material.mainTexture);
            _leftArmBent.material.mainTexture = null;
        }
        if (_rightArmBent.material.mainTexture != null)
        {
            Destroy(_rightArmBent.material.mainTexture);
            _rightArmBent.material.mainTexture = null;
        }
        if (_leftLegBent.material.mainTexture != null)
        {
            Destroy(_leftLegBent.material.mainTexture);
            _leftLegBent.material.mainTexture = null;
        }
        if (_rightLegBent.material.mainTexture != null)
        {
            Destroy(_rightLegBent.material.mainTexture);
            _rightLegBent.material.mainTexture = null;
        }
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
