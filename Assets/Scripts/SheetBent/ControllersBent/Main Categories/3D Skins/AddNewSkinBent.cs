using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class AddNewSkinBent : MonoBehaviour
{
    [FormerlySerializedAs("_skins3DcontrollerSCD")] [SerializeField] private Skins3dControllerBent skins3DcontrollerBent;
    [FormerlySerializedAs("_creatorSCD")] [SerializeField] private CreatorControllerBent creatorBent;

    private Button _buttonBent;

    private void Awake()
    {
        _buttonBent = GetComponent<Button>();
        _buttonBent.onClick.AddListener(creatorBent.ShowCreatorBent);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
