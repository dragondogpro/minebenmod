using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class FakeLoadingBent : MonoBehaviour
{
    [FormerlySerializedAs("_FakeSceneBackGrSCD")] [SerializeField] private Image _FakeSceneBackGrBent;
    [FormerlySerializedAs("_progressIndicationSCD")] [SerializeField] private Image _progressIndicationBent;


    private void Start()
    {
        DoAnimationBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    private void DoAnimationBent()
    {
        LeanTween.value(_progressIndicationBent.gameObject, 0, 1, 3f)
            .setOnUpdate((float valueBent) => { _progressIndicationBent.fillAmount = valueBent; })
            .setEaseLinear()
            .setOnComplete(FinishFakeLoadBent);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    private void FinishFakeLoadBent()
    {
        var _jsonBent = ContainerBent.GetBent<JsonControllerBent>();
        _jsonBent.StartWorkingBent();
        Destroy(gameObject);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
