using UnityEngine;
using System.Threading.Tasks;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Serialization;

public class SetTextureInScrollBent : MonoBehaviour
{
    [FormerlySerializedAs("_mainScrollRectSCD")] [SerializeField] private ScrollRect _mainScrollRectBent;
    [FormerlySerializedAs("_mainSCD")] [SerializeField] private SkinMakerSettingsBent _mainBent;

    [FormerlySerializedAs("disctanceToCheckCallingLoadSCD")] [SerializeField] private /*const*/ float disctanceToCheckCallingLoadBent = 50;
    [FormerlySerializedAs("distanceToCallLoadImagesSCD")] [SerializeField] private /*const*/ float distanceToCallLoadImagesBent = 0;

    [FormerlySerializedAs("ScrollViewContentLastPosistionSCD")] public float ScrollViewContentLastPosistionBent = Mathf.Infinity;
    private List<PartUiViewBent> _cardListBent;

    private bool IsForcedCallBent = false;

    private RectTransform _mainViewRectTransformBent;
    

    private void Start()
    {
        _mainScrollRectBent.onValueChanged.AddListener(_ => OnCardScrollViewValueChangedBent());
        _mainViewRectTransformBent = _mainScrollRectBent.GetComponent<RectTransform>();
        _cardListBent = _mainBent._partsBent;
        _mainBent.OnPartScrollOpenBent += (() => { IsForcedCallBent = true; OnCardScrollViewValueChangedBent();  });

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    public async void OnCardScrollViewValueChangedBent()
    {
        if (_mainScrollRectBent.gameObject.activeSelf)
        {
            await ServersCardScrollViewValueChangedBent();

            // trash =====================================================
            var boolBent = true;
            var boolBent2 = true;
            var boolBent3 = true;

            if (boolBent != boolBent2 && boolBent2 != boolBent3)
            {
                boolBent = true;
                boolBent2 = true;
                boolBent3 = true;
            }
        // trash =====================================================
        }
        else
        {
            return;
        }
    }

    private async Task ServersCardScrollViewValueChangedBent()
    {
        await Task.CompletedTask;

        float _scrollLocalPosYBent = _mainScrollRectBent.content.transform.localPosition.y;
        float _mathfAbsBetweenPositionsBent = Mathf.Abs(ScrollViewContentLastPosistionBent - _scrollLocalPosYBent);

        if (IsForcedCallBent || _mathfAbsBetweenPositionsBent >= disctanceToCheckCallingLoadBent)
        {
            // trash =====================================================
            var boolBent = true;
            var boolBent2 = true;
            var boolBent3 = true;

            if (boolBent != boolBent2 && boolBent2 != boolBent3)
            {
                boolBent = true;
                boolBent2 = true;
                boolBent3 = true;
            }
            // trash =====================================================
            if (IsForcedCallBent)
            {
                IsForcedCallBent = false;
                await Task.Delay(100);
            }
            
            float _toCheckRectMinYBent = _mainViewRectTransformBent.rect.yMin - distanceToCallLoadImagesBent;
            float _toCheckRectMaxYBent = _mainViewRectTransformBent.rect.yMax + distanceToCallLoadImagesBent;

            foreach (var someCardBent in _cardListBent)
            {
                ScrollViewContentLastPosistionBent = _scrollLocalPosYBent;

                RectTransform _cardTransformPosBent = someCardBent.GetComponent<RectTransform>();
                Vector3 _positionInWordBent = _cardTransformPosBent.parent.TransformPoint(_cardTransformPosBent.localPosition);
                Vector3 _positionInScrollBent = _mainViewRectTransformBent.InverseTransformPoint(_positionInWordBent);
                float _cardMinYPosBent = _positionInScrollBent.y + _cardTransformPosBent.rect.yMin;
                float _cardMaxYPosBent = _positionInScrollBent.y + _cardTransformPosBent.rect.yMax;

                if (_cardMaxYPosBent >= _toCheckRectMinYBent && _cardMinYPosBent <= _toCheckRectMaxYBent)
                {
                    if (!someCardBent.IsBuildedBent)
                        someCardBent.BuildModelBent();
                }
                else
                {
                    if (someCardBent.IsBuildedBent)
                        someCardBent.CleanMemoryBent();
                    else if (!someCardBent.IsBuildedBent && someCardBent.IsInProgressBent)
                        someCardBent.IsDeleteRequestedBent = true;
                    
                }
            }
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
