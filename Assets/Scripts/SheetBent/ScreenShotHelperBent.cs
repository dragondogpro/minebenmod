using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.Serialization;

public class ScreenShotHelperBent : MonoBehaviour
{
    [FormerlySerializedAs("_camSCD")] [SerializeField] private Camera _camBent;

    public async Task GetScreenShotBent(string savePathBent)
    {
        await Task.CompletedTask;

        RenderTexture screenTextureBent = new RenderTexture(Screen.width, Screen.height, 16);
        _camBent.targetTexture = screenTextureBent;
        RenderTexture.active = screenTextureBent;
        _camBent.Render();
        Texture2D renderedTextureBent = new Texture2D(Screen.width, Screen.height);
        renderedTextureBent.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        RenderTexture.active = null;
        _camBent.targetTexture = null;
        byte[] byteArrayBent = renderedTextureBent.EncodeToPNG();
        await File.WriteAllBytesAsync(savePathBent, byteArrayBent);
        Destroy(renderedTextureBent);
        Destroy(screenTextureBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
