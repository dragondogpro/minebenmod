using UnityEngine;

public class StrHoldeBent : MonoBehaviour
{
    public static string GetEditedStrBent(string anyStrinBent)
    {
        return anyStrinBent.Remove(anyStrinBent.Length - 4);
    }

    public static string DownloadBent = "DownloadBent";
    public static string DownloadingBent = "Downloading...Bent";
    public static string InstallBent = "InstallBent";
    public static string ServersSlashBent = "Servers/Bent";
    public static string CopyToClipBent = "Copyed to clipboardBent";
    public static string JustOneDotBent = ".Bent";
    public static string DownloadFailedBent = "Downloading failed. Please try again!Bent";
    public static string TwoDotsBent = ":Bent";
    public static string textureBent = "textureBent";
    public static string SeedSlashBent = "Seed/Bent";
    public static string Blur_SizBent = "_SizeBent";
    public static string Unexpectederror3dotsBent = "Unexpected error...Bent";
    public static string InstallSkinBent = "Install skinBent";
    public static string TereBent = "-Bent";
    public static string SlSoundsBent = "/sounds/Bent";
    public static string SoundsBent = "sounds/Bent";
    public static string ServersBent = "servers/Bent";
    public static string SlServersBent = "/servers/Bent";
    public static string AllFilterBent = "AllBent";
    public static string FavoriteFilterBent = "FavoriteBent";
    public static string NewFilterBent = "Last addedBent";
    public static string NoInternetBent = "No internet connectionBent";

    public const string _headsPathBent = "assets/resources/3d/heads/";
    public const string _bodyPathBent = "assets/resources/3d/body_textures/torso/";
    public const string _armsPathBent = "assets/resources/3d/body_textures/arms/";
    public const string _legsPathBent = "assets/resources/3d/body_textures/Legs/";
    public const string _acsBackPathBent = "assets/resources/3d/acessories/back/";
    public const string _acsHandsPathBent = "assets/resources/3d/acessories/hand/";
    public const string _acsHeadPathBent = "assets/resources/3d/acessories/head/";
    public const string LegsBent = "LegsBent";
    public const string AcsBent = "AccessoriesBent";
    public const string BodyBent = "BodyBent";
    public const string ArmsBent = "ArmsBent";
    public const string HeadsBent = "HeadBent";
    public const string Just3DBent = "3DBent";
    public const string DotJpgBent = ".jpgBent";
    public const string SlResSlBent = "/Resources/Bent";
    public const string DotJPGBent = ".JPGBent";
    public const string DotfbxBent = ".fbxBent";
    public const string DotFBXBent = ".FBXBent";
    public const string CloneBent = "(Clone)Bent";

    
    private static void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
