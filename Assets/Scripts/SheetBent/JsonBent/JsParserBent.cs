using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class JsParserBent : MonoBehaviour
{
    public List<ContentDataModelBent> ParsedSkinsDataBent = new List<ContentDataModelBent>();
    [FormerlySerializedAs("ParsedCategoriesSkinsSCD")] public List<string> ParsedCategoriesSkinsBent = new List<string>();

    private bool trashForNothBent = true; // trash

    public void InitSkinsDataBent(Dictionary<string, Dictionary<string, Dictionary<string, string>>> skinsCategoriesBent)
    {
        ParsedCategoriesSkinsBent.Add(StrHoldeBent.GetEditedStrBent(StrHoldeBent.AllFilterBent));
        ParsedCategoriesSkinsBent.Add(StrHoldeBent.GetEditedStrBent(StrHoldeBent.NewFilterBent));

        foreach (var categoryBent in skinsCategoriesBent)
        {
            string catNameBent = categoryBent.Key;
            if (!ParsedCategoriesSkinsBent.Contains(catNameBent))
                ParsedCategoriesSkinsBent.Add(catNameBent);

            foreach (var itemBent in categoryBent.Value)
            {
                var modelBent = new SkinsDataModelBent(itemBent.Value, catNameBent);
                ParsedSkinsDataBent.Add(modelBent);
            }
        }

        trashForNothBent = !trashForNothBent; // trash
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
