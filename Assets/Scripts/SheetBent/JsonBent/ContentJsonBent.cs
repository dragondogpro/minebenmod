using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public class ContentJsonBent
{
    [JsonProperty("3b0okl8ygp")] public Dictionary<string, Dictionary<string, Dictionary<string, string>>> SkinCategoriesBent { get; set; }

    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    public static ContentJsonBent FromJsonBent(string jsonBent)
    {
        var resaultBent = JsonConvert.DeserializeObject<ContentJsonBent>(jsonBent, ConverterBent.SettingsBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        return resaultBent;
    }
}

internal static class ConverterBent
{
    public static readonly JsonSerializerSettings SettingsBent = new JsonSerializerSettings
    {

        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
        DateParseHandling = DateParseHandling.None,
        Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
    };
    
    private static void TrashMethodBent1()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}

