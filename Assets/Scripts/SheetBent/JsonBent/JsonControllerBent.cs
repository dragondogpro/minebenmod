using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TMPro;
using UnityEngine.Serialization;

public class JsonControllerBent : BaseControllerBent
{
    [FormerlySerializedAs("_dirOfLightSCD")] [SerializeField] private Light _dirOfLightBent;
    [FormerlySerializedAs("_jsonDataSCD")] [SerializeField] private string[] _jsonDataBent;
    [FormerlySerializedAs("_jsToDownloadSCD")] [SerializeField] private string[] _jsToDownloadBent;
    [FormerlySerializedAs("_creatorContentSCD")] [SerializeField] private string _creatorContentBent;
    [FormerlySerializedAs("_skins3D")]
    [Space]
    [SerializeField] private Skins3dControllerBent _skins3DBent;
    [FormerlySerializedAs("_stubOnLoadingMCC")]
    [Space]
    [SerializeField] private GameObject _stubOnLoadingBent;
    [Space]
    public FBstorageBent fbStBent;
    [FormerlySerializedAs("StubPictureSCD")] public GameObject StubPictureBent;
    [FormerlySerializedAs("_contentAssetsSCD")] public AssetBundle _contentAssetsBent;
    

    public Dictionary<string, int> SavedFavoritesBent = new Dictionary<string, int>();
    public List<Skins3DdataBent> _saved3dSkinsBent = new List<Skins3DdataBent>();

    private JsParserBent _parserBent;

    public override void Awake()
    {
        Application.targetFrameRate = 60;
        base.Awake();
        _parserBent = GetComponent<JsParserBent>();
        _jsonDataBent = new string[_jsToDownloadBent.Length];

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void StartWorkingBent()
    {
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        FbStorGetLinkBent();
        LoadSavedDataBent();
    }
    private async void FbStorGetLinkBent()
    {
        string[] finalLinksBent = new string[_jsToDownloadBent.Length];
        for (int iBent = 0; iBent < _jsToDownloadBent.Length; iBent++)
        {
            finalLinksBent[iBent] = await FBstorageBent.GetFirebaseLinkBent(_jsToDownloadBent[iBent]);
        }
        //StartCoroutine(DownloadJsonSCD(finalLinksSCD));
        GetFromJsoneBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private async void GetFromJsoneBent()
    {
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        await DownloadContentAssetBudnleBent();
        var bundleRequestBent = AssetBundle.LoadFromFileAsync(Application.persistentDataPath + "/creatorcontentBent".Replace("Bent", ""));
        while (bundleRequestBent.assetBundle == null)
            await Task.Yield();

        _contentAssetsBent = bundleRequestBent.assetBundle;

        _skins3DBent.InitBent(_saved3dSkinsBent);
        
        Destroy(_stubOnLoadingBent);
        
        
        StubPictureBent.gameObject.SetActive(false);
        _dirOfLightBent.enabled = true;
        
        
    }
    private async Task DownloadContentAssetBudnleBent()
    {
        if (!File.Exists(Application.persistentDataPath + "/creatorcontentBent".Replace("Bent", "")))
        {
            float fileSezBent = 14.177f;
            StubPictureBent.transform.GetChild(1).gameObject.SetActive(true);
            TextMeshProUGUI warningBent = StubPictureBent.transform.GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>();
            string _fbUrlBent = await FBstorageBent.GetFirebaseLinkBent("creatorcontentBent".Replace("Bent", ""));
            using UnityWebRequest _uwrBent = UnityWebRequest.Get(_fbUrlBent);
           
            var _webRequestBent = _uwrBent.SendWebRequest();
            
            while (!_webRequestBent.isDone)
            {
                string donloadedBent = (fileSezBent * _uwrBent.downloadProgress).ToString();
                if (donloadedBent.Length > 4)
                    donloadedBent = donloadedBent.Remove(4);
                warningBent.text = $"{donloadedBent} / {fileSezBent} mb";
                await Task.Yield();
            }
            if (_uwrBent.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(_uwrBent.error);
            }
            else
            {
                await File.WriteAllBytesAsync(Application.persistentDataPath + "/creatorcontentBent".Replace("Bent", ""), _uwrBent.downloadHandler.data);
            }
            StubPictureBent.transform.GetChild(1).gameObject.SetActive(false);
        }

        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private async void SavePartsDataBent()
    {
        List<List<string>> _pathsBent = new List<List<string>>();
        for (int iBent = 0; iBent < 7; iBent++)
        {
            _pathsBent.Add(new List<string>());
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._headsPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJPGBent))
                _pathsBent[0].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._bodyPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJPGBent))
                _pathsBent[1].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._armsPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJPGBent))
                _pathsBent[2].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._legsPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJpgBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotJPGBent))
                _pathsBent[3].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._acsBackPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotfbxBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotFBXBent))
                _pathsBent[4].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._acsHandsPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotfbxBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotFBXBent))
                _pathsBent[5].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        foreach (var someStringBent in Directory.GetFiles(Application.dataPath + StrHoldeBent.GetEditedStrBent(StrHoldeBent.SlResSlBent) + SkinMakerSettingsBent._acsHeadPathBent))
        {
            if (Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotfbxBent) || Path.GetExtension(someStringBent) == StrHoldeBent.GetEditedStrBent(StrHoldeBent.DotFBXBent))
                _pathsBent[6].Add(Path.GetFileName(someStringBent));
            await Task.Delay(10);
        }
        string savedListBent = JsonConvert.SerializeObject(_pathsBent);
        await File.WriteAllTextAsync(Application.dataPath + "/Resources/dataList.txtBent".Replace("Bent", ""), savedListBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }


    public static async Task<string> DownloadObjectBent(string _urlBent, string _savePathBent)
    {
        string _fbUrlBent = await FBstorageBent.GetFirebaseLinkBent(_urlBent);
        using UnityWebRequest _uwrBent = UnityWebRequest.Get(_fbUrlBent);

        var _webRequestBent = _uwrBent.SendWebRequest();

        while (!_webRequestBent.isDone)
        {
            await Task.Yield();
        }

        if (string.IsNullOrEmpty(_uwrBent.error))
        {
            if (!Directory.Exists(Path.GetDirectoryName(_savePathBent)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(_savePathBent));
            }

            File.WriteAllBytes(_savePathBent, _uwrBent.downloadHandler.data);
            return _savePathBent;
        }
        else
        {
            Debug.Log(_uwrBent.error);
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================

        return null;
    }

    public void LoadSavedDataBent()
    {
        if (!Directory.Exists(Path.GetDirectoryName(Application.persistentDataPath + "/JsonSCD/SavedFavoritesSCDBent".Replace("Bent", ""))))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(Application.persistentDataPath + "/JsonSCD/SavedFavoritesSCDBent".Replace("Bent", "")));
        }
        if (File.Exists(Application.persistentDataPath + "/JsonSCD/SavedFavoritesSCDBent".Replace("Bent", "")))
        {
            string dataBent = File.ReadAllText(Application.persistentDataPath + "/JsonSCD/SavedFavoritesSCDBent".Replace("Bent", ""));
            SavedFavoritesBent = JsonConvert.DeserializeObject<Dictionary<string, int>>(dataBent);
        }
        if (File.Exists(Application.persistentDataPath + "/JsonSCD/Saved3dSkinsSCDBent".Replace("Bent", "")))
        {
            string dataBent = File.ReadAllText(Application.persistentDataPath + "/JsonSCD/Saved3dSkinsSCDBent".Replace("Bent", ""));
            _saved3dSkinsBent = JsonConvert.DeserializeObject<List<Skins3DdataBent>>(dataBent);
        }
        
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void SaveDataBent()
    {
        string dataBent = JsonConvert.SerializeObject(SavedFavoritesBent);
        await File.WriteAllTextAsync(Application.persistentDataPath + "/JsonSCD/SavedFavoritesSCDBent".Replace("Bent", ""), dataBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<JsonControllerBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<JsonControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
