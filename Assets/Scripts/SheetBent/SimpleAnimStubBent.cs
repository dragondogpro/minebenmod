using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SimpleAnimStubBent : MonoBehaviour
{
    [FormerlySerializedAs("_stubPictureSCD")] [SerializeField] private Image _stubPictureBent;
    [FormerlySerializedAs("_loadingIndicatorSCD")] [SerializeField] private Image _loadingIndicatorBent;

    private bool IsAnimationGoingBent = false;

    private void OnEnable()
    {
        if (!IsAnimationGoingBent)
        {
            IsAnimationGoingBent = true;
            LeanTween.rotateZ(this._loadingIndicatorBent.gameObject, 720f, 1f)
                .setLoopClamp();
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnDisable()
    {
        if (IsAnimationGoingBent)
        {
            IsAnimationGoingBent = false;
            LeanTween.cancel(this._loadingIndicatorBent.gameObject);
            var testingThisBent = this._loadingIndicatorBent.rectTransform.rotation;
            testingThisBent.z = 0;
            this._loadingIndicatorBent.rectTransform.rotation = testingThisBent;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
