using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
using System.Threading.Tasks;

public class GetImageBent : MonoBehaviour
{
    public async static Task<Texture2D> FromCacheBent(string pathToImageBent)
    {
        int lastIndexOfDotBent = pathToImageBent.LastIndexOf(StrHoldeBent.GetEditedStrBent(StrHoldeBent.JustOneDotBent), StringComparison.Ordinal);
        int prefixLengthBent = pathToImageBent.Length - lastIndexOfDotBent;
        string postfixBent = pathToImageBent.Remove(lastIndexOfDotBent, prefixLengthBent);

        var readedAsyncBent = File.ReadAllBytesAsync(pathToImageBent);

        while (readedAsyncBent.Result == null)
        {
            await Task.Yield();
        }

        byte[] dataBent = readedAsyncBent.Result;
        Texture2D textureBent;

        byte[] widthByteBent = new byte[] { dataBent[dataBent.Length - 8], dataBent[dataBent.Length - 7], dataBent[dataBent.Length - 6], dataBent[dataBent.Length - 5] };
        byte[] heighthByteBent = new byte[] { dataBent[dataBent.Length - 4], dataBent[dataBent.Length - 3], dataBent[dataBent.Length - 2], dataBent[dataBent.Length - 1] };

        int widthSCD = BitConverter.ToInt32(widthByteBent);
        int heightSCD = BitConverter.ToInt32(heighthByteBent);

        if ((float)((dataBent.Length - 8)) / (float)(widthSCD * heightSCD) >= 1)
            textureBent = new Texture2D(widthSCD, heightSCD, TextureFormat.ETC2_RGBA8, false);
        else
            textureBent = new Texture2D(widthSCD, heightSCD, TextureFormat.ETC2_RGB, false);

        textureBent.LoadRawTextureData(dataBent);
        textureBent.Apply(false, false);

        return textureBent;
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    public async static Task<Texture2D> WebRequestImageBent(string _urlBent, string _savePathBent, GameObject cardGObjBent)
    {
        if (!Directory.Exists(Path.GetDirectoryName(_savePathBent)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(_savePathBent));
        }
        //UnityWebRequest _uwrSCD = UnityWebRequest.Get(JsonControllerSCD.DomenServerSCD + _urlSCD);
        UnityWebRequest _uwrBent = UnityWebRequest.Get(await FBstorageBent.GetFirebaseLinkBent(_urlBent));
        UnityWebRequestAsyncOperation _webRequestBent = _uwrBent.SendWebRequest();

        while (!_webRequestBent.isDone)
        {
            await Task.Yield();
        }
        if (string.IsNullOrEmpty(_uwrBent.error))
        {
            byte[] dataBent = _uwrBent.downloadHandler.data;
            Texture2D textureBent;
            byte[] widthByteBent = new byte[] { dataBent[dataBent.Length - 8], dataBent[dataBent.Length - 7], dataBent[dataBent.Length - 6], dataBent[dataBent.Length - 5] };
            byte[] heighthByteBent = new byte[] { dataBent[dataBent.Length - 4], dataBent[dataBent.Length - 3], dataBent[dataBent.Length - 2], dataBent[dataBent.Length - 1] };
            int widthBent = BitConverter.ToInt32(widthByteBent);
            int heightBent = BitConverter.ToInt32(heighthByteBent);

            if ((float)((dataBent.Length - 8) * 6) / (float)(widthBent * heightBent * 6) >= 1)
                textureBent = new Texture2D(widthBent, heightBent, TextureFormat.ETC2_RGBA8, false);
            else
                textureBent = new Texture2D(widthBent, heightBent, TextureFormat.ETC2_RGB, false);

            textureBent.LoadRawTextureData(dataBent);
            textureBent.Apply(false, false);

            var someBent = File.WriteAllBytesAsync(_savePathBent, _uwrBent.downloadHandler.data);
            while (!someBent.IsCompleted)
            {
                await Task.Yield();
            }
            if (!cardGObjBent.activeSelf)
            {
                return null;
            }
            return textureBent;
        }
        else
        {
            Debug.Log(_uwrBent.error + " : . . . Bent" + cardGObjBent.name);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
        }
        return null;
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
