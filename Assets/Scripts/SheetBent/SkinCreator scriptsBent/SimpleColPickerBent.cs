using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SimpleColPickerBent : MonoBehaviour
{
    [FormerlySerializedAs("_sampleTextSliderSCD")] [SerializeField] private BoxSliderBent sampleTextSliderBent;
    [FormerlySerializedAs("_camSCD")]
    [Space]
    [SerializeField] private Camera _camBent;
    [FormerlySerializedAs("_colPickerSliderSCD")] [SerializeField] private Slider _colPickerSliderBent;
    [FormerlySerializedAs("_sampleTextureSCD")] [SerializeField] private RawImage _sampleTextureBent;
    [FormerlySerializedAs("_cancelButtonSCD")]
    [Space]
    [SerializeField] private Button _cancelButtonBent;
    [FormerlySerializedAs("_applylButtonSCD")] [SerializeField] private Button _applylButtonBent;

    private SkinEditorSettingsBent _editorSetttingsBent = null;
    private Color _colorPickerColBent;
    private float _s_valueBent;
    private float _v_valueBent;

    private int textureWidthBent = 128;
    private int textureHeightBent = 128;

    private void Start()
    {
        _colPickerSliderBent.onValueChanged.AddListener(_ => RegenerateSampleTextureBent());
        _cancelButtonBent.onClick.AddListener(() => CloseColorPickerBent(false));
        _applylButtonBent.onClick.AddListener(() => CloseColorPickerBent(true));
        sampleTextSliderBent.OnValueChangedBent.AddListener(SetSaturationAndGammaBent);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private void Update()
    {
        if (!this.gameObject.activeSelf)
            return;
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    public void OpenColorPickerBent(float curColStateBent, SkinEditorSettingsBent editorBent)
    {
        this.gameObject.SetActive(true);
        if (_editorSetttingsBent == null)
            _editorSetttingsBent = editorBent;
        _colPickerSliderBent.value = curColStateBent;
        _colorPickerColBent = _editorSetttingsBent._selectedColorSCDBent;
        float colBent, whiteToColorBent, gammaBent;
        Color.RGBToHSV(_colorPickerColBent, out colBent, out whiteToColorBent, out gammaBent);
        _s_valueBent = whiteToColorBent;
        _v_valueBent = gammaBent;
        RegenerateSampleTextureBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private void CloseColorPickerBent(bool allpyChangesBent)
    {
        if (allpyChangesBent)
        {
            _colorPickerColBent = Color.HSVToRGB(_colPickerSliderBent.value, _s_valueBent, _v_valueBent);
            _editorSetttingsBent.ApplyChangesBent(_colorPickerColBent);
            this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(false);
        }
        ContainerBent.GetBent<DrawingControllerBent>()._currentEditTypeBent = EditTypeBent.AbleRotBent;
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void RegenerateSampleTextureBent()
    {
        if (_sampleTextureBent.texture != null)
            DestroyImmediate(_sampleTextureBent.texture);

        var textureBent = new Texture2D(textureWidthBent, textureHeightBent);
        textureBent.wrapMode = TextureWrapMode.Clamp;
        textureBent.hideFlags = HideFlags.DontSave;

        for (int sBent = 0; sBent < textureWidthBent; sBent++)
        {
            Color[] colorsBent = new Color[textureHeightBent];
            for (int vBent = 0; vBent < textureHeightBent; vBent++)
            {
                colorsBent[vBent] = Color.HSVToRGB(_colPickerSliderBent.value, (float)sBent / textureWidthBent, (float)vBent / textureHeightBent);
            }
            textureBent.SetPixels(sBent, 0, 1, textureHeightBent, colorsBent);
        }
        textureBent.Apply();

        _sampleTextureBent.texture = textureBent;
        _colorPickerColBent = Color.HSVToRGB(_colPickerSliderBent.value, _s_valueBent, _v_valueBent);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private void SetSaturationAndGammaBent(float S_Bent, float V_Bent)
    {
        _s_valueBent = S_Bent;
        _v_valueBent = V_Bent;
        _colorPickerColBent = Color.HSVToRGB(_colPickerSliderBent.value, _s_valueBent, _v_valueBent);
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
