using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
using UnityEngine.UI;

[AddComponentMenu("UI/BoxSliderSCD", 35)]
[RequireComponent(typeof(RectTransform))]
public class BoxSliderBent : Selectable, IDragHandler, IInitializePotentialDragHandler, ICanvasElement
{
	public enum DirectionBent
	{
		LeftToRightBent,
		RightToLeftBent,
		BottomToTopBent,
		TopToBottomBent,
	}

	[Serializable]
	public class BoxSliderBentEvent : UnityEvent<float, float> { }

	[FormerlySerializedAs("m_HandleRectSCD")] [SerializeField]
	private RectTransform m_HandleRectBent;
	public RectTransform HandleRectBent { get { return m_HandleRectBent; } set { if (SetClassBent(ref m_HandleRectBent, value)) { UpdateCachedReferencesBent(); UpdateVisualsBent(); } } }

	[FormerlySerializedAs("m_MinValueSCD")]
	[Space(6)]

	[SerializeField]
	private float m_MinValueBent = 0;
	public float MinValueBent { get { return m_MinValueBent; } set { if (SetStructBent(ref m_MinValueBent, value)) { SetBent(m_ValueBent); SetYBent(m_ValueYBent); UpdateVisualsBent(); } } }

	[FormerlySerializedAs("m_MaxValueSCD")] [SerializeField]
	private float m_MaxValueBent = 1;
	public float MaxValueBent { get { return m_MaxValueBent; } set { if (SetStructBent(ref m_MaxValueBent, value)) { SetBent(m_ValueBent); SetYBent(m_ValueYBent); UpdateVisualsBent(); } } }

	[FormerlySerializedAs("m_WholeNumbersSCD")] [SerializeField]
	private bool m_WholeNumbersBent = false;
	public bool WholeNumbersBent { get { return m_WholeNumbersBent; } set { if (SetStructBent(ref m_WholeNumbersBent, value)) { SetBent(m_ValueBent); SetYBent(m_ValueYBent); UpdateVisualsBent(); } } }

	[FormerlySerializedAs("m_ValueSCD")] [SerializeField]
	private float m_ValueBent = 1f;
	public float ValueBent
	{
		get
		{
			if (WholeNumbersBent)
				return Mathf.Round(m_ValueBent);
			return m_ValueBent;
		}
		set
		{
			SetBent(value);
		}
	}

	public float normalizedValueBent
	{
		get
		{
			if (Mathf.Approximately(MinValueBent, MaxValueBent))
				return 0;
			return Mathf.InverseLerp(MinValueBent, MaxValueBent, ValueBent);
		}
		set
		{
			this.ValueBent = Mathf.Lerp(MinValueBent, MaxValueBent, value);
		}
	}

	[FormerlySerializedAs("m_ValueYSCD")] [SerializeField]
	private float m_ValueYBent = 1f;
	public float ValueYBent
	{
		get
		{
			if (WholeNumbersBent)
				return Mathf.Round(m_ValueYBent);
			return m_ValueYBent;
		}
		set
		{
			SetYBent(value);
		}
	}

	public float normalizedValueYSCD
	{
		get
		{
			if (Mathf.Approximately(MinValueBent, MaxValueBent))
				return 0;
			return Mathf.InverseLerp(MinValueBent, MaxValueBent, ValueYBent);
		}
		set
		{
			this.ValueYBent = Mathf.Lerp(MinValueBent, MaxValueBent, value);
		}
	}

	[FormerlySerializedAs("m_OnValueChangedSCD")]
	[Space(6)]
	[SerializeField]
	private BoxSliderBentEvent mOnValueChangedBent = new BoxSliderBentEvent();
	public BoxSliderBentEvent OnValueChangedBent { get { return mOnValueChangedBent; } set { mOnValueChangedBent = value; } }

	private Transform m_HandleTransformSCD;
	private RectTransform m_HandleContainerRectBent;

	private Vector2 m_OffsetBent = Vector2.zero;

    private DrivenRectTransformTracker m_TrackerBent;

    float stepSizeBent { get { return WholeNumbersBent ? 1 : (MaxValueBent - MinValueBent) * 0.1f; } }

	protected BoxSliderBent()
	{ }

#if UNITY_EDITOR
	protected override void OnValidate()
	{
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
		
		base.OnValidate();

		if (WholeNumbersBent)
		{
			m_MinValueBent = Mathf.Round(m_MinValueBent);
			m_MaxValueBent = Mathf.Round(m_MaxValueBent);
		}

		//Onvalidate is called before OnEnabled. We need to make sure not to touch any other objects before OnEnable is run.
		if (IsActive())
		{
			UpdateCachedReferencesBent();
			SetBent(m_ValueBent, false);
			SetYBent(m_ValueYBent, false);
			// Update rects since other things might affect them even if value didn't change.
			UpdateVisualsBent();
		}

#if UNITY_2018_3_OR_NEWER

		if (!UnityEditor.PrefabUtility.IsPartOfPrefabAsset(this) && !Application.isPlaying)
			CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);

#else

            var prefabType = UnityEditor.PrefabUtility.GetPrefabType(this);
			if (prefabType != UnityEditor.PrefabType.Prefab && !Application.isPlaying)
				CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
#endif
	}
#endif // if UNITY_EDITOR

	public virtual void Rebuild(CanvasUpdate executingBent)
	{
#if UNITY_EDITOR
		if (executingBent == CanvasUpdate.Prelayout)
			OnValueChangedBent.Invoke(ValueBent, ValueYBent);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
#endif
	}

	public void LayoutComplete()
	{
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public void GraphicUpdateComplete()
	{
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public static bool SetClassBent<T>(ref T currentValueBent, T newValueBent) where T : class
	{
		if ((currentValueBent == null && newValueBent == null) || (currentValueBent != null && currentValueBent.Equals(newValueBent)))
			return false;

		currentValueBent = newValueBent;
		return true;
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public static bool SetStructBent<T>(ref T currentValueBent, T newValueBent) where T : struct
	{
		if (currentValueBent.Equals(newValueBent))
			return false;

		currentValueBent = newValueBent;
		return true;
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	protected override void OnEnable()
	{
		base.OnEnable();
		UpdateCachedReferencesBent();
		SetBent(m_ValueBent, false);
		SetYBent(m_ValueYBent, false);
		// Update rects since they need to be initialized correctly.
		UpdateVisualsBent();
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	protected override void OnDisable()
	{
		m_TrackerBent.Clear();
		base.OnDisable();
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void UpdateCachedReferencesBent()
	{

		if (m_HandleRectBent)
		{
			m_HandleTransformSCD = m_HandleRectBent.transform;
			if (m_HandleTransformSCD.parent != null)
				m_HandleContainerRectBent = m_HandleTransformSCD.parent.GetComponent<RectTransform>();
		}
		else
		{
			m_HandleContainerRectBent = null;
		}
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void SetBent(float inputBent)
	{
		SetBent(inputBent, true);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void SetBent(float inputBent, bool sendCallbackBent)
	{
		float newValueBent = Mathf.Clamp(inputBent, MinValueBent, MaxValueBent);
		if (WholeNumbersBent)
			newValueBent = Mathf.Round(newValueBent);

		if (m_ValueBent.Equals(newValueBent))
			return;

		m_ValueBent = newValueBent;
		UpdateVisualsBent();
		if (sendCallbackBent)
			mOnValueChangedBent.Invoke(newValueBent, ValueYBent);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void SetYBent(float inputBent)
	{
		SetYBent(inputBent, true);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void SetYBent(float inputBent, bool sendCallbackBent)
	{
		float newValueSCD = Mathf.Clamp(inputBent, MinValueBent, MaxValueBent);
		if (WholeNumbersBent)
			newValueSCD = Mathf.Round(newValueSCD);

		if (m_ValueYBent.Equals(newValueSCD))
			return;

		m_ValueYBent = newValueSCD;
		UpdateVisualsBent();
		if (sendCallbackBent)
			mOnValueChangedBent.Invoke(ValueBent, newValueSCD);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}


	protected override void OnRectTransformDimensionsChange()
	{
		base.OnRectTransformDimensionsChange();
		UpdateVisualsBent();
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	enum AxisBent
	{
		Horizontal = 0,
		Vertical = 1
	}

	private void UpdateVisualsBent()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			UpdateCachedReferencesBent();
#endif

		m_TrackerBent.Clear();

		if (m_HandleContainerRectBent != null)
		{
			m_TrackerBent.Add(this, m_HandleRectBent, DrivenTransformProperties.Anchors);
			Vector2 anchorMinBent = Vector2.zero;
			Vector2 anchorMaxBent = Vector2.one;
			anchorMinBent[0] = anchorMaxBent[0] = (normalizedValueBent);
			anchorMinBent[1] = anchorMaxBent[1] = (normalizedValueYSCD);

			m_HandleRectBent.anchorMin = anchorMinBent;
			m_HandleRectBent.anchorMax = anchorMaxBent;
		}
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	void UpdateDragBent(PointerEventData eventDataBent, Camera camBent)
	{
		RectTransform clickRectBent = m_HandleContainerRectBent;
		if (clickRectBent != null && clickRectBent.rect.size[0] > 0)
		{
			Vector2 localCursorBent;
			if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(clickRectBent, eventDataBent.position, camBent, out localCursorBent))
				return;
			localCursorBent -= clickRectBent.rect.position;

			float valBent = Mathf.Clamp01((localCursorBent - m_OffsetBent)[0] / clickRectBent.rect.size[0]);
			normalizedValueBent = (valBent);

			float valYBent = Mathf.Clamp01((localCursorBent - m_OffsetBent)[1] / clickRectBent.rect.size[1]);
			normalizedValueYSCD = (valYBent);

		}
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	private bool MayDragBent(PointerEventData eventDataBent)
	{
		return IsActive() && IsInteractable() && eventDataBent.button == PointerEventData.InputButton.Left;
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public override void OnPointerDown(PointerEventData eventDataBent)
	{
		if (!MayDragBent(eventDataBent))
			return;

		base.OnPointerDown(eventDataBent);

		m_OffsetBent = Vector2.zero;
		if (m_HandleContainerRectBent != null && RectTransformUtility.RectangleContainsScreenPoint(m_HandleRectBent, eventDataBent.position, eventDataBent.enterEventCamera))
		{
			Vector2 localMousePosBent;
			if (RectTransformUtility.ScreenPointToLocalPointInRectangle(m_HandleRectBent, eventDataBent.position, eventDataBent.pressEventCamera, out localMousePosBent))
				m_OffsetBent = localMousePosBent;
			m_OffsetBent.y = -m_OffsetBent.y;
		}
		else
		{
			UpdateDragBent(eventDataBent, eventDataBent.pressEventCamera);
		}
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public virtual void OnDrag(PointerEventData eventDataBent)
	{
		if (!MayDragBent(eventDataBent))
			return;

		UpdateDragBent(eventDataBent, eventDataBent.pressEventCamera);
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}

	public virtual void OnInitializePotentialDrag(PointerEventData eventDataBent)
	{
		eventDataBent.useDragThreshold = false;
		
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}
	
	private void TrashMethodBent()
	{
		var boolBent = true;
		var boolBent2 = true;
		var boolBent3 = true;

		if (boolBent != boolBent2 && boolBent2 != boolBent3)
		{
			boolBent = true;
			boolBent2 = true;
			boolBent3 = true;
		}
	}
}
