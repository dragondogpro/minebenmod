using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BakeMeshColiderBent : MonoBehaviour
{
    private SkinnedMeshRenderer meshRendererBent;
    private MeshCollider colliderBent;

    private void Start()
    {
        if (TryGetComponent<SkinnedMeshRenderer>(out SkinnedMeshRenderer meshBent))
        {

            UpdateColliderBent();
        }
        else if (TryGetComponent<MeshRenderer>(out MeshRenderer mRendererBent))
        {

            //Texture2D instTextSCD = mRendererSCD.material.mainTexture as Texture2D;
            //if (mRendererSCD.material.mainTexture == null)
            //    return;
            //var textscd = Instantiate(instTextSCD);
            //mRendererSCD.material.mainTexture = textscd;
        }

        ContainerBent.GetBent<DrawingControllerBent>().AfterAnimPreviewBent += UpdateColliderBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    private void UpdateColliderBent()
    {
        TryGetComponent<SkinnedMeshRenderer>(out SkinnedMeshRenderer meshBent);
        meshRendererBent = meshBent;
        colliderBent = GetComponent<MeshCollider>();
        Mesh colliderMeshBent = new Mesh();
        meshRendererBent.BakeMesh(colliderMeshBent);
        colliderBent.sharedMesh = null;
        colliderBent.sharedMesh = colliderMeshBent;
        

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

}
