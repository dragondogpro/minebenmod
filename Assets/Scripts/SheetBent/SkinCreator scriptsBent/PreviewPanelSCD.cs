using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations;
using System.IO;
using System.Threading.Tasks;
using IOSBridge;
using UnityEngine.Serialization;

public class PreviewPanelSCD : MonoBehaviour
{
    [FormerlySerializedAs("_modelSCD")] [SerializeField] private SkinLiftingBent modelBent;
    [FormerlySerializedAs("_makerSCD")] [SerializeField] private SkinMakerSettingsBent makerBent;
    [FormerlySerializedAs("_screenShotSCD")] [SerializeField] private ScreenShotHelperBent screenShotBent;
    [FormerlySerializedAs("_panelsToHideSCD")]
    [Space]
    [SerializeField] private GameObject[] _panelsToHideBent;
    [FormerlySerializedAs("_animButtondsSCD")] [SerializeField] private Button[] _animButtondsBent;
    [FormerlySerializedAs("_modelAnimSCD")] [SerializeField] private Animator _modelAnimBent;
    [FormerlySerializedAs("_backButtonSCD")]
    [Space]
    [SerializeField] private Button _backButtonBent;
    [FormerlySerializedAs("_optionsButtonSCD")] [SerializeField] private Button _optionsButtonBent;
    [FormerlySerializedAs("_playPauseButtonSCD")] [SerializeField] private Button _playPauseButtonBent;
    [FormerlySerializedAs("_makeScreenshotSCD")] [SerializeField] private Button _makeScreenshotBent;
    [FormerlySerializedAs("_toHideWhenSavingSCD")]
    [Space]
    [SerializeField] private GameObject[] _toHideWhenSavingBent;
    [FormerlySerializedAs("_drawControllerSCD")] [SerializeField] private DrawingControllerBent drawControllerBent;
    [FormerlySerializedAs("_optionsSCD")] [SerializeField] private OptionsMenuBent optionsBent;

    private bool IsAnimPlayingBent = false;
    private List<GameObject> _toTurnOnBackBent = new List<GameObject>();
    

    private void Awake()
    {
        for (int iBent = 0; iBent < _animButtondsBent.Length; iBent++)
        {
            int indxBent = iBent;
            if (iBent == 15)
                indxBent = iBent + 1;
            AddListenOnButtBent(_animButtondsBent[iBent], indxBent);
        }
        _backButtonBent.onClick.AddListener(HidePreviewPanelBent);
        _playPauseButtonBent.onClick.AddListener(PlayPauseBent);
        _makeScreenshotBent.onClick.AddListener(SaveToGalleryBent);

        _optionsButtonBent.onClick.AddListener(() => optionsBent.gameObject.SetActive(true));

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public enum AnimationsBent
    {
        ChickenDanceBent = 0,
        GangamStyleBent = 1,
        RanningManBent = 2,
        SillyDancingBent = 3,
        SittingTalkingBent = 4,
        SwaggerWalkBent = 5,
        SwingDancingBent = 6,
        ToughWalkBent = 7,
        WALKING2Bent = 8,
        WarriorIdleBent = 9,
        WaveBent = 10,
        YesBent = 11,
        Arm_danceBent = 12,
        HappyWalkBent = 13,
        Idle2Bent = 14,
        Idle_lookUPBent = 15,
        MoonwalkBent = 16
    }
    private void Start()
    {
        var creatorBent = ContainerBent.GetBent<CreatorControllerBent>();
        creatorBent.OnDeleteSkinBent += HidePreviewPanelBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
   
    private void AddListenOnButtBent(Button butToListenBent, int indxBent)
    {
        Button tempBent = butToListenBent;
        tempBent.onClick.AddListener(() => PlayAnimationBent(indxBent));
        tempBent = null;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ShowPreviewPanelBent()
    {
        ContainerBent.GetBent<CreatorControllerBent>().SaveSkinBent();
        this.gameObject.SetActive(true);
        foreach (var itemBent in _panelsToHideBent)
        {
            if (itemBent.activeSelf)
            {
                _toTurnOnBackBent.Add(itemBent);
                itemBent.SetActive(false);
            }
        }
  

        _modelAnimBent.enabled = true;
        IsAnimPlayingBent = true;

        modelBent.LiftModelBent(35, 0.2f);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private async void HidePreviewPanelBent()
    {
        await HideTaskBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async Task HideTaskBent()
    {
        if (!this.gameObject.activeSelf)
            return;

        PlayAnimationBent(14);
        foreach (var itemBent in _toTurnOnBackBent)
        {
            itemBent.SetActive(true);
        }
        _toTurnOnBackBent.Clear();
        if (!makerBent.IsCurrentPanelBent)
            modelBent.LiftModelBent(50, 0.2f);
        else
            modelBent.LiftModelBent(0, 0.2f);
        await Task.Delay(200);
        _modelAnimBent.enabled = false;
        this.gameObject.SetActive(false);
        drawControllerBent.AfterAnimPreviewBent?.Invoke();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void SaveToGalleryBent()
    {
        var skinDataBent = ContainerBent.GetBent<CreatorControllerBent>();
        string nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent;
        int indxIfNeededBent = 0;
        while (File.Exists(Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "")))
        {
            nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent + $" ({indxIfNeededBent++})";
        }
        string savePathBent = Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "");;
        
        if (!Directory.Exists(Path.GetDirectoryName(savePathBent)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(savePathBent));
        }
        await screenShotBent.GetScreenShotBent(savePathBent);
        IOStoUnityBridge.SaveImageToAlbum(savePathBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void PlayAnimationBent(int animIndxBent)
    {
        IsAnimPlayingBent = false;
        PlayPauseBent();
        _modelAnimBent.SetTrigger($"{animIndxBent}");
        _playPauseButtonBent.transform.GetChild(0).gameObject.SetActive(true);
        IsAnimPlayingBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void PlayPauseBent()
    {
        if (IsAnimPlayingBent)
        {
            _playPauseButtonBent.transform.GetChild(0).gameObject.SetActive(false);
            _modelAnimBent.speed = 0;
            IsAnimPlayingBent = false;
        }
        else
        {
            _playPauseButtonBent.transform.GetChild(0).gameObject.SetActive(true);
            _modelAnimBent.speed = 1;
            IsAnimPlayingBent = true;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnFavoriteBent()
    {
        var _mainBent = ContainerBent.GetBent<CreatorControllerBent>();

        _mainBent.SkinDataEditBent.IsFavoriteBent = !_mainBent.SkinDataEditBent.IsFavoriteBent;

        var jsonBent = ContainerBent.GetBent<JsonControllerBent>();
        if (!_mainBent.SkinDataEditBent.IsFavoriteBent)
            jsonBent.SavedFavoritesBent.Remove(_mainBent.SkinDataEditBent.SkinIdBent.ToString());
        else
            jsonBent.SavedFavoritesBent.Add(_mainBent.SkinDataEditBent.SkinIdBent.ToString(), 1);

        jsonBent.SaveDataBent();
        _mainBent.OnFavoriteChangeBent?.Invoke(_mainBent.SkinDataEditBent.IsFavoriteBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
