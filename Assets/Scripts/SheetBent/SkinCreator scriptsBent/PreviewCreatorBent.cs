using UnityEngine;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.Serialization;

public class PreviewCreatorBent : MonoBehaviour
{
    [FormerlySerializedAs("_fullModelSCd")] [SerializeField] private GameObject _fullModelBent;
    [FormerlySerializedAs("_acsHeadSCD")] [SerializeField] private Transform _acsHeadBent;
    [FormerlySerializedAs("_acsBackSCD")] [SerializeField] private Transform _acsBackBent;
    [FormerlySerializedAs("_acsHandsSCD")] [SerializeField] private Transform _acsHandsBent;
    [FormerlySerializedAs("_camSCD")]
    [Space]
    [SerializeField] private Camera _camBent;
    [FormerlySerializedAs("_headSCD")] [SerializeField] private GameObject _headBent;
    [FormerlySerializedAs("_bodySCD")] [SerializeField] private Renderer _bodyBent;
    [FormerlySerializedAs("_leftArmSCD")] [SerializeField] private Renderer _leftArmBent;
    [FormerlySerializedAs("_rightArmSCD")] [SerializeField] private Renderer _rightArmBent;
    [FormerlySerializedAs("_leftLegCD")] [SerializeField] private Renderer _leftLegBent;
    [FormerlySerializedAs("_rightLegSCD")] [SerializeField] private Renderer _rightLegBent;
    [FormerlySerializedAs("_accesoriesHeadSCD")]
    [Space]
    [SerializeField] private Transform _accesoriesHeadBent;
    [FormerlySerializedAs("_accesoriesBackSCD")] [SerializeField] private Transform _accesoriesBackBent;
    [FormerlySerializedAs("_accessoriesHandsSCD")] [SerializeField] private Transform _accessoriesHandsBent;
    [FormerlySerializedAs("_defMaterialSCD")]
    [Space]
    [SerializeField] private Material _defMaterialBent;

    private string savePathBent;
    private AssetBundle _contenBent;
    
    public async Task<bool> CreatePreviewBent(string pathToSaveBent, Skins3DdataBent dataBentBent, bool isAcsBent)
    {
        await GenPreviewBent(pathToSaveBent, dataBentBent, isAcsBent);
        return true;
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    public async Task GenPreviewBent(string pathToSaveBent, Skins3DdataBent dataBent, bool isAcsBent)
    {
        await Task.CompletedTask;
        Debug.Log(pathToSaveBent);
        if (_contenBent == null)
            _contenBent = ContainerBent.GetBent<JsonControllerBent>()._contentAssetsBent;
        savePathBent = pathToSaveBent;
        await RebuildModelFOrPreviewBent(dataBent, isAcsBent);

        _camBent.gameObject.SetActive(true);
        RenderTexture screenTexturBent = new RenderTexture(Screen.width, Screen.height, 16);
        _camBent.targetTexture = screenTexturBent;
        RenderTexture.active = screenTexturBent;
        _camBent.Render();
        Texture2D renderedTextureBent = new Texture2D(Screen.width, Screen.height);
        renderedTextureBent.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        RenderTexture.active = null;
        _camBent.targetTexture = null;
        byte[] byteArrayBent = renderedTextureBent.EncodeToPNG();
        await File.WriteAllBytesAsync(savePathBent, byteArrayBent);
        Destroy(renderedTextureBent);
        Destroy(screenTexturBent);
        _camBent.gameObject.SetActive(false);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private async Task RebuildModelFOrPreviewBent(Skins3DdataBent dataBentBent, bool isAcsBent)
    {
        await Task.CompletedTask;

        if (_headBent.transform.childCount > 1)
            Destroy(_headBent.transform.GetChild(1).gameObject);
        string fbxFileBent = "";
        if (isAcsBent)
        {
            _fullModelBent.SetActive(false);
            
            _acsBackBent.gameObject.SetActive(false);
            
            if (dataBentBent.AccessoriesBackPathBent != null)
            {
                
                _acsBackBent.gameObject.SetActive(true);
                string facbxFileBent = dataBentBent.AccessoriesBackPathBent;
                if (_acsBackBent.childCount > 0)
                    Destroy(_acsBackBent.GetChild(0).gameObject);
                ResourceRequest acs1AsyncBent = _contenBent.LoadAssetAsync<GameObject>(facbxFileBent);
                while (!acs1AsyncBent.isDone)
                    await Task.Yield();
                Instantiate(acs1AsyncBent.asset as GameObject, _acsBackBent);
            }
            if (dataBentBent.AccessoriesHandPathBent != null)
            {
                
                _accessoriesHandsBent.gameObject.SetActive(true);
                string facbxFileBent = dataBentBent.AccessoriesHandPathBent;
                if (_accessoriesHandsBent.childCount > 0)
                    Destroy(_acsBackBent.GetChild(0).gameObject);
                ResourceRequest acs2AsyncBent = _contenBent.LoadAssetAsync<GameObject>(facbxFileBent);
                while (!acs2AsyncBent.isDone)
                    await Task.Yield();
                Instantiate(acs2AsyncBent.asset as GameObject, _accessoriesHandsBent);
            }
            if (dataBentBent.AccessoriesHeadPathBent != null)
            {
                
                _acsHeadBent.gameObject.SetActive(true);
                string facbxFileBent = dataBentBent.AccessoriesHeadPathBent;
                Transform spotForHeadBent = _acsHeadBent.GetChild(1);
                if (spotForHeadBent.childCount > 0)
                {
                    while (spotForHeadBent.childCount != 0)
                    {
                        Destroy(spotForHeadBent.GetChild(0).gameObject);
                    }
                }
                    
                ResourceRequest acs3AsyncBent = _contenBent.LoadAssetAsync<GameObject>(facbxFileBent);
                while (!acs3AsyncBent.isDone)
                    await Task.Yield();
                Instantiate(acs3AsyncBent.asset as GameObject, spotForHeadBent);
            }
        }
        else
        {
            
            _fullModelBent.SetActive(true);
            fbxFileBent = dataBentBent.HeadPathBent.Remove(dataBentBent.HeadPathBent.Length - 4);

            if (fbxFileBent.Split("/")[0] != "assetsBent".Replace("Bent", ""))
                fbxFileBent = StrHoldeBent._headsPathBent + Path.GetFileNameWithoutExtension(dataBentBent.HeadPathBent);

            GameObject newHeadBent = Instantiate(_contenBent.LoadAsset<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", "")) as GameObject, _headBent.transform);
            newHeadBent.SetActive(false);
            //newHeadSCD.layer = 3;
            //newHeadSCD.tag = "AbleToRotSCD";
            //newHeadSCD.AddComponent<BakeMeshColiderSCD>();

            Texture2D textureBent = new Texture2D(2, 2);
            if (dataBentBent.HeadPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
                textureBent = new Texture2D(compressedBent.width, compressedBent.height);
                textureBent.SetPixels(compressedBent.GetPixels());
                textureBent.Apply();
            }
            else
            {
                textureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.HeadPathBent).Result, false);
            }
            Renderer headRendBent = newHeadBent.GetComponent<Renderer>();
            headRendBent.material = Instantiate(_defMaterialBent);
            headRendBent.material.color = Color.white;
            headRendBent.material.mainTexture = Instantiate(textureBent);
            newHeadBent.transform.localScale = Vector3.one;
            newHeadBent.transform.localRotation = Quaternion.Euler(Vector3.zero);
            newHeadBent.SetActive(true);

            string f1bxFileBent = dataBentBent.LeftLegPathBent;
            Texture2D t1extureBent = new Texture2D(2, 2);
            if (dataBentBent.LeftLegPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f1bxFileBent);
                t1extureBent = new Texture2D(compressedBent.width, compressedBent.height);
                t1extureBent.SetPixels(compressedBent.GetPixels());
                t1extureBent.Apply();
            }
            else
                t1extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.LeftLegPathBent).Result, false);
            if (_leftLegBent.material.mainTexture != null)
                Destroy(_leftLegBent.material.mainTexture);
            _leftLegBent.material.mainTexture = Instantiate(t1extureBent);
            _leftLegBent.material.color = Color.white;
            string f2bxFileBent = dataBentBent.RightLegPathBent;
            Texture2D t2extureBent = new Texture2D(2, 2);
            if (dataBentBent.RightLegPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f2bxFileBent);
                t2extureBent = new Texture2D(compressedBent.width, compressedBent.height);
                t2extureBent.SetPixels(compressedBent.GetPixels());
                t2extureBent.Apply();
            }
            else
                t2extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.RightLegPathBent).Result, false);
            if (_rightLegBent.material.mainTexture != null)
                Destroy(_rightLegBent.material.mainTexture);
            _rightLegBent.material.mainTexture = Instantiate(t2extureBent);
            _rightLegBent.material.color = Color.white;

            string f3bxFileBent = dataBentBent.LeftArmPathBent;
            Texture2D t3extureBent = new Texture2D(2, 2);
            if (dataBentBent.LeftArmPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f3bxFileBent);
                t3extureBent = new Texture2D(compressedBent.width, compressedBent.height);
                t3extureBent.SetPixels(compressedBent.GetPixels());
                t3extureBent.Apply();
            }
            else
                t3extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.LeftArmPathBent).Result, false);
            if (_leftArmBent.material.mainTexture != null)
                Destroy(_leftArmBent.material.mainTexture);
            _leftArmBent.material.mainTexture = Instantiate(t3extureBent);
            _leftArmBent.material.color = Color.white;
            string f4bxFileBent = dataBentBent.RightArmPathBent;
            Texture2D t4extureBent = new Texture2D(2, 2);
            if (dataBentBent.RightArmPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f4bxFileBent);
                t4extureBent = new Texture2D(compressedBent.width, compressedBent.height);
                t4extureBent.SetPixels(compressedBent.GetPixels());
                t4extureBent.Apply();
            }
            else
                t4extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.RightArmPathBent).Result, false);
            if (_rightArmBent.material.mainTexture != null)
                Destroy(_rightArmBent.material.mainTexture);
            _rightArmBent.material.mainTexture = Instantiate(t4extureBent);
            _rightArmBent.material.color = Color.white;

            string f5bxFileBent = dataBentBent.BodyPathBent;
            Texture2D t5extureBent = new Texture2D(2, 2);
            if (dataBentBent.BodyPathBent.Contains("assets/Bent".Replace("Bent", "")))
            {
                Texture2D compressedBent = _contenBent.LoadAsset<Texture2D>(f5bxFileBent);
                t5extureBent = new Texture2D(compressedBent.width, compressedBent.height);
                t5extureBent.SetPixels(compressedBent.GetPixels());
                t5extureBent.Apply();
            }
            else
                t5extureBent.LoadImage(File.ReadAllBytesAsync(dataBentBent.BodyPathBent).Result, false);
            if (_bodyBent.material.mainTexture != null)
                Destroy(_bodyBent.material.mainTexture);
            _bodyBent.material.mainTexture = Instantiate(t5extureBent);
            _bodyBent.material.color = Color.white;

            if (_accesoriesBackBent.childCount != 0)
                Destroy(_accesoriesBackBent.GetChild(0).gameObject);
            if (!string.IsNullOrEmpty(dataBentBent.AccessoriesBackPathBent))
            {
                string f6bxFileBent = dataBentBent.AccessoriesBackPathBent.Remove(dataBentBent.AccessoriesBackPathBent.Length - 4);
                Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accesoriesBackBent);
            }
            if (_accessoriesHandsBent.childCount != 0)
                Destroy(_accessoriesHandsBent.GetChild(0).gameObject);
            if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHandPathBent))
            {
                string f6bxFileBent = dataBentBent.AccessoriesHandPathBent.Remove(dataBentBent.AccessoriesHandPathBent.Length - 4);
                Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accessoriesHandsBent);
            }
            if (_accesoriesHeadBent.childCount != 0)
                Destroy(_accesoriesHeadBent.GetChild(0).gameObject);
            if (!string.IsNullOrEmpty(dataBentBent.AccessoriesHeadPathBent))
            {
                string f6bxFileBent = dataBentBent.AccessoriesHeadPathBent.Remove(dataBentBent.AccessoriesHeadPathBent.Length - 4);
                Instantiate(_contenBent.LoadAsset<GameObject>(f6bxFileBent + ".fbxBent".Replace("Bent", "")), _accesoriesHeadBent);
            }
        }

        //_acsHeadSCD.gameObject.SetActive(false);
        //_acsBackSCD.gameObject.SetActive(false);
        //_accessoriesHandsSCD.gameObject.SetActive(false);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
