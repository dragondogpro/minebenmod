using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Threading.Tasks;
using UnityEngine.Serialization;

public class SkinRotationBent : MonoBehaviour
{
    [FormerlySerializedAs("_YaxisSCD")] [SerializeField] private Transform _YaxisBent;
    [FormerlySerializedAs("_camSCD")] [SerializeField] private Camera _camBent;
    [FormerlySerializedAs("_yRotSpeedSCD")]
    [Space]
    [SerializeField] private float _yRotSpeedBent = 260f;
    [FormerlySerializedAs("_xRotSpeedSCD")] [SerializeField] private float _xRotSpeedBent = 120f;
    [FormerlySerializedAs("_scaleMultiplSCD")] [SerializeField] private float _scaleMultiplBent = 1;

    private float _currModelScaleBent = 1;
    private float _startingPositionxBent;
    private float _startingPositionyBent;
    private Vector2 _onBeginPosBent;
    private bool IsYrotatingBent = false;
    private bool IsXrotaitingBent = false;
    private bool IsNeededToCheckBent = true;
    private DrawingControllerBent _mainContrBent;

    private void Start()
    {
        _mainContrBent = ContainerBent.GetBent<DrawingControllerBent>();
        ContainerBent.GetBent<CreatorControllerBent>().OnEditExitBent += ResetRotBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ResetRotBent()
    {
        _YaxisBent.rotation = Quaternion.Euler(Vector3.zero);
        this.transform.rotation = Quaternion.Euler(Vector3.zero);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void Update()
    {
        
        if (Input.touchCount > 0 && _mainContrBent._currentEditTypeBent == EditTypeBent.AbleRotBent)
        {
            RaycastHit hitBent;
            if (!Physics.Raycast(_camBent.ScreenPointToRay(Input.mousePosition), out hitBent))
                return;
            if (!hitBent.collider.CompareTag("AbleToRotSCD"))
                return;
            TryToRotateBent();

            // trash =====================================================
            var boolBent = true;
            var boolBent2 = true;
            var boolBent3 = true;

            if (boolBent != boolBent2 && boolBent2 != boolBent3)
            {
                boolBent = true;
                boolBent2 = true;
                boolBent3 = true;
            }
        // trash =====================================================
        }
    }
    
    private async void TryToRotateBent()
    {
        Touch touchBent = Input.GetTouch(0);
        switch (touchBent.phase)
        {
            case TouchPhase.Began:
                _startingPositionxBent = touchBent.position.x;
                _startingPositionyBent = touchBent.position.y;
                _onBeginPosBent = new Vector2(touchBent.position.x, touchBent.position.y);
                IsNeededToCheckBent = true;
                break;
            case TouchPhase.Moved:

                if (IsXrotaitingBent)
                {
                    if (_startingPositionxBent > touchBent.position.x)
                    {
                        this.transform.Rotate(Vector3.up, _xRotSpeedBent * Time.deltaTime);
                    }
                    else if (_startingPositionxBent < touchBent.position.x)
                    {
                        this.transform.Rotate(Vector3.up, -_xRotSpeedBent * Time.deltaTime);
                    }
                }
                else if (IsYrotatingBent)
                {
                    if (_startingPositionyBent > touchBent.position.y)
                    {
                        _YaxisBent.Rotate(Vector3.left, _yRotSpeedBent * Time.deltaTime);
                    }
                    else if (_startingPositionyBent < touchBent.position.y)
                    {
                        _YaxisBent.Rotate(Vector3.left, -_yRotSpeedBent * Time.deltaTime);
                    }
                }
                if (!IsNeededToCheckBent)
                    break;
                float xLanthBent = _onBeginPosBent.x - touchBent.position.x;
                float yLangthBent = _onBeginPosBent.y - touchBent.position.y;
                while (Mathf.Abs(xLanthBent) < 10 && Mathf.Abs(yLangthBent) < 20)
                {
                    await Task.Yield();
                }
                IsXaxisRotatingBent(xLanthBent, yLangthBent);
                break;
            case TouchPhase.Stationary:
                _startingPositionxBent = touchBent.position.x;
                _startingPositionyBent = touchBent.position.y;
                //await Task.Delay(1000);
                //if (_startingPositionxSCD != touchSCD.position.x && _startingPositionySCD != touchSCD.position.y)
                //    break;
                //IsXrotaitingSCD = false;
                //IsYrotatingSCD = false;
                //IsNeededToCheckSCD = true;
                break;
            case TouchPhase.Ended:
                IsNeededToCheckBent = false;
                IsXrotaitingBent = false;
                IsYrotatingBent = false;
                break;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void IsXaxisRotatingBent(float xBent, float yBent)
    {
        if (!this.gameObject.activeSelf)
            return;
        IsNeededToCheckBent = false;
        if (Mathf.Abs(xBent) > Mathf.Abs(yBent))
        {
            IsXrotaitingBent = true;
            IsYrotatingBent = false;
        }
        else
        {
            IsXrotaitingBent = false;
            IsYrotatingBent = true;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}

