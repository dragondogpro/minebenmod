using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class SkinEditorSettingsBent : BaseControllerBent
{
    [FormerlySerializedAs("_colorSCD")] [SerializeField] private Slider _colorBent;
    [FormerlySerializedAs("_colSliderBgSCD")] [SerializeField] private Image _colSliderBgBent;
    [FormerlySerializedAs("_whiteToColorSCD")] [SerializeField] private Slider _whiteToColorBent;
    [FormerlySerializedAs("_saturationBgSCD")] [SerializeField] private RawImage _saturationBgBent;
    [FormerlySerializedAs("_brushSizeSCD")] [SerializeField] private Slider _brushSizeBent;
    [FormerlySerializedAs("_colorButtonSCD")]
    [Space]
    [SerializeField] private Button _colorButtonBent;
    [FormerlySerializedAs("_buttonColIndicSCD")] [SerializeField] private Image _buttonColIndicBent;
    [FormerlySerializedAs("_brushButtonSCD")] [SerializeField] private Button _brushButtonBent;
    [FormerlySerializedAs("_fillButtonSCD")] [SerializeField] private Button _fillButtonBent;
    [FormerlySerializedAs("_eraserButtonSCD")] [SerializeField] private Button _eraserButtonBent;
    [SerializeField] private Color _selectedColorBent;
    [FormerlySerializedAs("_colPickerSCD")]
    [Space]
    [SerializeField] private SimpleColPickerBent colPickerBent;
    [FormerlySerializedAs("_undoButtonSCD")]
    [Space]
    [SerializeField] private Button _undoButtonBent;
    [FormerlySerializedAs("_redoButtonSCD")] [SerializeField] private Button _redoButtonBent;
    [FormerlySerializedAs("_previewButtonSCD")] [SerializeField] private Button _previewButtonBent;
    [FormerlySerializedAs("_previewPanelSCD")] [SerializeField] private PreviewPanelSCD _previewPanelBent;

    [FormerlySerializedAs("_selectedColorSCD")] public Color _selectedColorSCDBent;

    private float _gammaValueBent = 1;
    [FormerlySerializedAs("mainDrawSCD")] [SerializeField] private DrawingControllerBent mainDrawBent;
    private GameObject _curSelectedBent;

    public override void Awake()
    {
        base.Awake();

        _previewButtonBent.onClick.AddListener(() => { _previewPanelBent.ShowPreviewPanelBent(); mainDrawBent._currentEditTypeBent = EditTypeBent.AbleRotBent; });
        _brushSizeBent.value = 4;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void Start()
    {
        _colorBent.onValueChanged.AddListener(_ => SetColorChangesBent());
        _whiteToColorBent.onValueChanged.AddListener(_ => SetColorChangesBent());
        _brushSizeBent.onValueChanged.AddListener(_ => mainDrawBent.ChangeBrushSizeBent((int)_brushSizeBent.value));
        SetColorChangesBent();
        _brushButtonBent.onClick.AddListener(() => ChangeEditModBent(EditTypeBent.BrushBent));
        _fillButtonBent.onClick.AddListener(() => ChangeEditModBent(EditTypeBent.FillBent));
        _colorButtonBent.onClick.AddListener(() => ChangeEditModBent(EditTypeBent.ColorBent));
        _eraserButtonBent.onClick.AddListener(() => ChangeEditModBent(EditTypeBent.EaserBent));
        CreateSaturationPalleteBent();

        _undoButtonBent.onClick.AddListener(UndoStepBent);
        _redoButtonBent.onClick.AddListener(RedoStepBent);

        UpdateActiveStepsBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void DelTextBent()
    {
        if (mainDrawBent == null)
            mainDrawBent = ContainerBent.GetBent<DrawingControllerBent>();
        mainDrawBent.CurrStepBent = 0;
        foreach (var anyStepBent in mainDrawBent._editedStepsBent)
        {
            Destroy(anyStepBent.EndTextureBent);
            Destroy(anyStepBent.StTextureBent);
        }

        Destroy(mainDrawBent._originTexturesBent[0].orTextBent);
        Destroy(mainDrawBent._originTexturesBent[1].orTextBent);
        Destroy(mainDrawBent._originTexturesBent[2].orTextBent);
        Destroy(mainDrawBent._originTexturesBent[3].orTextBent);
        Destroy(mainDrawBent._originTexturesBent[4].orTextBent);
        Destroy(mainDrawBent._originTexturesBent[5].orTextBent);

    }
    public void ResetStepLogBent(List<OriginsDataBent> newOriginsBent)
    {
        if (mainDrawBent == null)
            mainDrawBent = ContainerBent.GetBent<DrawingControllerBent>();
        mainDrawBent.CurrStepBent = 0;
        foreach (var anyStepBent in mainDrawBent._editedStepsBent)
        {
            Destroy(anyStepBent.EndTextureBent);
            Destroy(anyStepBent.StTextureBent);
        }
        mainDrawBent._originTexturesBent = newOriginsBent;
        mainDrawBent.CurrStepBent = 0;
        mainDrawBent.firstBent = false;
        ChangeEditModBent(EditTypeBent.AbleRotBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ApplyChangesBent(Color collorToSetBent)
    {
        float colBent, whiteToColorBent, gammaBent;
        Color.RGBToHSV(collorToSetBent, out colBent, out whiteToColorBent, out gammaBent);
        _colorBent.value = colBent;
        _whiteToColorBent.value = whiteToColorBent;
        _gammaValueBent = gammaBent;
        CreateSaturationPalleteBent();
        SetColorChangesBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void SetColorChangesBent()
    {
        if (mainDrawBent == null)
            mainDrawBent = ContainerBent.GetBent<DrawingControllerBent>();
        _selectedColorSCDBent = Color.HSVToRGB(_colorBent.value, _whiteToColorBent.value, _gammaValueBent);
        mainDrawBent.ChangeColorBent(_selectedColorSCDBent);
        _buttonColIndicBent.color = _selectedColorSCDBent;
        CreateSaturationPalleteBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void CHangeSelectedEditModeBent(GameObject toCHangeBent)
    {
        _curSelectedBent?.SetActive(false);
        _curSelectedBent = toCHangeBent;
        _curSelectedBent.SetActive(true);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void CreateSaturationPalleteBent()
    {
        if (_saturationBgBent.texture != null)
            DestroyImmediate(_saturationBgBent.texture);

        var textureBent = new Texture2D(128, 128);
        textureBent.wrapMode = TextureWrapMode.Clamp;
        textureBent.hideFlags = HideFlags.DontSave;

        for (int sBent = 0; sBent < 128; sBent++)
        {
            Color[] colorsBent = new Color[128];
            for (int vBent = 0; vBent < 128; vBent++)
            {
                colorsBent[vBent] = Color.HSVToRGB(_colorBent.value, (float)sBent / 128, (float)vBent / 128);
            }
            textureBent.SetPixels(sBent, 0, 1, 128, colorsBent);
        }
        textureBent.Apply();

        _saturationBgBent.texture = textureBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void ChangeEditModBent(EditTypeBent toChangeBent)
    {
        if (mainDrawBent._currentEditTypeBent == toChangeBent)
        {
            mainDrawBent._currentEditTypeBent = EditTypeBent.AbleRotBent;
            toChangeBent = EditTypeBent.AbleRotBent;
        }
            
        
        switch (toChangeBent)
        {
            case EditTypeBent.AbleRotBent:
                _brushButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                _fillButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                _eraserButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                _brushButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                _fillButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                _eraserButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                _colorButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                break;
            case EditTypeBent.ColorBent:
                mainDrawBent._currentEditTypeBent = EditTypeBent.ColorBent;
                colPickerBent.OpenColorPickerBent(_colorBent.value, this);
                break;
            case EditTypeBent.BrushBent:
                mainDrawBent._currentEditTypeBent = EditTypeBent.BrushBent;
                
                if (_brushButtonBent.transform.GetChild(0).gameObject.activeSelf)
                {
                    _brushButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _brushButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                }
                else
                {
                    _brushButtonBent.transform.GetChild(0).gameObject.SetActive(true);
                    _brushButtonBent.transform.GetChild(2).gameObject.SetActive(false);
                    _eraserButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _fillButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _fillButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _eraserButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                }
                break;
            case EditTypeBent.FillBent:
                mainDrawBent._currentEditTypeBent = EditTypeBent.FillBent;
                
                if (_fillButtonBent.transform.GetChild(0).gameObject.activeSelf)
                {
                    _fillButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _fillButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                }
                else
                {
                    _fillButtonBent.transform.GetChild(0).gameObject.SetActive(true);
                    _fillButtonBent.transform.GetChild(2).gameObject.SetActive(false);
                    _brushButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _eraserButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _brushButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _eraserButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                }
              
                break;
            case EditTypeBent.EaserBent:
                mainDrawBent._currentEditTypeBent = EditTypeBent.EaserBent;

                if (_eraserButtonBent.transform.GetChild(0).gameObject.activeSelf)
                {
                    _eraserButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _eraserButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                }
                else
                {
                    _eraserButtonBent.transform.GetChild(0).gameObject.SetActive(true);
                    _eraserButtonBent.transform.GetChild(2).gameObject.SetActive(false);
                    _brushButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _fillButtonBent.transform.GetChild(0).gameObject.SetActive(false);
                    _brushButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                    _fillButtonBent.transform.GetChild(2).gameObject.SetActive(true);
                }
                
                break;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void UndoStepBent()
    {
        var stepsBent = mainDrawBent._editedStepsBent;
        var someBent = stepsBent[mainDrawBent.CurrStepBent];
        someBent.RendBent.material.mainTexture = Instantiate(someBent.StTextureBent);
        mainDrawBent.CurrStepBent -= 1;
        UpdateActiveStepsBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void RedoStepBent()
    {
        mainDrawBent.CurrStepBent += 1;
        var stepsBent = mainDrawBent._editedStepsBent;
        var someBent = stepsBent[mainDrawBent.CurrStepBent];
        someBent.RendBent.material.mainTexture = Instantiate(someBent.EndTextureBent);
        UpdateActiveStepsBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void UpdateActiveStepsBent()
    {
        if (mainDrawBent.CurrStepBent > 0)
            _undoButtonBent.interactable = true;
        else
            _undoButtonBent.interactable = false;
        if (mainDrawBent.CurrStepBent >= 0 && mainDrawBent._editedStepsBent.Count - 1 > mainDrawBent.CurrStepBent)
            _redoButtonBent.interactable = true;
        else
            _redoButtonBent.interactable = false;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<SkinEditorSettingsBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<SkinEditorSettingsBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
