using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.IO;
using System;
using System.Threading.Tasks;
using UnityEngine.Serialization;

public class PartUiViewBent : MonoBehaviour
{

    [FormerlySerializedAs("_cardButtonSCD")] [SerializeField] private Button _cardButtonBent;
    [FormerlySerializedAs("_skinModelSCD")]
    [Space]
    [SerializeField] private GameObject _skinModelBent;
    [FormerlySerializedAs("_accessorySpotSCD")] [SerializeField] private GameObject _accessorySpotBent;
    [FormerlySerializedAs("_headSCD")]
    [Space]
    [SerializeField] private GameObject _headBent;
    [FormerlySerializedAs("_bodySCD")] [SerializeField] private Renderer _bodyBent;
    [FormerlySerializedAs("_lHandSCD")] [SerializeField] private Renderer _lHandBent;
    [FormerlySerializedAs("_rHandSCD")] [SerializeField] private Renderer _rHandBent;
    [FormerlySerializedAs("_lLegSCD")] [SerializeField] private Renderer _lLegBent;
    [FormerlySerializedAs("_rLegSCD")] [SerializeField] private Renderer _rLegBent;
    [FormerlySerializedAs("_previewSCD")]
    [Space]
    [SerializeField] private RawImage _previewBent;
    [FormerlySerializedAs("_loadingIndicatorSCD")] [SerializeField] private Image _loadingIndicatorBent;
    [FormerlySerializedAs("HeadPathSCD")] [Space]

    public string HeadPathBent;
    [FormerlySerializedAs("BodyPathSCD")] public string BodyPathSCDBent;
    [FormerlySerializedAs("HandsPathSCD")] public string HandsPathBent;
    [FormerlySerializedAs("LegsPathSCD")] public string LegsPathSCDBent;
    [FormerlySerializedAs("AccessoryHeadPathSCD")] public string AccessoryHeadPathBent;
    [FormerlySerializedAs("AccessoryBackPathSCD")] public string AccessoryBackPathBent;
    [FormerlySerializedAs("AccessoryHandPathSCD")] public string AccessoryHandPathBent;
    [FormerlySerializedAs("CardIdSCD")] public int CardIdBent = 0;
    [FormerlySerializedAs("AccessoryIdSCD")] public int AccessoryIdBent = 0;
    public List<List<string>> PathsDuplicateBent;
    [FormerlySerializedAs("_thisTypeSCD")] public TypeOfPartBent _thisTypeBent;
    
    public delegate void OnPartCLick(PartUiViewBent cardClickedBent);
    public event OnPartCLick OnPaertClickedBent;

    [FormerlySerializedAs("IsDeleteRequestedSCD")] public bool IsDeleteRequestedBent = false;
    [FormerlySerializedAs("IsBuildedSCD")] public bool IsBuildedBent = false;
    [FormerlySerializedAs("IsReadySCD")] public bool IsReadyBent = false;
    public Skins3DdataBent ModelDataBent;
    private SkinMakerSettingsBent _makerBent;
    private JsonControllerBent _jsonBent;
    private CreatorControllerBent _creatorMainBent;
    private string _nameIdentifyBent;
    [FormerlySerializedAs("IsInProgressSCD")] public bool IsInProgressBent = false;
    private bool IsAcsBent = false;

    private void Awake()
    {
        _cardButtonBent.onClick.AddListener(() => OnPaertClickedBent(this));

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void SetMakerLinkBent(SkinMakerSettingsBent toSetBent)
    {
        _makerBent = toSetBent;
        _makerBent.OnCLosePartsBent += CleanMemoryBent;
        if (_creatorMainBent == null)
            _creatorMainBent = ContainerBent.GetBent<CreatorControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void CleanMemoryBent()
    {
        if (IsBuildedBent && !IsInProgressBent)
        {
            IsInProgressBent = true;
            IsDeleteRequestedBent = false;

            Destroy(_headBent.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.mainTexture);
            _headBent.transform.GetChild(1).gameObject.GetComponent<Renderer>().material.mainTexture = null;
            Destroy(_bodyBent.material.mainTexture);
            _bodyBent.material.mainTexture = null;
            Destroy(_lHandBent.material.mainTexture);
            _lHandBent.material.mainTexture = null;
            Destroy(_rHandBent.material.mainTexture);
            _rHandBent.material.mainTexture = null;
            Destroy(_lLegBent.material.mainTexture);
            _lLegBent.material.mainTexture = null;
            Destroy(_rLegBent.material.mainTexture);
            _rLegBent.material.mainTexture = null;


            IsInProgressBent = false;
            IsBuildedBent = false;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
        }
    }
    public void InitBent(List<List<string>> pathToPartDataBent, int indxBent, TypeOfPartBent thisTypeBent, int accessoryIndBent)
    {
        if (_makerBent == null)
            _makerBent = ContainerBent.GetBent<CreatorControllerBent>().skinMakerBent;
        if (_jsonBent == null)
            _jsonBent = ContainerBent.GetBent<JsonControllerBent>();
        ModelDataBent = new Skins3DdataBent();
        ModelDataBent.IsEditedBent = false;
        _thisTypeBent = thisTypeBent;
        PathsDuplicateBent = pathToPartDataBent;
        CardIdBent = indxBent;
        AccessoryIdBent = accessoryIndBent;

        switch (thisTypeBent)
        {
            case TypeOfPartBent.AllBodyBent:
                ModelDataBent.HeadPathBent = StrHoldeBent._headsPathBent + pathToPartDataBent[0][indxBent];
                ModelDataBent.BodyPathBent = StrHoldeBent._bodyPathBent + pathToPartDataBent[1][indxBent];
                ModelDataBent.LeftArmPathBent = StrHoldeBent._armsPathBent + pathToPartDataBent[2][indxBent];
                ModelDataBent.RightArmPathBent = StrHoldeBent._armsPathBent + pathToPartDataBent[2][indxBent];
                ModelDataBent.LeftLegPathBent = StrHoldeBent._legsPathBent + pathToPartDataBent[3][indxBent];
                ModelDataBent.RightLegPathBent = StrHoldeBent._legsPathBent + pathToPartDataBent[3][indxBent];
                _nameIdentifyBent = "allBodyBent".Replace("Bent", "") + indxBent;
                break;
            case TypeOfPartBent.HeadBent:
                ModelDataBent.HeadPathBent = StrHoldeBent._headsPathBent + pathToPartDataBent[0][indxBent];
                _nameIdentifyBent = Path.GetFileNameWithoutExtension(ModelDataBent.HeadPathBent);
                break;
            case TypeOfPartBent.LegsBent:
                ModelDataBent.LeftLegPathBent = StrHoldeBent._legsPathBent + pathToPartDataBent[3][indxBent];
                ModelDataBent.RightLegPathBent = StrHoldeBent._legsPathBent + pathToPartDataBent[3][indxBent];
                _nameIdentifyBent = Path.GetFileNameWithoutExtension(StrHoldeBent._legsPathBent + pathToPartDataBent[3][indxBent]);
                break;
            case TypeOfPartBent.ArmsBent:
                ModelDataBent.LeftArmPathBent = StrHoldeBent._armsPathBent + pathToPartDataBent[2][indxBent];
                ModelDataBent.RightArmPathBent = StrHoldeBent._armsPathBent + pathToPartDataBent[2][indxBent];
                _nameIdentifyBent = Path.GetFileNameWithoutExtension(StrHoldeBent._armsPathBent + pathToPartDataBent[2][indxBent]);
                break;
            case TypeOfPartBent.BodyBent:
                ModelDataBent.BodyPathBent = StrHoldeBent._bodyPathBent + pathToPartDataBent[1][indxBent];
                _nameIdentifyBent = Path.GetFileNameWithoutExtension(ModelDataBent.BodyPathBent);
                break;
            case TypeOfPartBent.AccessoriesBent:
                AccessoryIdBent = accessoryIndBent;
                if (AccessoryIdBent == 0)
                    return;
                _skinModelBent.SetActive(false);

                if (AccessoryIdBent == 1)
                {
                    ModelDataBent.AccessoriesBackPathBent = StrHoldeBent._acsBackPathBent + pathToPartDataBent[4][indxBent];
                    _nameIdentifyBent = Path.GetFileNameWithoutExtension(ModelDataBent.AccessoriesBackPathBent);
                } 
                if (AccessoryIdBent == 2)
                {
                    ModelDataBent.AccessoriesHandPathBent = StrHoldeBent._acsHandsPathBent + pathToPartDataBent[5][indxBent];
                    _nameIdentifyBent = Path.GetFileNameWithoutExtension(ModelDataBent.AccessoriesHandPathBent);
                } 
                if (AccessoryIdBent == 3)
                {
                    ModelDataBent.AccessoriesHeadPathBent = StrHoldeBent._acsHeadPathBent + pathToPartDataBent[6][indxBent];
                    _nameIdentifyBent = Path.GetFileNameWithoutExtension(ModelDataBent.AccessoriesHeadPathBent);
                }
                IsAcsBent = true;
                break;
        }
        IsReadyBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public async void BuildModelBent()
    {
        if (IsReadyBent && this.gameObject.activeSelf && _makerBent.IsCurrentPanelBent)
            await TaskBuildModelBent();
    }
    public async Task TaskBuildModelBent()
    {
        await Task.CompletedTask;
        if (!IsReadyBent)
            return;
        if (IsBuildedBent || IsInProgressBent)
            return;
        AssetBundle contBent = _jsonBent._contentAssetsBent;
        IsInProgressBent = true;

        if (_thisTypeBent == TypeOfPartBent.HeadBent || _thisTypeBent == TypeOfPartBent.AllBodyBent)
        {
            Destroy(_headBent.transform.GetChild(1).gameObject);
            string fbxFileBent = ModelDataBent.HeadPathBent.Remove(ModelDataBent.HeadPathBent.Length - 4);
            ResourceRequest headAsyncBent = contBent.LoadAssetAsync<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", ""));
            while (!headAsyncBent.isDone)
                await Task.Yield();
            GameObject newHeadBent = Instantiate(headAsyncBent.asset as GameObject, _headBent.transform);
            newHeadBent.SetActive(false);
            ResourceRequest headTexAsynscBent = contBent.LoadAssetAsync<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
            while (!headTexAsynscBent.isDone)
                await Task.Yield();
            Texture2D textureSCD = Instantiate(headTexAsynscBent.asset as Texture2D);

            newHeadBent.GetComponent<Renderer>().material.mainTexture = textureSCD;
            newHeadBent.transform.localScale = Vector3.one;
            newHeadBent.transform.localRotation = Quaternion.Euler(Vector3.zero);
            newHeadBent.SetActive(true);

        }
        if (_thisTypeBent == TypeOfPartBent.BodyBent || _thisTypeBent == TypeOfPartBent.AllBodyBent)
        {
            string fbxFileBent = ModelDataBent.BodyPathBent.Remove(ModelDataBent.BodyPathBent.Length - 4);
            ResourceRequest bodyAsyncBent = contBent.LoadAssetAsync<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
            while (!bodyAsyncBent.isDone)
                await Task.Yield();
            Texture2D textureBent = Instantiate(bodyAsyncBent.asset as Texture2D);
            _bodyBent.GetComponent<Renderer>().material.mainTexture = textureBent;
        }
        if (_thisTypeBent == TypeOfPartBent.ArmsBent || _thisTypeBent == TypeOfPartBent.AllBodyBent)
        {
            string fbxFileBent = ModelDataBent.LeftArmPathBent.Remove(ModelDataBent.LeftArmPathBent.Length - 4);
            ResourceRequest armsAsynSCD = contBent.LoadAssetAsync<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
            while (!armsAsynSCD.isDone)
                await Task.Yield();
            Texture2D textureBent = Instantiate(armsAsynSCD.asset as Texture2D);
            _lHandBent.GetComponent<Renderer>().material.mainTexture = textureBent;
            _rHandBent.GetComponent<Renderer>().material.mainTexture = textureBent;
        }
        if (_thisTypeBent == TypeOfPartBent.LegsBent || _thisTypeBent == TypeOfPartBent.AllBodyBent)
        {
            string fbxFileBent = ModelDataBent.LeftLegPathBent.Remove(ModelDataBent.LeftLegPathBent.Length - 4);
            ResourceRequest legsAsyncBent = contBent.LoadAssetAsync<Texture2D>(fbxFileBent + ".jpgBent".Replace("Bent", ""));
            while (!legsAsyncBent.isDone)
                await Task.Yield();
            Texture2D textureSCD = Instantiate(legsAsyncBent.asset as Texture2D);
            _lLegBent.GetComponent<Renderer>().material.mainTexture = textureSCD;
            _rLegBent.GetComponent<Renderer>().material.mainTexture = textureSCD;
        }
        if (_thisTypeBent == TypeOfPartBent.AccessoriesBent && AccessoryIdBent == 1)
        {
            string fbxFileBent = ModelDataBent.AccessoriesBackPathBent.Remove(ModelDataBent.AccessoriesBackPathBent.Length - 4);
            GameObject spotBent = _accessorySpotBent.transform.GetChild(0).gameObject;
            spotBent.SetActive(true);
            ResourceRequest acs1AsyncBent = contBent.LoadAssetAsync<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", ""));
            while (!acs1AsyncBent.isDone)
                await Task.Yield();
            Instantiate(acs1AsyncBent.asset as GameObject, spotBent.transform);
        }
        if (_thisTypeBent == TypeOfPartBent.AccessoriesBent && AccessoryIdBent == 2)
        {
            string fbxFileBent = ModelDataBent.AccessoriesHandPathBent.Remove(ModelDataBent.AccessoriesHandPathBent.Length - 4);
            GameObject spotBent = _accessorySpotBent.transform.GetChild(1).gameObject;
            spotBent.SetActive(true);
            ResourceRequest acs2AsyncBent = contBent.LoadAssetAsync<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", ""));
            while (!acs2AsyncBent.isDone)
                await Task.Yield();
            Instantiate(acs2AsyncBent.asset as GameObject, spotBent.transform);
        }
        if (_thisTypeBent == TypeOfPartBent.AccessoriesBent && AccessoryIdBent == 3)
        {
            string fbxFileBent = ModelDataBent.AccessoriesHeadPathBent.Remove(ModelDataBent.AccessoriesHeadPathBent.Length - 4);
            GameObject spotBent = _accessorySpotBent.transform.GetChild(2).gameObject;
            spotBent.SetActive(true);
            Transform spotForHeadBent = spotBent.transform.GetChild(1);
            ResourceRequest acs3AsyncBent = contBent.LoadAssetAsync<GameObject>(fbxFileBent + ".fbxBent".Replace("Bent", ""));
            while (!acs3AsyncBent.isDone)
                await Task.Yield();
            Instantiate(acs3AsyncBent.asset as GameObject, spotForHeadBent);
        }

        IsBuildedBent = true;
        IsInProgressBent = false;
        if (IsDeleteRequestedBent)
            CleanMemoryBent();
        
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
public enum TypeOfPartBent
{
    AllBodyBent = 0,
    HeadBent = 1,
    LegsBent = 2,
    ArmsBent = 3,
    BodyBent = 4,
    AccessoriesBent = 5
}
