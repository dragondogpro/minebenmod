using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEditor;
using TMPro;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using UnityEngine.Serialization;

public class SkinMakerSettingsBent : MonoBehaviour
{
    [FormerlySerializedAs("_previewSkinButtonSCD")] [SerializeField] private Button _previewSkinButtonBent;
    [FormerlySerializedAs("_previewPanelSCD")] [SerializeField] private PreviewPanelSCD _previewPanelBent;
    [FormerlySerializedAs("_undoButtonSCD")] [SerializeField] private Button _undoButton;
    [FormerlySerializedAs("_redoButtonSCD")] [SerializeField] private Button _redoButtonBent;
    [FormerlySerializedAs("_scrollButtons")]
    [Space]
    [SerializeField] private Button[] _scrollButtonsBent;
    [SerializeField] public  List<Skins3DdataBent> _makerStepsBent = new List<Skins3DdataBent>();
    [FormerlySerializedAs("_partsSCD")] [SerializeField] public List<PartUiViewBent> _partsBent = new List<PartUiViewBent>();
    [FormerlySerializedAs("_contentTransformSCD")] [SerializeField] private RectTransform _contentTransformBent;
    [FormerlySerializedAs("_partPrefabSCD")] [SerializeField] private PartUiViewBent partPrefabBent;
    [FormerlySerializedAs("_partsWindowSCD")]
    [Space]
    [SerializeField] private GameObject _partsWindowBent;
    [FormerlySerializedAs("_partsScrollSCD")] [SerializeField] private ScrollRect _partsScrollBent;
    [FormerlySerializedAs("_currTypeTitleSCD")] [SerializeField] private TextMeshProUGUI _currTypeTitleBent;
    [FormerlySerializedAs("_cancelPartsScrollSCD")] [SerializeField] private Button _cancelPartsScrollBent;
    [FormerlySerializedAs("_scrollPullingSCD")] [SerializeField] private SetTextureInScrollBent scrollPullingBent;

    public Action OnCLosePartsBent;

    public const string _headsPathBent = "3D/heads/";
    public const string _bodyPathBent = "3D/body_textures/torso/";
    public const string _armsPathBent = "3D/body_textures/arms/";
    public const string _legsPathBent = "3D/body_textures/Legs/";
    public const string _acsBackPathBent = "3D/acessories/back/";
    public const string _acsHandsPathBent = "3D/acessories/hand/";
    public const string _acsHeadPathBent = "3D/acessories/head/";
    private int currIndxPosBent = 0;
    [FormerlySerializedAs("CurrStepMakerSCD")] public int CurrStepMakerBent = 0;
    private PartCategoryBent _partCategoryBent;
    private DrawingControllerBent _drawContrBent;
    private CreatorControllerBent _creatorBent;

    //private List<string> _headTexturesSCD = new List<string>(); 0
    //private List<string> _bodyTexturesSCD = new List<string>(); 1
    //private List<string> _armsTexturesSCD = new List<string>(); 2
    //private List<string> _legsTexturesSCD = new List<string>(); 3
    //private List<string> _accesoriesPathsSCD = new List<string>(); 4

    private PartUiViewBent _currClickedBent;
    private List<List<string>> _partsPathDataBent = new List<List<string>>();
    private bool IsFIrstStepBent = true;
    [FormerlySerializedAs("IsCurrentPanelSCD")] public bool IsCurrentPanelBent = false;
    [FormerlySerializedAs("IsNotStepjuorneySCD")] public bool IsNotStepjuorneyBent = true;
    public Action OnPartScrollOpenBent;

    private bool IsAllBodyInitedBent = false;
    private bool IsHeadsInitedBent = false;
    private bool IsBodiesInitedBent = false;
    private bool IsArmsInitedBent = false;
    private bool IsLegsInitedBent = false;
    private bool IsAcsInitedBent = false;


    private void Awake()
    {
        _scrollButtonsBent[0].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.AllBodyBent));
        _scrollButtonsBent[1].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.HeadBent));
        _scrollButtonsBent[2].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.LegsBent));
        _scrollButtonsBent[3].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.ArmsBent));
        _scrollButtonsBent[4].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.BodyBent));
        _scrollButtonsBent[5].onClick.AddListener(() => ShowPartsContentBent(TypeOfPartBent.AccessoriesBent));
        _cancelPartsScrollBent.onClick.AddListener(() => { OnCLosePartsBent?.Invoke(); _partsWindowBent.gameObject.SetActive(false); Resources.UnloadUnusedAssets(); });
        _previewSkinButtonBent.onClick.AddListener(OnPreviewButClickBent);
        _undoButton.onClick.AddListener(UndoMakerBent);
        _redoButtonBent.onClick.AddListener(RedoMakerBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void InitMakerBent(Skins3DdataBent modelDataBent)
    {
        if (_drawContrBent == null)
            _drawContrBent = ContainerBent.GetBent<DrawingControllerBent>();
        if (_creatorBent == null)
            _creatorBent = ContainerBent.GetBent<CreatorControllerBent>();
        CurrStepMakerBent = 0;
        IsFIrstStepBent = true;
        _makerStepsBent = new List<Skins3DdataBent>();
        _partsPathDataBent = JsonConvert.DeserializeObject<List<List<string>>>(Resources.Load<TextAsset>("dataListBent".Replace("Bent", "")).text);
        if (IsFIrstStepBent)
        {
            _makerStepsBent.Add(modelDataBent);
            IsFIrstStepBent = false;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void UndoMakerBent()
    {
        IsNotStepjuorneyBent = false;
        CurrStepMakerBent--;
        _creatorBent.RebuildSkinBent(_makerStepsBent[CurrStepMakerBent]);
        UpdateActiveStepsBent();
        IsNotStepjuorneyBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void RedoMakerBent()
    {
        IsNotStepjuorneyBent = false;
        CurrStepMakerBent++;
        _creatorBent.RebuildSkinBent(_makerStepsBent[CurrStepMakerBent]);
        UpdateActiveStepsBent();
        IsNotStepjuorneyBent = true;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void UpdateActiveStepsBent()
    {
        if (CurrStepMakerBent > 0)
            _undoButton.interactable = true;
        else
            _undoButton.interactable = false;
        if (CurrStepMakerBent >= 0 && _makerStepsBent.Count - 1 > CurrStepMakerBent)
            _redoButtonBent.interactable = true;
        else
            _redoButtonBent.interactable = false;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnPreviewButClickBent()
    {
        _previewPanelBent.ShowPreviewPanelBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void SkinCardOnSkinClickedBent(PartUiViewBent partClickedBent)
    {
        
        _creatorBent.SetMakerChangesBent(partClickedBent);
        _partsWindowBent.gameObject.SetActive(false);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void ShowPartsContentBent(TypeOfPartBent typeToShowBent)
    {
        _partsScrollBent.verticalNormalizedPosition = 1;

        switch (typeToShowBent)
        {
            case TypeOfPartBent.AllBodyBent:
                _currTypeTitleBent.text = "All bodyBent".Replace("Bent", "");
                if (IsAllBodyInitedBent)
                    break;
                for (int iBent = 0; iBent < 20; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.AllBodyBent, 0);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                IsAllBodyInitedBent = true;
                break;
            case TypeOfPartBent.HeadBent:
                _currTypeTitleBent.text = StrHoldeBent.GetEditedStrBent(StrHoldeBent.HeadsBent);
                if (IsHeadsInitedBent)
                    break;
                for (int iBent = 0; iBent < _partsPathDataBent[0].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.HeadBent, 0);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;


                    currIndxPosBent++;
                }
                IsHeadsInitedBent = true;
                break;
            case TypeOfPartBent.LegsBent:
                _currTypeTitleBent.text = StrHoldeBent.GetEditedStrBent(StrHoldeBent.LegsBent);
                if (IsLegsInitedBent)
                    break;
                for (int iBent = 0; iBent < _partsPathDataBent[3].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.LegsBent, 0);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                IsLegsInitedBent = true;
                break;
            case TypeOfPartBent.ArmsBent:
                _currTypeTitleBent.text = StrHoldeBent.GetEditedStrBent(StrHoldeBent.ArmsBent);
                if (IsArmsInitedBent)
                    break;
                for (int iBent = 0; iBent < _partsPathDataBent[2].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.ArmsBent, 0);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                IsArmsInitedBent = true;
                break;
            case TypeOfPartBent.BodyBent:
                _currTypeTitleBent.text = StrHoldeBent.GetEditedStrBent(StrHoldeBent.BodyBent);
                if (IsBodiesInitedBent)
                    break;
                for (int iBent = 0; iBent < _partsPathDataBent[1].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.BodyBent, 0);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }

                IsBodiesInitedBent = true;
                break;
            case TypeOfPartBent.AccessoriesBent:
                _currTypeTitleBent.text = StrHoldeBent.GetEditedStrBent(StrHoldeBent.AcsBent);
                if (IsAcsInitedBent)
                    break;
                
                for (int iBent = 0; iBent < _partsPathDataBent[4].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.AccessoriesBent, 1);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                for (int iBent = 0; iBent < _partsPathDataBent[5].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.AccessoriesBent, 2);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                for (int iBent = 0; iBent < _partsPathDataBent[6].Count; iBent++)
                {
                    if (_partsBent.Count <= currIndxPosBent)
                    {
                        var prefabPardBent = Instantiate(partPrefabBent, _contentTransformBent);
                        _partsBent.Add(prefabPardBent);
                    }
                    _partsBent[currIndxPosBent].gameObject.SetActive(true);
                    _partsBent[currIndxPosBent].SetMakerLinkBent(this);
                    _partsBent[currIndxPosBent].InitBent(_partsPathDataBent, iBent, TypeOfPartBent.AccessoriesBent, 3);
                    _partsBent[currIndxPosBent].OnPaertClickedBent += SkinCardOnSkinClickedBent;
                    currIndxPosBent++;
                }
                IsAcsInitedBent = true;
                break;
        }

        foreach (var partViewBent in _partsBent)
        {
            if (partViewBent._thisTypeBent == typeToShowBent && partViewBent.IsReadyBent)
                partViewBent.gameObject.SetActive(true);
            else
                partViewBent.gameObject.SetActive(false);
        }

        _partsWindowBent.SetActive(true);
        OnPartScrollOpenBent?.Invoke();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
public enum PartCategoryBent
{ 
    HeadsBent = 0,
    BodyBent = 1,
    ArmsBent = 2,
    LegsBent = 3,
    AcsBackBent = 4,
    AcsHandsBent = 5,
    ACsHeadsBent = 6
}


