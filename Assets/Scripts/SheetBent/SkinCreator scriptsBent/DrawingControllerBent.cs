using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading.Tasks;
using System;
using UnityEngine.Serialization;

public class DrawingControllerBent : BaseControllerBent
{
    [FormerlySerializedAs("_camSCD")] [SerializeField] private Camera _camBent;
    [FormerlySerializedAs("_currentEditTypeSCD")] public EditTypeBent _currentEditTypeBent;
    [FormerlySerializedAs("_skinRotSCD")] public SkinRotationBent _skinRotBent;

    private int _brushWidthBent = 16;
    private int _brushHeightBent = 16;

    private Color[] collorsBent = new Color[256];
    private Color _currentColorBent;
    public Action AfterAnimPreviewBent;
    [FormerlySerializedAs("_editedStepsSCD")] public List<StepDataBent> _editedStepsBent = new List<StepDataBent>();
    [FormerlySerializedAs("CurrStepSCD")] public int CurrStepBent = 0;

    private Renderer _beginRendererBent = null;
    private Texture2D _tempTextBent = null;
    private SkinEditorSettingsBent _skinEditorBent;
    private SkinMakerSettingsBent _skinMakerBent;
    private CreatorControllerBent _creatorBent;
    private Renderer _currentRendererBent = null;
    [FormerlySerializedAs("_originTexturesSCD")] public List<OriginsDataBent> _originTexturesBent = new List<OriginsDataBent>();
    [FormerlySerializedAs("firstSCD")] public bool firstBent = false;

    [Serializable]
    public struct StepDataBent
    {
        [FormerlySerializedAs("StTextureSCD")] public Texture2D StTextureBent;
        [FormerlySerializedAs("EndTextureSCD")] public Texture2D EndTextureBent;
        [FormerlySerializedAs("RendSCD")] public Renderer RendBent;
        
        private void TrashMethodBent()
        {
            var boolBent = true;
            var boolBent2 = true;
            var boolBent3 = true;

            if (boolBent != boolBent2 && boolBent2 != boolBent3)
            {
                boolBent = true;
                boolBent2 = true;
                boolBent3 = true;
            }
        }
    }
    public override void Awake()
    {
        Application.targetFrameRate = 60;
        //base.Awake();
        AddToContainerBent();
        ChangeColorBent(Color.black);
        ChangeBrushSizeBent(4);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void Start()
    {
        _skinEditorBent = ContainerBent.GetBent<SkinEditorSettingsBent>();
        _creatorBent = ContainerBent.GetBent<CreatorControllerBent>();
        _creatorBent.OnEditExitBent += (() => {
            ClearTexturesBent();
            _skinEditorBent.UpdateActiveStepsBent(); 
        });
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private void Update()
    {
        if (Input.touchCount > 0)
        {
            DrawBent();
            if (_skinEditorBent == null)
                _skinEditorBent = ContainerBent.GetBent<SkinEditorSettingsBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
        }
    }
    private void ClearTexturesBent()
    {
        CurrStepBent = 0;
        foreach (var anyStepBent in _editedStepsBent)
        {
            Destroy(anyStepBent.EndTextureBent);
            Destroy(anyStepBent.StTextureBent);
        }
        _editedStepsBent.Clear();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private Renderer GetRendInstanceBent(Renderer someBent)
    {
        Renderer yepBent = new Renderer();
        yepBent = someBent;
        return yepBent;
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

    // Very scary method, but no time for refactoring this monster
    private void DrawBent()
    {
        if (_currentEditTypeBent == EditTypeBent.AbleRotBent)
            return;
        bool IsAbleToDrawBent = true;
        RaycastHit hitBent;
        if (!Physics.Raycast(_camBent.ScreenPointToRay(Input.mousePosition), out hitBent))
            return;
        Renderer rendBent = hitBent.transform.GetComponent<Renderer>();
        MeshCollider meshColliderBent = hitBent.collider as MeshCollider;

        if (rendBent == null || rendBent.sharedMaterial == null || rendBent.sharedMaterial.mainTexture == null || meshColliderBent == null)
            IsAbleToDrawBent = false;

        Texture2D texBent = new Texture2D(2, 2, TextureFormat.RGB24, true);
        if (IsAbleToDrawBent)
             texBent = rendBent.material.mainTexture as Texture2D;
        Touch touchBent = Input.GetTouch(0);

        switch (touchBent.phase)
        {
            case TouchPhase.Began:
                _currentRendererBent = rendBent;
                if ( _currentEditTypeBent == EditTypeBent.AbleRotBent || _currentEditTypeBent == EditTypeBent.ColorBent || !IsAbleToDrawBent)
                    break;

                Texture2D CurTextureBent = new Texture2D(2, 2, TextureFormat.RGB24, true);
                var tempTextureBent = rendBent.material.mainTexture as Texture2D;
                CurTextureBent = Instantiate(tempTextureBent);
                
                if (!firstBent)
                {
                    StepDataBent stepDataBent1 = new StepDataBent();
                    stepDataBent1.StTextureBent = Instantiate(rendBent.material.mainTexture as Texture2D);
                    stepDataBent1.EndTextureBent = Instantiate(rendBent.material.mainTexture as Texture2D);
                    stepDataBent1.RendBent = rendBent;
                    _editedStepsBent.Add(stepDataBent1);
                    firstBent = true;
                }
                if (CurrStepBent + 1 != _editedStepsBent.Count)
                {
                    _editedStepsBent.RemoveRange(CurrStepBent + 1, _editedStepsBent.Count - CurrStepBent -1);
                    _skinEditorBent.UpdateActiveStepsBent();
                }
                else if (CurrStepBent + 1 >= 13)
                {
                    Destroy(_editedStepsBent[0].StTextureBent);
                    Destroy(_editedStepsBent[0].EndTextureBent);
                    _editedStepsBent.RemoveAt(0);
                    CurrStepBent--;
                }
                    
                var bBent = Instantiate(CurTextureBent);
                _tempTextBent = bBent;
                if (_beginRendererBent == null)
                    _beginRendererBent = GetRendInstanceBent(rendBent);

                if (!_originTexturesBent.Exists(dataBent => dataBent.orRendBent == rendBent))
                {
                    OriginsDataBent tempBent = new OriginsDataBent();
                    tempBent.orRendBent = GetRendInstanceBent(rendBent);
                    tempBent.orTextBent = Instantiate(CurTextureBent);
                    _originTexturesBent.Add(tempBent);
                }
                    
                Destroy(rendBent.material.mainTexture);
                rendBent.material.mainTexture = CurTextureBent;
                texBent = CurTextureBent;
                texBent.filterMode = FilterMode.Point;

                Destroy(tempTextureBent);
                break;
            case TouchPhase.Ended:
                
                if ( _currentEditTypeBent == EditTypeBent.AbleRotBent || _currentEditTypeBent == EditTypeBent.ColorBent || _tempTextBent == null)
                    break;
                StepDataBent stepDataBent = new StepDataBent();
                stepDataBent.StTextureBent = Instantiate(_tempTextBent);
                stepDataBent.EndTextureBent = Instantiate(_beginRendererBent.material.mainTexture as Texture2D);
                stepDataBent.RendBent = _beginRendererBent;
                _editedStepsBent.Add(stepDataBent);
                Destroy(_tempTextBent);
                _beginRendererBent = null;
                _currentRendererBent = null;
                CurrStepBent++;
                _skinEditorBent.UpdateActiveStepsBent();
                break;
        }

        if (rendBent != _currentRendererBent || !IsAbleToDrawBent)
            return;
        
        switch (_currentEditTypeBent)
        {
            case EditTypeBent.AbleRotBent:
                break;
            case EditTypeBent.BrushBent:
                Vector2 pixelUVBent = hitBent.textureCoord;
                pixelUVBent.x *= texBent.width;
                pixelUVBent.y *= texBent.height;
                texBent.SetPixels((int)pixelUVBent.x, (int)pixelUVBent.y, _brushWidthBent, _brushHeightBent, collorsBent);
                texBent.Apply(true);
                break;
            case EditTypeBent.FillBent:
                
                int baseAmmountOfPixelsBent = texBent.height * texBent.width;
                Color[] colorToFillBent = new Color[baseAmmountOfPixelsBent];
                for (int i = 0; i < colorToFillBent.Length; i++)
                {
                    colorToFillBent[i] = _currentColorBent;
                }
                for (int xPixBent = 0; xPixBent < texBent.width; xPixBent++)
                {
                    texBent.SetPixels(xPixBent, 0, 1, texBent.height, colorToFillBent);
                }
                texBent.Apply(true);

                break;
            case EditTypeBent.EaserBent:
                Vector2 UVeraserBent = hitBent.textureCoord;
                UVeraserBent.x *= texBent.width;
                UVeraserBent.y *= texBent.height;
                Color[] eraseColBent = _originTexturesBent.Find(origDatBent => origDatBent.orRendBent == rendBent).orTextBent.GetPixels((int)UVeraserBent.x, (int)UVeraserBent.y, _brushWidthBent, _brushHeightBent, 0);
                texBent.SetPixels((int)UVeraserBent.x, (int)UVeraserBent.y, _brushWidthBent, _brushHeightBent, eraseColBent);
                texBent.Apply(true);
                break;
        }
        
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    public void ChangeColorBent(Color brushColorBent)
    {
        _currentColorBent = brushColorBent;
        for (int iBent = 0; iBent < collorsBent.Length; iBent++)
        {
            collorsBent[iBent] = brushColorBent;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ChangeBrushSizeBent(int someSizeBent)
    {
        int newSizeBent = someSizeBent * 2;
        _brushWidthBent = newSizeBent;
        _brushHeightBent = newSizeBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    protected override void AddToContainerBent()
    {
        ContainerBent.AddBent<DrawingControllerBent>(this);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    protected override void RemoveFromContainerBent()
    {
        ContainerBent.RemoveBent<DrawingControllerBent>();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
[Serializable]
public struct OriginsDataBent
{
    [FormerlySerializedAs("orRendSCD")] public Renderer orRendBent;
    [FormerlySerializedAs("orTextSCD")] public Texture2D orTextBent;
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
public enum EditTypeBent
{
    AbleRotBent = 0,
    ColorBent = 1,
    BrushBent = 2,
    FillBent = 3,
    EaserBent = 4
}
