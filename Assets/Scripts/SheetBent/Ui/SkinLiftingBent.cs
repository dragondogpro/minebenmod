using UnityEngine;

public class SkinLiftingBent : MonoBehaviour
{
    private RectTransform _modelPosBent;

    private float _currYposBent => _modelPosBent.anchoredPosition.y;

    private void Awake()
    {
        _modelPosBent = this.GetComponent<RectTransform>();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    public void LiftModelBent(float yValueBent, float timeBent)
    {
        LeanTween.cancel(_modelPosBent.gameObject);
        LeanTween.value(_modelPosBent.gameObject,
                        _currYposBent,
                        yValueBent,
                        timeBent).
          setOnUpdate((float valueBent) =>
          {
              _modelPosBent.anchoredPosition = new Vector2(_modelPosBent.anchoredPosition.x, valueBent);
          }).
          setEaseLinear();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }

}
