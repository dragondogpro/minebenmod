using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using IOSBridge;
using System.IO;
using System.Threading.Tasks;
using UnityEngine.Serialization;

public class OptionsMenuBent : MonoBehaviour
{
    [FormerlySerializedAs("_screenShotSCD")] [SerializeField] private ScreenShotHelperBent screenShotBent;
    [FormerlySerializedAs("_bgSCD")]
    [Space]
    [SerializeField] private Image _bgBent;
    [FormerlySerializedAs("_optionFrameSCD")] [SerializeField] private Image _optionFrameBent;
    [FormerlySerializedAs("_shareButtonSCD")]
    [Space]
    [SerializeField] private Button _shareButtonBent;
    [FormerlySerializedAs("_saveToGalleySCD")] [SerializeField] private Button _saveToGalleyBent;
    [FormerlySerializedAs("_saveToGalleySCD")] [SerializeField] private Button _renamSkinBent;
    //[SerializeField] private Button _renameButtonSCD;
    //[SerializeField] private Button _addToFavSCD;
    [FormerlySerializedAs("_deleteButtonSCD")] [SerializeField] private Button _deleteButtonBent;
    [FormerlySerializedAs("_closeButtonSCD")] [SerializeField] private Button _closeButtonBent;
    [FormerlySerializedAs("_stubBackSCD")]
    [Space]
    [Header("Save / Rename window")]
    [SerializeField] private GameObject _stubBackBent;
    [FormerlySerializedAs("_titleNameSCD")] [SerializeField] private Text _titleNameBent;
    [FormerlySerializedAs("_saveToFinputSCD")] [SerializeField] private TMP_InputField _saveToFinputBent;
    [FormerlySerializedAs("_cancelSaveSCD")] [SerializeField] private Button _cancelSaveBent;
    [FormerlySerializedAs("_saveChangesSCD")] [SerializeField] private Button _saveChangesBent;
    [FormerlySerializedAs("_deleteWindowSCD")]
    [Space]
    [Header("Delete window")]
    [SerializeField] private GameObject _deleteWindowBent;
    [FormerlySerializedAs("_cancelDelButtonCD")] [SerializeField] private Button _cancelDelButtonBent;
    [FormerlySerializedAs("_yesButtonSCD")] [SerializeField] private Button _yesButtonBent;
    [FormerlySerializedAs("_toHideWhenSavingSCD")]
    [Space]
    [SerializeField] private GameObject[] _toHideWhenSavingBent;
    private CreatorControllerBent _mainBent = null;
    private Button _missClickButtonBent;

    private bool IsFirstStepBent = true;
    private bool IsJustRenameBent = true;

    private void Awake()
    {
        _missClickButtonBent = _bgBent.GetComponent<Button>();
        _missClickButtonBent.onClick.AddListener(HideOptionsBent);
        _closeButtonBent.onClick.AddListener(HideOptionsBent);

        _cancelSaveBent.onClick.AddListener(OnCancelBent);
        _saveChangesBent.onClick.AddListener(OnSaveChangesBent);
        _renamSkinBent.onClick.AddListener(RenameSkinBent);

        _cancelDelButtonBent.onClick.AddListener(() => 
        { _deleteWindowBent.gameObject.SetActive(false); _optionFrameBent.gameObject.SetActive(true); });
        _yesButtonBent.onClick.AddListener(() => DeleteSkinBent(true));

        _shareButtonBent.onClick.AddListener(ShareSkinBent);
        _saveToGalleyBent.onClick.AddListener(() => { IsJustRenameBent = false; SaveToGalleryBent(); });
      //  _renameButtonSCD.onClick.AddListener(() => { IsJustRenameSCD = true; RenameSkinSCD(); });
        //_addToFavSCD.onClick.AddListener(OnFavoriteSCD);
        _deleteButtonBent.onClick.AddListener(() => DeleteSkinBent(false));

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnEnable()
    {
        if (_mainBent == null)
            _mainBent = ContainerBent.GetBent<CreatorControllerBent>();
        //var textOnBUtSCD = _addToFavSCD.GetComponentInChildren<TextMeshProUGUI>();

        /*if (!_mainSCD.SkinDataEditSCD.IsFavoriteSCD)
            textOnBUtSCD.text = "Add to favorites";
        else
            textOnBUtSCD.text = "Remove from favorites";
            */

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    public void ShowOptionsBent()
    {
        this.gameObject.SetActive(true);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void HideOptionsBent()
    {
        this.gameObject.SetActive(false);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private async void ShareSkinBent()
    {
        var skinDataBent = ContainerBent.GetBent<CreatorControllerBent>();
        string nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent;
        int indxIfNeededBent = 0;
        while (File.Exists(Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "")))
        {
            nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent + $" ({indxIfNeededBent++})";
        }
        string savePathBentBent = Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "");
        
        if (!Directory.Exists(Path.GetDirectoryName(savePathBentBent)))
        {
            Directory.CreateDirectory(Path.GetDirectoryName(savePathBentBent));
        }
        
        await screenShotBent.GetScreenShotBent(savePathBentBent);
        
        IOStoUnityBridge.InitWithActivity(savePathBentBent);

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private async void SaveToGalleryBent()
    {
        if (IsFirstStepBent)
        {
            RenameSkinBent();
            _titleNameBent.text = "Name SkinBent".Replace("Bent", "");
            _saveToFinputBent.text = _mainBent.SkinDataEditBent.SkinNameBent;
        }
        else
        {
            var skinDataBent = ContainerBent.GetBent<CreatorControllerBent>();
            string nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent;
            int indxIfNeededBent = 0;
            while (File.Exists(Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "")))
            {
                nameForSkinBent = skinDataBent.SkinDataEditBent.SkinNameBent + $" ({indxIfNeededBent++})";
            }
            string savePathBent = Application.persistentDataPath + "/Screens/Bent".Replace("Bent", "") + nameForSkinBent + ".pngBent".Replace("Bent", "");
            if (!Directory.Exists(Path.GetDirectoryName(savePathBent)))
            {
                Directory.CreateDirectory(Path.GetDirectoryName(savePathBent));
            }
            await screenShotBent.GetScreenShotBent(savePathBent);
            IOStoUnityBridge.SaveImageToAlbum(savePathBent);
            IsFirstStepBent = true;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void RenameSkinBent()
    {
        _titleNameBent.text = "Rename SkinBent".Replace("Bent", "");
        _optionFrameBent.gameObject.SetActive(false);
        _stubBackBent.gameObject.SetActive(true);
        _saveToFinputBent.text = _mainBent.SkinDataEditBent.SkinNameBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void DeleteSkinBent(bool iasAgreedBent)
    {
        if (iasAgreedBent)
        {
            var creatorBent = ContainerBent.GetBent<CreatorControllerBent>();
            creatorBent.DeleteSkinBent();
            this.gameObject.SetActive(false);
            _deleteWindowBent.SetActive(false);
            _optionFrameBent.gameObject.SetActive(true);
        }
        else
        {
            _optionFrameBent.gameObject.SetActive(false);
            _deleteWindowBent.SetActive(true);
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnCancelBent()
    {
        if (_stubBackBent.gameObject.activeSelf)
        {
            _stubBackBent.gameObject.SetActive(false);
            _optionFrameBent.gameObject.SetActive(true);
        }
           
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void OnSaveChangesBent()
    {
        if (_stubBackBent.gameObject.activeSelf)
        {
            bool IsNotEmptyBent = false;
            for (int i = 0; i < _saveToFinputBent.text.Length; i++)
            {
                if (_saveToFinputBent.text[i] != ' ')
                {
                    IsNotEmptyBent = true;
                    break;
                }
            }
            if (!IsNotEmptyBent)
                _mainBent.SkinDataEditBent.SkinNameBent = "Empty nameBent".Replace("Bent", "");
            else
                _mainBent.SkinDataEditBent.SkinNameBent = _saveToFinputBent.text;
            _stubBackBent.gameObject.SetActive(false);
            _bgBent.gameObject.SetActive(false);
            _optionFrameBent.gameObject.SetActive(true);
            if (!IsJustRenameBent)
            {
                IsFirstStepBent = false;
                SaveToGalleryBent();
            }
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
