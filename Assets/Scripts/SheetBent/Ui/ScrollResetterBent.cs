using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollResetterBent : MonoBehaviour
{
    [FormerlySerializedAs("IsVerticalToResetSCD")] public bool IsVerticalToResetBent = false;
    [FormerlySerializedAs("IsHorizontalToResetSCD")] public bool IsHorizontalToResetBent = false;

    private MenuControllerBent _mainBent;
    private ScrollRect _scrollBent;

    private void Start()
    {
        _mainBent = ContainerBent.GetBent<MenuControllerBent>();
        _scrollBent = GetComponent<ScrollRect>();

        if (IsVerticalToResetBent)
            _mainBent.OnMenuChangedBent += VerticalScrollResetPosBent;
        if (IsHorizontalToResetBent)
            _mainBent.OnMenuChangedBent += HorizontalScrollResetPos;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    public void CallToResetScrollBent(bool TrueItsHorizBent)
    {
        if (TrueItsHorizBent)
        {
            if (_scrollBent.horizontalNormalizedPosition != 0)
                _scrollBent.horizontalNormalizedPosition = 0;
        }
        else
        {
            if (_scrollBent.verticalNormalizedPosition != 1)
                _scrollBent.verticalNormalizedPosition = 1;
        }

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    private void VerticalScrollResetPosBent()
    {
        if (_scrollBent.verticalNormalizedPosition != 1)
            _scrollBent.verticalNormalizedPosition = 1;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    private void HorizontalScrollResetPos()
    {
        if (_scrollBent.horizontalNormalizedPosition != 0)
            _scrollBent.horizontalNormalizedPosition = 0;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
