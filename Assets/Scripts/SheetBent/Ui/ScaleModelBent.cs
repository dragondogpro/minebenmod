using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class ScaleModelBent : MonoBehaviour
{
    [FormerlySerializedAs("_previewModelSCD")] [SerializeField] RectTransform _previewModelBent;
    private float _defScaleBent = 1.8f;
    private RectTransform _cardRectBent;
    
    private void Awake()
    {
        _cardRectBent = this.GetComponent<RectTransform>();
        CalcScaleForModelBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
#if UNITY_EDITOR
    private void Update()
    {
        CalcScaleForModelBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
#endif
    private void CalcScaleForModelBent()
    {
        float difKoofBent = (_cardRectBent.rect.width / 168) - 1;
        float newScaleBent = _defScaleBent + difKoofBent;
        _previewModelBent.localScale = new Vector3(_defScaleBent + difKoofBent, _defScaleBent + difKoofBent, _defScaleBent + difKoofBent);

        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
