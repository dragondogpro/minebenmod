using UnityEngine;
using UnityEngine.UI;

public class ScaleContentToScreenBent : MonoBehaviour
{
    private RectTransform rectTransformBent;
    private GridLayoutGroup gridLayoutBent;

    private void Start()
    {
        rectTransformBent = GetComponent<RectTransform>();
        gridLayoutBent = GetComponent<GridLayoutGroup>();
        Canvas.ForceUpdateCanvases();
        CalcCellSizeBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

#if UNITY_EDITOR
    private void Update()
    {
        CalcCellSizeBent();

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
#endif

    private void TrashNotNeeded1SCD()
    {
        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }

    [ContextMenu("CalcCellSizeBent")]
    private void CalcCellSizeBent()
    {
        var coefficientWidthToHeightBent = gridLayoutBent.cellSize.y / gridLayoutBent.cellSize.x;

        var paddingSumBent = gridLayoutBent.padding.left + gridLayoutBent.padding.right;
        var rectWidthWithoutPaddingBent = rectTransformBent.rect.width - paddingSumBent;
        var numOfCardInColumnBent = gridLayoutBent.constraintCount;
        var contentWidthWithoutPaddingBent = gridLayoutBent.cellSize.x * numOfCardInColumnBent +
                                               (gridLayoutBent.spacing.x * (numOfCardInColumnBent - 1));

        var widthCoefficientBent = rectWidthWithoutPaddingBent / contentWidthWithoutPaddingBent;
        var newCellWidthBent = gridLayoutBent.cellSize.x * widthCoefficientBent;

        gridLayoutBent.cellSize = new Vector2(newCellWidthBent, newCellWidthBent * coefficientWidthToHeightBent);

        var spacingTempBent = gridLayoutBent.spacing;
        spacingTempBent.x *= widthCoefficientBent;
        gridLayoutBent.spacing = spacingTempBent;

        // trash =====================================================
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        // trash =====================================================
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
