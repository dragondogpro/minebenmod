using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(BoxCollider))]
public class FixWrongRayCastBent : MonoBehaviour
{
    [FormerlySerializedAs("IsBottomPanelSCD")] [SerializeField] private bool IsBottomPanelBent = false;
    [FormerlySerializedAs("IsTopPanelSCD")] [SerializeField] private bool IsTopPanelBent = false;
    [FormerlySerializedAs("IsOverAllPamelSCD")] [SerializeField] private bool IsOverAllPamelBent = false;


    RectTransform _thisObjectBent;
    BoxCollider _coliderOfThisObjgBent;

    private void Awake()
    {
        _thisObjectBent = GetComponent<RectTransform>();
        _coliderOfThisObjgBent = GetComponent<BoxCollider>();
        ColiderSizeToRectSizeBent();
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
    private async void ColiderSizeToRectSizeBent()
    {
        await System.Threading.Tasks.Task.Delay(200);
        if(IsBottomPanelBent)
        {
            _coliderOfThisObjgBent.size = new Vector3(_thisObjectBent.rect.width, _thisObjectBent.rect.height, 1);
            _coliderOfThisObjgBent.center = new Vector3(0, _thisObjectBent.rect.height / 2, 0);
        }
        else if(IsTopPanelBent)
        {
            _coliderOfThisObjgBent.size = new Vector3(_thisObjectBent.rect.width, _thisObjectBent.rect.height, 1);
            _coliderOfThisObjgBent.center = new Vector3(0, _thisObjectBent.rect.height / 2 * -1, 0);
        }
        else if(IsOverAllPamelBent)
        {
            _coliderOfThisObjgBent.size = new Vector3(_thisObjectBent.rect.width, _thisObjectBent.rect.height, 1);
        }
        
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
        
    }
    
    private void TrashMethodBent()
    {
        var boolBent = true;
        var boolBent2 = true;
        var boolBent3 = true;

        if (boolBent != boolBent2 && boolBent2 != boolBent3)
        {
            boolBent = true;
            boolBent2 = true;
            boolBent3 = true;
        }
    }
}
