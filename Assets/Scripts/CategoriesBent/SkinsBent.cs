﻿using System.Collections.Generic;
using System.Linq;
using InterfacesBent;
using ModelsBent;
using UnityEngine;

namespace CategoriesBent
{
    public class SkinsBent : MonoBehaviour, IHttpClientHelperBent, IPathControllerBent, ICategoryTypeHelperBent
    {
        [SerializeField] private GameObject _gameObjectBent;
        [SerializeField] private Transform _gridTransformBent;

        public List<Transform> cardsListBent = new List<Transform>();
        public List<Transform> newCardsListBent;

        public async void Start()
        {
            ModelsBent.ContentJsonBent contentJsonBent =  CardGeneratorBent.ContentJsonBent;
            List<FavoriteCardBent> _favoriteCardsListBent = CardGeneratorBent.FavoriteCardsListBent;

            int cardIdBent = 0;
            foreach (var categoryBent in contentJsonBent.B6657.Reverse())
            {
                int cardNewIdBent = 0;
                foreach (var subcategoriesBent in categoryBent.Value.Reverse())
                {
                    CardDefaultBent cardBent = new CardDefaultBent();
                    cardBent.IdBent = cardIdBent;
                    var nameBent = $"skin_{cardIdBent}";
                    cardBent.cardCategoryBent = categoryBent.Key.ToLower();
                    cardBent.IconPathBent = subcategoriesBent.Value["aezij_q"];

                    bool cardFavoriteBent =
                        _favoriteCardsListBent.Exists(x => x.IdBent == cardIdBent && x.NameBent == nameBent);
                    
                    var objectCardBent = Instantiate(_gameObjectBent, _gridTransformBent);
                    objectCardBent.name = nameBent;
                    
                    objectCardBent.SetActive(true);

                    objectCardBent.GetComponent<IconControllerBent>().devicePathBent =
                        IPathControllerBent.ContentSkinsPathBent;
                    objectCardBent.GetComponent<IconControllerBent>().iconPathBent = cardBent.IconPathBent;
                    objectCardBent.GetComponent<IconControllerBent>().iconNameBent =
                        cardBent.IconPathBent.Split(IKeyStorageHelperBent.SlashSeparatorBent).Last();

                    objectCardBent.GetComponent<CardDefaultBent>().cardBent = cardBent;
                    objectCardBent.GetComponent<CardDefaultBent>().cardIdBent = cardBent.IdBent;
                    objectCardBent.GetComponent<CardDefaultBent>().filePathBent = subcategoriesBent.Value["x6veqec5y"];
                    objectCardBent.GetComponent<CardDefaultBent>().cardTypeBent =
                        ICategoryTypeHelperBent.SkinsCategoryNameBent;
                    objectCardBent.GetComponent<CardDefaultBent>().cardCategoryBent = cardBent.cardCategoryBent;


                    if (cardFavoriteBent)
                    {
                        objectCardBent.GetComponent<CardDefaultBent>().isFavoriteBent = true;
                        FavoritesListOperationsBent.AddedToFavoritesTransformListBent(objectCardBent.transform);
                    }

                    var newCardBent = objectCardBent.transform.GetChild(0).transform.GetChild(0)
                        .transform.GetChild(1).gameObject;
                    
                    if (cardNewIdBent <= 2 && !newCardBent.activeSelf)
                    {
                        newCardBent.SetActive(true);
                        objectCardBent.GetComponent<CardDefaultBent>().isNewCardBent = true;
                        newCardsListBent.Add(objectCardBent.transform);
                    }
                    
                    cardsListBent.Add(objectCardBent.transform);

                    cardNewIdBent++;
                    cardIdBent++;
                    cardBent = null;
                }
            }
        }
    }
}
