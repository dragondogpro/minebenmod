using System.Collections;
using System.Collections.Generic;
using CategoriesBent;
using InterfacesBent;
using ModelsBent;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

public class RecomendationBent : MonoBehaviour
{
    [SerializeField] private string _categoryNameBent;
    [SerializeField] private GameObject _gameObjectBent;
    [SerializeField] private Transform _gridTransformBent;
    [SerializeField] private Sprite _selectedSpriteBent;
    [SerializeField] private Sprite _defaultSpriteBent;
    [SerializeField] private CheckCategoryBent _categoryObjectBent;
    [SerializeField] private ScrollRect _recScrollBent;

    public List<Transform> RecommendationListBent = new List<Transform>();
    private List<Transform> cardListBent;

    public void StartGeneratedCardBent()
    {
        _recScrollBent.horizontalNormalizedPosition = 1f;
        cardListBent = _categoryObjectBent.GetCategoryListBent(_categoryNameBent);
        var newCardListBent = _categoryObjectBent.GetNewCardListBent(_categoryNameBent);

        for (int i = 0; i < 5; i++)
        {
            var randomIntBent = new System.Random().Next(0, cardListBent.Count - 1);
            RecommendationListBent.Add(cardListBent[randomIntBent]);
        }

        foreach (var recommendationCardBent in RecommendationListBent)
        {
            var objectCardBent = Instantiate(_gameObjectBent, _gridTransformBent);
            objectCardBent.name = recommendationCardBent.name;


            objectCardBent.GetComponent<CardDefaultBent>().cardTypeBent =
                recommendationCardBent.GetComponent<CardDefaultBent>().cardTypeBent;

            objectCardBent.GetComponent<CardDefaultBent>().isNewCardBent =
                recommendationCardBent.GetComponent<CardDefaultBent>().isNewCardBent;

            objectCardBent.GetComponent<CardDefaultBent>().cardIdBent =
                recommendationCardBent.GetComponent<CardDefaultBent>().cardIdBent;

            objectCardBent.GetComponent<CardDefaultBent>().filePathBent =
                recommendationCardBent.GetComponent<CardDefaultBent>().filePathBent;

            if (recommendationCardBent.GetComponent<CardDefaultBent>().isFavoriteBent)
            {
                objectCardBent.GetComponent<CardDefaultBent>().isFavoriteBent = true;
                FavoritesListOperationsBent.AddedToFavoritesTransformListBent(objectCardBent.transform);
            }

            objectCardBent.GetComponent<CardDefaultBent>().cardBent =
                recommendationCardBent.GetComponent<CardDefaultBent>().cardBent;
            
            if (_categoryNameBent != ICategoryTypeHelperBent.SkinsCategoryNameBent)
                objectCardBent.transform.GetChild(2).GetComponent<Text>().text = recommendationCardBent.name;

            objectCardBent.GetComponent<IconControllerBent>().devicePathBent =
                recommendationCardBent.GetComponent<IconControllerBent>().devicePathBent;
            objectCardBent.GetComponent<IconControllerBent>().iconPathBent =
                recommendationCardBent.GetComponent<IconControllerBent>().iconPathBent;
            objectCardBent.GetComponent<IconControllerBent>().iconNameBent =
                recommendationCardBent.GetComponent<IconControllerBent>().iconNameBent;
            objectCardBent.GetComponent<IconControllerBent>().urlBent =
                recommendationCardBent.GetComponent<IconControllerBent>().urlBent;

            if (newCardListBent.Contains(recommendationCardBent))
                objectCardBent.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject
                    .SetActive(true);

            objectCardBent.SetActive(true);
        }
    }

    public void DestroyRecommendationCard()
    {
        foreach (Transform cardBent in _gridTransformBent.transform)
        {
            if (cardBent.GetComponent<CardDefaultBent>().isFavoriteBent)
            {
                cardListBent[cardBent.GetComponent<CardDefaultBent>().cardIdBent].GetComponent<CardDefaultBent>()
                    .isFavoriteBent = true;
                
                FavoritesListOperationsBent.RemovedFromFavoriteListBent(cardBent);
                
                FavoritesListOperationsBent.AddedToFavoritesTransformListBent(
                    cardListBent[cardBent.GetComponent<CardDefaultBent>().cardIdBent].transform);

                cardListBent[cardBent.GetComponent<CardDefaultBent>().cardIdBent].transform.GetChild(0).transform
                        .GetChild(1).transform.GetChild(0).GetComponent<Image>().sprite
                    = _selectedSpriteBent;
            }

        }

        for (int i = 1; i < _gridTransformBent.childCount; i++)
            Destroy(_gridTransformBent.GetChild(i).gameObject);

        RecommendationListBent.Clear();
    }
}
