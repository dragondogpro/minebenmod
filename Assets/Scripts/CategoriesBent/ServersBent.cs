﻿using System.Collections.Generic;
using System.Linq;
using InterfacesBent;
using ModelsBent;
using UnityEngine;
using UnityEngine.UI;

namespace CategoriesBent
{
    public class ServersBent : MonoBehaviour, IHttpClientHelperBent, IPathControllerBent, ICategoryTypeHelperBent
    {
        [SerializeField] private GameObject _gameObjectBent;
        [SerializeField] private Transform _gridTransformBent;
        
        public List<Transform> cardsListBent;
        public List<Transform> newCardsListBent;

        private int cardIdBent = 0;

        public async void Start()
        {
            var serverJsonBent = CardGeneratorBent.ServerJsonBent;
            List<FavoriteCardBent> _favoriteCardsListBent = CardGeneratorBent.FavoriteCardsListBent;
            
            foreach (var serverCardBent in serverJsonBent.ServerCardsBent)
            {
                CardDefaultBent cardBent = new CardDefaultBent();
                cardBent.IdBent = cardIdBent;
                cardBent.NameBent = serverCardBent.Zarg;
                cardBent.ServerPortBent = serverCardBent.O5Fp8.ToString();
                cardBent.ServerIPBent = serverCardBent.The34O5Yp;
                cardBent.IconPathBent = serverCardBent.Eb2Wqwc;
                cardBent.DetailsBent = serverCardBent.The0R5Ou9;
                
                bool cardFavoriteBent = _favoriteCardsListBent.Exists(x =>
                    x.IdBent == cardBent.IdBent && x.NameBent == cardBent.NameBent);
                
                var objectCardBent = Instantiate(_gameObjectBent, _gridTransformBent);
                objectCardBent.name = cardBent.NameBent;
                objectCardBent.SetActive(true);
                
                objectCardBent.GetComponent<CardDefaultBent>().cardCategoryBent = ICategoryTypeHelperBent.AllSubcategoryNameBent;
                objectCardBent.GetComponent<CardDefaultBent>().cardServerPortBent = cardBent.ServerPortBent;
                objectCardBent.GetComponent<CardDefaultBent>().cardServerIPBent = cardBent.ServerIPBent;

                
                objectCardBent.GetComponent<IconControllerBent>().iconPathBent = cardBent.IconPathBent;
                objectCardBent.GetComponent<IconControllerBent>().iconNameBent =
                    cardBent.IconPathBent.Split(IKeyStorageHelperBent.SlashSeparatorBent)[1];
                    
                objectCardBent.transform.GetChild(2).GetComponent<Text>().text = cardBent.NameBent.Length > 50
                    ? cardBent.NameBent[..50] + IKeyStorageHelperBent.DotSeparatorBent + 
                      IKeyStorageHelperBent.DotSeparatorBent + 
                      IKeyStorageHelperBent.DotSeparatorBent
                    : cardBent.NameBent;

                objectCardBent.transform.GetChild(3).GetComponent<Text>().text = cardBent.DetailsBent.Length > 35
                    ? cardBent.DetailsBent[..35] + IKeyStorageHelperBent.DotSeparatorBent + 
                      IKeyStorageHelperBent.DotSeparatorBent + 
                      IKeyStorageHelperBent.DotSeparatorBent
                    : cardBent.DetailsBent;
            
                objectCardBent.GetComponent<CardDefaultBent>().cardBent = cardBent;
                objectCardBent.GetComponent<CardDefaultBent>().cardIdBent = cardBent.IdBent;

                objectCardBent.GetComponent<IconControllerBent>().devicePathBent =
                    IPathControllerBent.ContentServersPathBent;
                objectCardBent.GetComponent<CardDefaultBent>().cardTypeBent =
                    ICategoryTypeHelperBent.ServersCategoryNameBent;
                
                var newCardBent = objectCardBent.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject;
                if (cardIdBent <= 2 && !newCardBent.activeSelf)
                {
                    newCardBent.SetActive(true);
                    objectCardBent.GetComponent<CardDefaultBent>().isNewCardBent = true;
                    newCardsListBent.Add(objectCardBent.transform);
                }
                
                if (cardFavoriteBent)
                {
                    objectCardBent.GetComponent<CardDefaultBent>().isFavoriteBent = true;
                    FavoritesListOperationsBent.AddedToFavoritesTransformListBent(objectCardBent.transform);
                }
                
                cardsListBent.Add(objectCardBent.transform);
                
                cardIdBent++;
                cardBent = null;
            }
        }
    }
}
