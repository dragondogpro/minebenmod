﻿using System.Collections.Generic;
using System.Linq;
using InterfacesBent;
using ModelsBent;
using UnityEngine;
using UnityEngine.UI;

namespace CategoriesBent
{
    public class ModsBent : MonoBehaviour, IHttpClientHelperBent, IPathControllerBent, ICategoryTypeHelperBent
    {
        [SerializeField]private GameObject _gameObjectBent;
        [SerializeField]private Transform _gridTransformBent;
        
        public List<Transform> cardsListBent = new List<Transform>();
        public List<Transform> newCardsListBent;

        private async void Start()
        {
            ModelsBent.ContentJsonBent contentJsonBent =  CardGeneratorBent.ContentJsonBent;
            List<FavoriteCardBent> _favoriteCardsListBent = CardGeneratorBent.FavoriteCardsListBent;

            int cardIdBent = 0;
            
            foreach (var categoryBent in contentJsonBent.Mjkbb.Reverse())
            {
                int cardNewIdBent = 0;
                foreach (var subcategoriesBent in categoryBent.Value.Reverse())
                {
                    CardDefaultBent cardDefaultBent = new CardDefaultBent();
                    cardDefaultBent.IdBent = cardIdBent;
                    cardDefaultBent.NameBent = subcategoriesBent.Value["m434f9jk"];
                    cardDefaultBent.cardCategoryBent = categoryBent.Key.ToLower();
                    cardDefaultBent.DetailsBent = subcategoriesBent.Value["x8tmt"];
                    cardDefaultBent.IconPathBent = subcategoriesBent.Value["lmela1"];
                    cardDefaultBent.filePathBent = subcategoriesBent.Value["5f0xy94qcn"];
                    
                    bool cardFavoriteBent = _favoriteCardsListBent.Exists(x =>
                        x.IdBent == cardDefaultBent.IdBent && x.NameBent == cardDefaultBent.NameBent);
                    
                    var objectCardBent = Instantiate(_gameObjectBent, _gridTransformBent);

                    objectCardBent.name = cardDefaultBent.NameBent;
                    objectCardBent.SetActive(true);
                    
                    objectCardBent.GetComponent<IconControllerBent>().iconPathBent = cardDefaultBent.IconPathBent;
                    objectCardBent.GetComponent<IconControllerBent>().iconNameBent =
                        cardDefaultBent.IconPathBent.Split(IKeyStorageHelperBent.SlashSeparatorBent)[1];
                    
                    objectCardBent.transform.GetChild(2).GetComponent<Text>().text = cardDefaultBent.NameBent.Length > 50
                        ? cardDefaultBent.NameBent[..50] + IKeyStorageHelperBent.DotSeparatorBent + 
                          IKeyStorageHelperBent.DotSeparatorBent + 
                          IKeyStorageHelperBent.DotSeparatorBent
                        : cardDefaultBent.NameBent;

                    objectCardBent.transform.GetChild(3).GetComponent<Text>().text = cardDefaultBent.DetailsBent.Length > 35
                        ? cardDefaultBent.DetailsBent[..35] + IKeyStorageHelperBent.DotSeparatorBent + 
                          IKeyStorageHelperBent.DotSeparatorBent + 
                          IKeyStorageHelperBent.DotSeparatorBent
                        : cardDefaultBent.DetailsBent;
            
                    objectCardBent.GetComponent<CardDefaultBent>().cardBent = cardDefaultBent;
                    objectCardBent.GetComponent<CardDefaultBent>().cardIdBent = cardDefaultBent.IdBent;
                    
                    objectCardBent.GetComponent<IconControllerBent>().devicePathBent = IPathControllerBent.ContentModsPathBent;
                    
                    objectCardBent.GetComponent<CardDefaultBent>().cardTypeBent = ICategoryTypeHelperBent.ModsCategoryNameBent;
                    objectCardBent.GetComponent<CardDefaultBent>().filePathBent = cardDefaultBent.filePathBent;
                    objectCardBent.GetComponent<CardDefaultBent>().cardCategoryBent = cardDefaultBent.cardCategoryBent;
                    
                    
                    var newCardBent = objectCardBent.transform.GetChild(0).transform.GetChild(0).
                        transform.GetChild(0).gameObject;
                    
                    if (cardNewIdBent <= 2 && !newCardBent.activeSelf)
                    {
                        newCardBent.SetActive(true);
                        objectCardBent.GetComponent<CardDefaultBent>().isNewCardBent = true;
                        newCardsListBent.Add(objectCardBent.transform);
                    }

                    if (cardFavoriteBent)
                    {
                        objectCardBent.GetComponent<CardDefaultBent>().isFavoriteBent = true;
                        FavoritesListOperationsBent.AddedToFavoritesTransformListBent(objectCardBent.transform);
                    }
                    
                    cardsListBent.Add(objectCardBent.transform);
                    
                    cardNewIdBent++;
                    cardIdBent++;
                    cardDefaultBent = null;
                }
            }
        }
    }
}