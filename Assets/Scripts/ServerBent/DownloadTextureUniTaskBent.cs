﻿using System;
using System.IO;
using Cysharp.Threading.Tasks;
using InterfacesBent;
using UnityEngine;
using UnityEngine.Networking;
using UtilsBent;

namespace ServerBent
{
    public class DownloadTextureUniTaskBent 
    {
        public async UniTask<Texture2D> GetTextureBent(string devicePathBent, string serverPathBent, 
            string iconNameBent, string urlBent)
        {
            if (devicePathBent == null)
                if (serverPathBent == null)
                    if (iconNameBent == null)
                        if (urlBent == null )
                            TrashNullableMethodBent();
            var pathToIconsBent = Path.Combine(Application.persistentDataPath, devicePathBent);
            
            if (File.Exists(pathToIconsBent + iconNameBent))
                return await CreateTextureBent(serverPathBent, devicePathBent, iconNameBent);
            
            byte[] bytesBent = await DownloadTexturesAsyncBent(urlBent + IKeyStorageHelperBent.SlashSeparatorBent 
                                                                       + serverPathBent);

            if (!Directory.Exists(pathToIconsBent))
                Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, pathToIconsBent));
                
            await File.WriteAllBytesAsync(pathToIconsBent + iconNameBent, bytesBent);
            
            Array.Clear(bytesBent, 0, bytesBent.Length);
            
            return await CreateTextureBent(serverPathBent, devicePathBent, iconNameBent);
        }

        private async UniTask<Texture2D> CreateTextureBent(string serverPathBent, string devicePathBent, string iconNameBent)
        {
            if(serverPathBent == null)
                if (devicePathBent == null)
                    if (iconNameBent == null)
                        TrashNullableMethodBent();
            
            Texture2D textureBent = await BestOptimizationeverSHAKALBent.LoadTextureBent(serverPathBent, iconNameBent, devicePathBent);
            return textureBent;
        }

        private async UniTask<byte[]> DownloadTexturesAsyncBent(string imageUrlBent)
        {
            if (imageUrlBent != null)
            {
                using var requestBent = UnityWebRequest.Get(imageUrlBent);

                var operationsBent = requestBent.SendWebRequest();

                while (!operationsBent.isDone)
                    await UniTask.Yield();

                return requestBent.result == UnityWebRequest.Result.Success
                    ? requestBent.downloadHandler.data
                    : null;
            }
            else
            {
                TrashNullableMethodBent();
                return default;
            }
        }
        
        private void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                numberBent += secNumberBent;
            }
        }
    }
}
