using System;
using System.IO;
using Cysharp.Threading.Tasks;
using Firebase;
using Firebase.Extensions;
using Firebase.Storage;
using InterfacesBent;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace ServerBent
{
    public class DownloaderJsonAsyncBent : IPathControllerBent
    {
        public static async UniTask<TResultType> DownloadJsonAsyncBent<TResultType>(string urlBent, string fileNameBent)
        {
            if (urlBent == null) return default;
            
            var devicePathBent =
                Path.Combine(Application.persistentDataPath, IPathControllerBent.ContentJsonPathBent);

            using var requestBent = UnityWebRequest.Get(urlBent);

            var operationsBent = requestBent.SendWebRequest();

            while (!operationsBent.isDone) await UniTask.Yield();

            var jsonResponseBent = requestBent.downloadHandler.data;

            if (requestBent.result != UnityWebRequest.Result.Success)
                Debug.LogError($"{requestBent.result}: {requestBent.error}");
            else
            {
                if (!Directory.Exists(devicePathBent)) Directory.CreateDirectory(devicePathBent);

                await File.WriteAllBytesAsync(devicePathBent + fileNameBent, jsonResponseBent);

                var jsonBent = await File.ReadAllTextAsync(devicePathBent + fileNameBent);
                return JsonConvert.DeserializeObject<TResultType>(jsonBent);
            }

            return default;
        }
    }
}