using System;
using System.Threading.Tasks;
using Firebase;
using Firebase.Extensions;
using Firebase.Storage;
using InterfacesBent;
using UnityEngine;

namespace ServerBent
{
    public class FirebaseStorageControllerBent : MonoBehaviour
    {
        private FirebaseApp _firebaseAppBent;
        FirebaseStorage _storageBent;
        private StorageReference _gsReferenceBent;

        private void Start()
        {
            DontDestroyOnLoad(this);

            _firebaseAppBent = FirebaseApp.Create();
        }
        
        public async void FirebaseLinkBent(string localPathBent, Action<string> someShitToDoWithLinkBent)
        {
            _storageBent = FirebaseStorage.DefaultInstance;

            _gsReferenceBent = _storageBent.GetReferenceFromUrl(IHttpClientHelperBent.FirebaseUrlBent);
            
            StorageReference childBent = _gsReferenceBent.Child(localPathBent);
    
            await childBent.GetDownloadUrlAsync().ContinueWithOnMainThread(
                taskBent =>
                {
                    if (taskBent.IsFaulted || taskBent.IsCanceled) Debug.Log(taskBent.Exception);
                    else someShitToDoWithLinkBent?.Invoke(taskBent.Result.AbsoluteUri);
                });
        }
    }
}
