﻿using InterfacesBent;
using ModelsBent;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FavoriteBent : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Sprite _selectedSpriteBent;
    [SerializeField] private Sprite _defaultSpriteBent;
    [SerializeField] private CategoryButtonChangeBent categoryBent;
    [SerializeField] private Image favoriteIconBent;

    public Transform cardBent;

    public void OnPointerClick(PointerEventData eventData)
    {
        var cardTransformFavoriteBent = cardBent.GetChild(0).transform.GetChild(1).transform.GetChild(0);

        var categoryDefaultBent = cardBent.GetComponent<CardDefaultBent>();

        if (categoryDefaultBent.isFavoriteBent)
        {
            categoryDefaultBent.isFavoriteBent = false;

            if (favoriteIconBent != null)
                favoriteIconBent.sprite = _defaultSpriteBent;
            else
                gameObject.GetComponent<Image>().sprite = _defaultSpriteBent;

            cardTransformFavoriteBent.GetComponent<Image>().sprite = _defaultSpriteBent;

            FavoritesListOperationsBent.RemovedFromFavoriteListBent(cardBent.transform);
        }
        else
        {
            categoryDefaultBent.isFavoriteBent = true;

            if (favoriteIconBent != null)
                favoriteIconBent.sprite = _selectedSpriteBent;
            else
                gameObject.GetComponent<Image>().sprite = _selectedSpriteBent;

            cardTransformFavoriteBent.GetComponent<Image>().sprite = _selectedSpriteBent;

            FavoritesListOperationsBent.AddedToFavoritesListBent(cardBent.transform);
        }
        
        if (categoryBent.SelecetedCategoryBent.ToLower() == ICategoryTypeHelperBent.FavoritesSubcategoryNameBent)
            categoryBent.PrintFavoriteListBent(categoryBent._categoryNameBent);

        FavoritesListOperationsBent.SaveFavoriteListFromFileBent();
    }
}
