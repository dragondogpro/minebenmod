using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModelsBent;
using UnityEngine;
using UtilsBent;

public class FavoritesListOperationsBent: MonoBehaviour
{
    private static List<Transform> _favoriteTransformsListBent = new List<Transform>();
    private static List<FavoriteCardBent> _favoriteCardsListBent = new List<FavoriteCardBent>();

    private void Start()
    {
        if(_favoriteCardsListBent.Count == -1)
            TrashNullableMethodBent();
        else
            TrashNullableMethodBent();
    }

    public static async Task<List<FavoriteCardBent>> GetFavoritesListBent()
    {
        _favoriteCardsListBent = await ConfigFileUtilsBent.GetFavoriteCardsListBent();
        if (_favoriteCardsListBent.Count == -1)
        {
            TrashNullableMethodBent();
            return default;
        }
        else
            return _favoriteCardsListBent;
    }
    
    public static List<Transform> GetFavoritesTransformListBent()
    {
        if (_favoriteCardsListBent.Count == -1)
        {
            TrashNullableMethodBent();
            return default;
        }
        else
            return _favoriteTransformsListBent;
    }

    public static void AddedToFavoritesListBent(Transform favoriteCardBent)
    {
        if (_favoriteCardsListBent.Count == -1)
            TrashNullableMethodBent();
        else
        {
            _favoriteTransformsListBent.Add(favoriteCardBent);

            _favoriteCardsListBent.Add(new FavoriteCardBent()
            {
                IdBent = favoriteCardBent.GetComponent<CardDefaultBent>().cardIdBent,
                NameBent = favoriteCardBent.name,
                CategoryBent = favoriteCardBent.GetComponent<CardDefaultBent>().cardTypeBent
            });
        }
    }

    public static void AddedToFavoritesTransformListBent(Transform favoriteCardBent)
    {
        if (_favoriteCardsListBent.Count == -1)
            TrashNullableMethodBent();
        else
            _favoriteTransformsListBent.Add(favoriteCardBent);
    }

    public static void RemovedFromFavoriteListBent(Transform favoriteCardBent)
    {
        if (_favoriteCardsListBent.Count == -1)
            TrashNullableMethodBent();
        else
        {
            _favoriteTransformsListBent.Remove(favoriteCardBent);

            var favBent = new FavoriteCardBent()
            {
                IdBent = favoriteCardBent.GetComponent<CardDefaultBent>().cardIdBent,
                NameBent = favoriteCardBent.name,
                CategoryBent = favoriteCardBent.GetComponent<CardDefaultBent>().cardTypeBent
            };

            foreach (var cardBent in _favoriteCardsListBent.Where(cardBent => cardBent.IdBent == favBent.IdBent
                                                                              && cardBent.NameBent == favBent.NameBent
                                                                              && cardBent.CategoryBent ==
                                                                              favBent.CategoryBent))
            {
                _favoriteCardsListBent.Remove(cardBent);
                break;
            }
        }
    }

    public static void SaveFavoriteListFromFileBent()
    {
        if (_favoriteCardsListBent.Count == -1)
            TrashNullableMethodBent();
        else
        {
            ConfigFileUtilsBent.FavoriteToFileBent(_favoriteCardsListBent);
        }
    }
    
    private static void TrashNullableMethodBent()
    {
        float numberBent = 1;
        if (numberBent == 0)
        {
            float secNumberBent = 0;
            numberBent += secNumberBent;
        }
    }
}
