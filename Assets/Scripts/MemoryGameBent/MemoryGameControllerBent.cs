using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using CategoriesBent;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using IOSBridge;
using ModelsBent;
using Plugins.Utils;
using ServerBent;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Random = System.Random;

public class MemoryGameControllerBent : MonoBehaviour
{
    [SerializeField] private Button[] _buttonDifficultyArrayBent;
    [SerializeField] private Button[] _cardArrayBent;
    [SerializeField] private Transform _contentCardBent;
    [SerializeField] private GameObject _difficultyPanelBent;
    [SerializeField] private GameObject _gamePanelBent;
    [SerializeField] private GameObject _winPanelBent;
    [SerializeField] private GameObject _losePanelBent;
    [SerializeField] private Button _tryAgainBent;
    [SerializeField] private Button _tryAgain1Bent;
    [SerializeField] private Button _nextLevelBent;
    [SerializeField] private Button _nextLevel1Bent;
    [SerializeField] private Button _backButtonBent;
    [SerializeField] private Text _textTimerBent;
    [Space] [SerializeField] private GameObject _menuHeaderPanelBent;
    [SerializeField] private GameObject _gameHeaderPanelBent;

    [Space] [SerializeField] private SkinsBent _skinsBentListBent;
    [SerializeField] private RawImage _skinImageContainerBent;
    [SerializeField] private GameObject _SkinPriseBent;
    [SerializeField] private Button _downloadButtonBent;
    [SerializeField] private Button _installButtonBent;
    [SerializeField] private Button ExitButtonBent;

    private List<GameObject> intListBent = new List<GameObject>();
    private List<Button> buttonsListBent = new List<Button>();
    private List<GameObject> testBent = new List<GameObject>();
    private Random randomBent = new Random();

    private int countBent;
    private GameObject _firstBent;
    private GameObject _secondBent;
    private float timeLeftBent;
    private bool timerTickBent = false;
    
    private string SuccessOperationBent = "DownloadBent".Remove("DownloadBent".Length - 4);
    private string _devicePathBent = "Data/FilesBent/Bent".Remove("Data/FilesBent/Bent".Length - 4);

    private string urlBent;
    private string _filePathBent;

    // [SerializeField] private RawImage _priseHorror;
    // [SerializeField] private RectTransform _chestHorror;
    // [SerializeField] private List<Texture2D> _giftsHorror;
    //
    // private int RandomNum;
    private int _winCountBent;
    private string _fileNameBent;

    private void Start()
    {
        _firstBent = null;
        _secondBent = null;

        _buttonDifficultyArrayBent[0].onClick.AddListener(delegate { GenerateCardBent(6); });
        _buttonDifficultyArrayBent[1].onClick.AddListener(delegate { GenerateCardBent(8); });
        _buttonDifficultyArrayBent[2].onClick.AddListener(delegate { GenerateCardBent(10); });
        _buttonDifficultyArrayBent[3].onClick.AddListener(delegate { GenerateCardBent(12); });
        _buttonDifficultyArrayBent[4].onClick.AddListener(delegate { GenerateCardBent(14); });

        _tryAgainBent.onClick.AddListener(TryAgainBent);
        _tryAgain1Bent.onClick.AddListener(TryAgainBent);
        _backButtonBent.onClick.AddListener(BackButtonBent);
        _nextLevelBent.onClick.AddListener(NextLevelBent);
        _nextLevel1Bent.onClick.AddListener(NextLevelBent);
        ExitButtonBent.onClick.AddListener(ExitOnRandomSkinBent);


        if (PlayerPrefs.HasKey("TimeBent") &&
            (DateTime.Parse(PlayerPrefs.GetString("TimeBent")) - DateTime.UtcNow).TotalSeconds > 0)
            StartCoroutine(TimerBent());
    }

    private void ExitOnRandomSkinBent()
    {
        _SkinPriseBent.gameObject.SetActive(false);
        _skinImageContainerBent.texture = null;
        _SkinPriseBent.transform.GetChild(1).gameObject.SetActive(true);
    }

    private void GenerateCardBent(int diffNumberBent)
    {
        while (_contentCardBent.childCount > 0)
        {
            DestroyImmediate(_contentCardBent.GetChild(0).gameObject);
        }

        _menuHeaderPanelBent.gameObject.SetActive(false);
        _gameHeaderPanelBent.gameObject.SetActive(true);

        countBent = 0;
        intListBent.Clear();
        buttonsListBent.Clear();

        buttonsListBent = _cardArrayBent.ToList();


        _gamePanelBent.SetActive(true);
        _difficultyPanelBent.SetActive(false);

        switch (diffNumberBent)
        {
            case 6:
                RandomCardBent(6);
                timeLeftBent = 30;
                _winCountBent = 6;
                break;

            case 8:
                RandomCardBent(8);
                timeLeftBent = 27;
                _winCountBent = 8;
                break;

            case 10:
                RandomCardBent(10);
                timeLeftBent = 25;
                _winCountBent = 10;
                break;

            case 12:
                RandomCardBent(12);
                timeLeftBent = 23;
                _winCountBent = 12;
                break;

            case 14:
                RandomCardBent(14);
                timeLeftBent = 20;
                _winCountBent = 14;
                break;
        }

        for (int iBent = intListBent.Count - 1; iBent >= 1; iBent--)
        {
            int jBent = randomBent.Next(iBent + 1);
            var tempBent = intListBent[jBent];
            intListBent[jBent] = intListBent[iBent];
            intListBent[iBent] = tempBent;
        }

        foreach (var gameObjBent in intListBent)
        {
            var gameobjBent = Instantiate(gameObjBent, _contentCardBent);
            gameobjBent.gameObject.SetActive(true);
            gameobjBent.GetComponent<Button>().onClick.AddListener(delegate { LogicCardBent(gameobjBent.transform); });
        }

        timerTickBent = true;
    }

    private void RandomCardBent(int difficultyIntBent)
    {
        while (intListBent.Count < difficultyIntBent)
        {
            int randNumberBent = randomBent.Next(1, buttonsListBent.Count);

            intListBent.Add(buttonsListBent[randNumberBent - 1].gameObject);
            intListBent.Add(buttonsListBent[randNumberBent - 1].gameObject);

            buttonsListBent.RemoveAt(randNumberBent - 1);
        }
    }

    private void LogicCardBent(Transform transformBent)
    {
        transformBent.GetChild(0).gameObject.SetActive(false);
        transformBent.GetChild(1).gameObject.SetActive(true);
        Debug.Log("dadasdasdasdsadasdasdasdsadasdasdsadasdasdasdasdasdasdasdasdsa");
        
        countBent++;

        if (_firstBent == null)
        {
            _firstBent = transformBent.gameObject;
            _firstBent.gameObject.GetComponent<Button>().interactable = false;
                    
        }
        else
        {
            _secondBent = transformBent.gameObject;
            _secondBent.gameObject.GetComponent<Button>().interactable = false;    
        }


        if (_firstBent != null && _secondBent != null)
        {

            if (_firstBent.name != _secondBent.name)
            {
                _firstBent.transform.GetChild(0).gameObject.SetActive(true);
                _firstBent.transform.GetChild(1).gameObject.SetActive(false);
                _secondBent.transform.GetChild(0).gameObject.SetActive(true);
                _secondBent.transform.GetChild(1).gameObject.SetActive(false);
            }
            else if (_firstBent.name == _secondBent.name)
            {
                testBent.Add(_firstBent);
                testBent.Add(_secondBent);

                _firstBent.gameObject.GetComponent<Button>().enabled = false;
                _secondBent.gameObject.GetComponent<Button>().enabled = false;
            }

           _secondBent.gameObject.GetComponent<Button>().interactable =true;   
           _firstBent.gameObject.GetComponent<Button>().interactable = true;
            _firstBent = null;
            _secondBent = null;
        }
    }

    private void Update()
    {
        if (timerTickBent)
        {
            timeLeftBent -= Time.deltaTime;
            _textTimerBent.text = Convert.ToInt32(timeLeftBent).ToString();
            if (timeLeftBent < 0)
            {
                _losePanelBent.gameObject.SetActive(true);
                _winPanelBent.gameObject.SetActive(false);
                _gamePanelBent.gameObject.SetActive(false);
                _menuHeaderPanelBent.gameObject.SetActive(true);
                _gameHeaderPanelBent.gameObject.SetActive(false);
                timerTickBent = false;
            }
        }

        if (testBent.Count == _winCountBent && testBent.Count != 0)
        {
            _winPanelBent.gameObject.SetActive(true);
            _gamePanelBent.gameObject.SetActive(false);
            _losePanelBent.gameObject.SetActive(false);
            _menuHeaderPanelBent.gameObject.SetActive(true);
            _gameHeaderPanelBent.gameObject.SetActive(false);

            if (PlayerPrefs.HasKey("TimeBent") &&
                (DateTime.Parse(PlayerPrefs.GetString("TimeBent")) - DateTime.UtcNow).TotalSeconds <= 0)
                StagesHorror();

            
            testBent.Clear();
            timerTickBent = false;
        }
    }

    private void TryAgainBent()
    {
        switch (_contentCardBent.childCount)
        {
            case 6:
                timeLeftBent = 30;
                break;

            case 8:
                timeLeftBent = 27;
                break;

            case 10:
                timeLeftBent = 25;
                break;

            case 12:
                timeLeftBent = 23;
                break;

            case 14:
                timeLeftBent = 20;
                break;
        }

        for (int iBent = 0; iBent < _contentCardBent.childCount; iBent++)
        {
            _contentCardBent.GetChild(iBent).GetChild(0).gameObject.SetActive(true);
            _contentCardBent.GetChild(iBent).GetChild(1).gameObject.SetActive(false);
            _contentCardBent.GetChild(iBent).GetComponent<Button>().interactable = true;
            _contentCardBent.GetChild(iBent).GetComponent<Button>().enabled = true;
        }

        testBent.Clear();

        _gamePanelBent.gameObject.SetActive(true);
        _winPanelBent.gameObject.SetActive(false);
        _losePanelBent.gameObject.SetActive(false);
        _menuHeaderPanelBent.gameObject.SetActive(false);
        _gameHeaderPanelBent.gameObject.SetActive(true);

        timerTickBent = true;
    }

    private void NextLevelBent()
    {
        testBent.Clear();
        _menuHeaderPanelBent.gameObject.SetActive(true);
        _gameHeaderPanelBent.gameObject.SetActive(false);


        while (_contentCardBent.childCount > 0)
        {
            DestroyImmediate(_contentCardBent.GetChild(0).gameObject);
        }

        timerTickBent = false;

        _winPanelBent.SetActive(false);
        _losePanelBent.SetActive(false);
        _difficultyPanelBent.SetActive(true);
    }

    private void BackButtonBent()
    {
        _menuHeaderPanelBent.gameObject.SetActive(true);
        _gameHeaderPanelBent.gameObject.SetActive(false);

        _gamePanelBent.SetActive(false);
        _difficultyPanelBent.SetActive(true);

        while (_contentCardBent.childCount > 0)
        {
            DestroyImmediate(_contentCardBent.GetChild(0).gameObject);
        }

        timerTickBent = false;
    }


    private async void StagesHorror()
    {
        //string path = Application.persistentDataPath + "/Res/" + RandomNum + "gift1.png";
        //await File.WriteAllBytesAsync(path, _giftsHorror[RandomNum].EncodeToPNG());
        //IOStoUnityBridge.SaveImageToAlbum(path);

        SetPrise();

        PlayerPrefs.SetString("TimeBent", DateTime.UtcNow.AddMinutes(25d).ToString());
        //StartCoroutine(TimerBent());
    }

    private void SetPrise()
    {
        var RandomNum = randomBent.Next(1, _skinsBentListBent.cardsListBent.Count - 1);

        string texturePathBent = _skinsBentListBent.cardsListBent[RandomNum].gameObject.GetComponent<CardDefaultBent>()
            .IconPathBent;


        _SkinPriseBent.GetComponent<IconControllerBent>().iconNameBent = _skinsBentListBent
            .cardsListBent[RandomNum].gameObject.GetComponent<IconControllerBent>().iconNameBent;
        _SkinPriseBent.GetComponent<IconControllerBent>().iconPathBent = _skinsBentListBent
            .cardsListBent[RandomNum].gameObject.GetComponent<IconControllerBent>().iconPathBent;

        _fileNameBent = _skinsBentListBent
            .cardsListBent[RandomNum].gameObject.GetComponent<CardDefaultBent>().filePathBent;
        
       
        //_skinImageContainerBent.texture = _skinsBentListBent.cardsListBent[RandomNum]
        //    .transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).GetComponent<RawImage>().texture;

        _downloadButtonBent.gameObject.SetActive(true);
        _installButtonBent.gameObject.SetActive(false);
        _SkinPriseBent.SetActive(true);
    }


    public  void DownloadFileBent()
    {
        _downloadButtonBent.transform.GetChild(0).GetComponent<Text>().text = "Downloading..";
        var firebaseStorageControllerBentBent = new FirebaseStorageControllerBent();
        firebaseStorageControllerBentBent.FirebaseLinkBent(_fileNameBent, linkBent => 
            GetFilePathBent(linkBent.ToString()));

        
    }
    private async void GetFilePathBent(string urlBent1)
    {
        urlBent = urlBent1;
        _filePathBent = await GetDownloadFilePathBent();
        Debug.Log(_filePathBent);
    }

    private IEnumerator TimerBent()
    {
        DateTime TargetDTBent = DateTime.Parse(PlayerPrefs.GetString("TimeBent"));
        TimeSpan deltaBent = TargetDTBent - DateTime.UtcNow;
        while (deltaBent.TotalSeconds > 0)
        {
            deltaBent = TargetDTBent - DateTime.UtcNow;
            //_btnTextHorror.text = delta.ToString(@"mm\:ss");
//            Debug.Log(deltaBent.ToString(@"mm\:ss"));
            yield return new WaitForSeconds(1f);
        }

        //_btnTextHorror.text = "Open dower chest";
        //GetComponent<Image>().color = new(0.4745098f, 0.04705883f, 0.04705883f, 1f);
    }

    private async UniTask<string> GetDownloadFilePathBent()
    {
        var devicePathBent = Path.Combine(Application.persistentDataPath, _devicePathBent);

        if (File.Exists(devicePathBent + _fileNameBent.Split('/').Last()))
        {
            return devicePathBent + _fileNameBent.Split('/').Last();
        }
        else
        {
            using var requestBent = UnityWebRequest.Get(urlBent);
            Debug.Log(urlBent);
            var operationsBent = requestBent.SendWebRequest();

            while (!operationsBent.isDone)
                await UniTask.Yield();

            var jsonResponseBent = requestBent.downloadHandler.data;

            if (requestBent.result != UnityWebRequest.Result.Success)
                Debug.LogError($"{requestBent.result}: {requestBent.error}");
            else
            {
                if (!Directory.Exists(devicePathBent))
                    Directory.CreateDirectory(devicePathBent);

                await File.WriteAllBytesAsync(devicePathBent + _fileNameBent.Split('/').Last(), jsonResponseBent);

                _downloadButtonBent.transform.GetChild(0).GetComponent<Text>().text = "Download";
                _downloadButtonBent.gameObject.SetActive(false);
                _installButtonBent.gameObject.SetActive(true);
                
                Debug.Log(devicePathBent + _fileNameBent.Split('/').Last());
                return devicePathBent + _fileNameBent.Split('/').Last();
            }

            return default;
        }
    }
    
    public void InstallSkinButtonOnClickBent()
    {
        bool trashIntBEnt = false;
        bool trashIntBEnt1 = false;
        if (trashIntBEnt == true)
            trashIntBEnt = !true;
        
        var fileZipCreatorBent = new FileZipCreator();
        Debug.Log(_filePathBent);
        _filePathBent = Path.GetFileNameWithoutExtension(_filePathBent);
       
        var applPathBent = $"{Application.persistentDataPath}/{_devicePathBent + _fileNameBent.Split("/").Last()}";
 Debug.Log(applPathBent);
        IOSBridge.IOStoUnityBridge.InitWithActivity(
            fileZipCreatorBent.CreateSkinFile(applPathBent, _filePathBent, _filePathBent));

        IOSBridge.IOStoUnityBridge.ShowAlert(SuccessOperationBent, "");
        
    }
}