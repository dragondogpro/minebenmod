﻿using System.Linq;
using InterfacesBent;
using ModelsBent;
using UnityEngine;
using UnityEngine.UI;

public class CategoryButtonChangeBent : MonoBehaviour
{
    [SerializeField] private CheckCategoryBent categoryBentObjectBent;
    [SerializeField] private Sprite _selectedSpriteBent;
    [SerializeField] private Sprite _defaultSpriteBent;
    [SerializeField] private Color _selectedColorBent;
    [SerializeField] private Color _defaultColorBent;
    [SerializeField] private GameObject[] _categoryPanelsBent;
    [SerializeField] private ScrollRect _scrollRectBent;
    
    public string _categoryNameBent;
    public string SelecetedCategoryBent;
    
    public void PrintCategoryBent(GameObject categoryPanelBent)
    {
        _scrollRectBent.verticalNormalizedPosition = 1f;
        var categoryListBent = categoryBentObjectBent.GetCategoryListBent(_categoryNameBent);
        var favoriteListBent = FavoritesListOperationsBent.GetFavoritesTransformListBent();

        foreach (var panelBent in _categoryPanelsBent)
        {
            panelBent.GetComponent<Image>().sprite = _defaultSpriteBent;
            panelBent.transform.GetChild(0).GetComponent<Text>().color = _defaultColorBent;
        }

        var categoryBent = categoryPanelBent.transform.GetChild(0).gameObject;
        categoryPanelBent.GetComponent<Image>().sprite = _selectedSpriteBent;
        categoryPanelBent.transform.GetChild(0).GetComponent<Text>().color = _selectedColorBent;
        SelecetedCategoryBent = categoryBent.GetComponent<Text>().text;
        
        foreach (var cardBent in categoryListBent)
        {
            switch (categoryBent.transform.GetComponent<Text>().text.ToLower())
            {
                case ICategoryTypeHelperBent.FavoritesSubcategoryNameBent:
                    cardBent.gameObject.SetActive(favoriteListBent.Contains(cardBent));
                    break;

                case ICategoryTypeHelperBent.NewSubcategoryNameBent:
                    var lastCardListBent = categoryBentObjectBent.GetNewCardListBent(_categoryNameBent);
                    cardBent.gameObject.SetActive(lastCardListBent.Contains(cardBent));
                    break;

                default:
                    var categoryWithTypeCardBent = cardBent.GetComponent<CardDefaultBent>().cardCategoryBent;

                    var categorySelectedCardBent = categoryBent.transform.GetComponent<Text>().text.ToLower();

                    if ( categoryWithTypeCardBent == categorySelectedCardBent || categorySelectedCardBent == 
                        ICategoryTypeHelperBent.AllSubcategoryNameBent)
                    {
                        cardBent.gameObject.SetActive(true);
                    }
                    else
                        cardBent.gameObject.SetActive(false);

                    break;
            }
        }
    }

    public void PrintFavoriteListBent(string categoryNameBent)
    {
        if (categoryNameBent != "")
        {
            var categoryListBent = categoryBentObjectBent.GetCategoryListBent(categoryNameBent);
            var favoriteListBent = FavoritesListOperationsBent.GetFavoritesTransformListBent();

            foreach (var cardBent in categoryListBent)
                cardBent.gameObject.SetActive(favoriteListBent.Contains(cardBent));
        }
    }
}
