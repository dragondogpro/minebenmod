using System.Collections.Generic;
using Newtonsoft.Json;

namespace ModelsBent
{
    public class FavoriteJsonBent
    {
        [JsonProperty("favorite_cardBent")]
        public List<FavoriteCardBent> FavoriteCardsBent { get; set; }
        
        private void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                
                numberBent += secNumberBent;
            }
        }
    }
    
    public class FavoriteCardBent
    {
        [JsonProperty("idBent")] public int IdBent { get; set; }

        [JsonProperty("nameBent")] public string NameBent { get; set; }
        
        [JsonProperty("categoryBent")] public string CategoryBent { get; set; }
        
        private void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                
                numberBent += secNumberBent;
            }
        }
    }
}
