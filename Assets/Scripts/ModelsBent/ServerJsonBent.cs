﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ModelsBent
{
    public class ServerJsonBent
    {
        [JsonProperty("wxdqog3dd_list")]
        public List<ServerCardBent> ServerCardsBent { get; set; }
        
        private void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                
                numberBent += secNumberBent;
            }
        }
    }

    public class ServerCardBent
    {
        [JsonProperty("o5fp8")] public long O5Fp8 { get; set; }

        [JsonProperty("34o5yp")] public string The34O5Yp { get; set; }

        [JsonProperty("wxdqog3dd")] public long Wxdqog3Dd { get; set; }

        [JsonProperty("zarg")] public string Zarg { get; set; }

        [JsonProperty("xjki64")] public string Xjki64 { get; set; }

        [JsonProperty("5alupx")] public string The5Alupx { get; set; }

        [JsonProperty("eb2wqwc")] public string Eb2Wqwc { get; set; }

        [JsonProperty("0r5ou9")] public string The0R5Ou9 { get; set; }
        
        private void TrashNullableMethodBent()
        {
            float numberBent1 = 2;
            if (numberBent1 == 1)
            {
                float secNumberBent = 1;
                
                numberBent1 += secNumberBent;
            }
        }
    }
}