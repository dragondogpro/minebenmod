﻿using InterfacesBent;
using ServerBent;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ModelsBent
{
    public class CardDefaultBent : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private GameObject _cardDetailsPaneBent;
        [SerializeField] private GameObject _cardNameBent;
        [SerializeField] private GameObject _cardDescriptionBent;
        [SerializeField] private GameObject _cardIconBent;
        [SerializeField] private GameObject _buttonFavBent;
        [SerializeField] private GameObject _buttonSeedCopyBent;
        
        [SerializeField] private Sprite _selectedSpriteBent;
        [SerializeField] private Sprite _defaultSpriteBent;

        public CardDefaultBent cardBent;
        public int cardIdBent;
        public string cardCategoryBent;
        public string cardTypeBent;
        public bool isNewCardBent;
        public bool isFavoriteBent;
        public string filePathBent;
        public string cardServerPortBent;
        public string cardServerIPBent;
        public string cardSeedIdBent;

        public int IdBent { get; set; }
        public string NameBent { get; set; }
        public string DetailsBent { get; set; }
        public string IconPathBent { get; set; }
        public string CategoryBent { get; set; }
        public bool FavoriteBent { get; set; }
        public string ServerPortBent { get; set; }
        public string ServerIPBent { get; set; }
        public string SeedIdBent { get; set; }

        private void Awake()
        {
            if (_selectedSpriteBent == null)
            {
                if (_defaultSpriteBent == null)
                {
                    int keyBent = 9;
                    if (keyBent > 10)
                        keyBent = 10;
                }
            }

            if (isFavoriteBent) transform.GetChild(0).transform.GetChild(1).transform.GetChild(0).
                GetComponent<Image>().sprite = _selectedSpriteBent;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            if (cardTypeBent == ICategoryTypeHelperBent.SkinsCategoryNameBent)
                _cardDetailsPaneBent.transform.GetChild(0).transform.GetChild(2).gameObject.SetActive(isNewCardBent);
            else
                _cardIconBent.transform.GetChild(2).gameObject.SetActive(isNewCardBent);

            if (cardTypeBent == ICategoryTypeHelperBent.ServersCategoryNameBent)
            {
                _cardDetailsPaneBent.transform.GetChild(2).transform.GetChild(1).transform.GetChild(0).GetComponent<Text>().text = cardServerIPBent;
                _cardDetailsPaneBent.transform.GetChild(2).transform.GetChild(2).transform.GetChild(0).GetComponent<Text>().text = cardServerPortBent;
            }
            
            if (_cardDetailsPaneBent.GetComponent<DownloadButtonBent>() != null)
            {
                FirebaseStorageControllerBent firebaseStorageControllerBentBent = new FirebaseStorageControllerBent();
                firebaseStorageControllerBentBent.FirebaseLinkBent(filePathBent, linkBent => SetDownloadFileInfo(linkBent));
            }
            
            if (cardBent.NameBent != null)
                _cardNameBent.GetComponent<Text>().text = cardBent.NameBent;

            if (_cardDescriptionBent != null)
                _cardDescriptionBent.GetComponent<Text>().text = cardBent.DetailsBent;

            if (_buttonSeedCopyBent != null)
            {
                _buttonSeedCopyBent.GetComponent<CopyServerDetailsBent>().seedTextNumBent = cardSeedIdBent;
            }
            
            _buttonFavBent.transform.GetChild(0).GetComponent<Image>().sprite = 
                isFavoriteBent ? _selectedSpriteBent : _defaultSpriteBent;
            _buttonFavBent.GetComponent<FavoriteBent>().cardBent = transform;
            
            _cardIconBent.transform.GetChild(0).GetComponent<RawImage>().texture =
                gameObject.GetComponent<IconControllerBent>().textureBent;
        }

        private void SetDownloadFileInfo(string urlBent)
        {
            _cardDetailsPaneBent.GetComponent<DownloadButtonBent>().filePathFromServerBent = filePathBent;
            _cardDetailsPaneBent.GetComponent<DownloadButtonBent>().urlBent = urlBent;
        }
    }
}