﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ModelsBent
{
    public class ContentJsonBent
    {
        [JsonProperty("3zltop9")]
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> s3zltop9 { get; set; }
		
        [JsonProperty("48c8dhv")]
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> d48c8dhv { get; set; }
		
        [JsonProperty("b-6657")]
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> B6657 { get; set; }
		
        [JsonProperty("mjkbb")]
        public Dictionary<string, Dictionary<string, Dictionary<string, string>>> Mjkbb { get; set; }
        
        private void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                
                numberBent += secNumberBent;
            }
        }
    }
}
