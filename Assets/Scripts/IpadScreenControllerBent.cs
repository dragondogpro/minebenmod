using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IpadScreenControllerBent : MonoBehaviour
{
    [SerializeField] private ScrollRect _firstScrollBent;
    [SerializeField] private HorizontalLayoutGroup _firstHLGBent;
    [SerializeField] private ScrollRect _secondScrollBent;
    [SerializeField] private HorizontalLayoutGroup _secondHLGBent;
    [SerializeField] private Slider _firstSBBent;
    [SerializeField] private Slider _secondSBBent;
    [SerializeField] private Canvas _canvasBent;

    private void Start()
    {
        PlayerPrefs.SetInt("CloseSubBent", 0);
        
        if ((float)Screen.height / Screen.width < 1.5f)
        {
            _canvasBent.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.3f;
        }

        if (Screen.height == 1334 && Screen.width == 750)
        {
            _canvasBent.GetComponent<CanvasScaler>().matchWidthOrHeight = 0.4f;
        }
        
        if (Application.systemLanguage == SystemLanguage.Arabic ||
            Application.systemLanguage == SystemLanguage.Hebrew)
        {
            
        
            
            _firstScrollBent.horizontalNormalizedPosition = 1;
            _firstHLGBent.reverseArrangement = true;
            _secondScrollBent.horizontalNormalizedPosition = 1;
            _secondHLGBent.reverseArrangement = true;

            _firstSBBent.direction = Slider.Direction.RightToLeft;
            _secondSBBent.direction = Slider.Direction.RightToLeft;
        }
        else
        {
           
        }
    }
}
