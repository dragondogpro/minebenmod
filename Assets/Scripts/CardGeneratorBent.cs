﻿using System.Collections.Generic;
using CategoriesBent;
using InterfacesBent;
using ModelsBent;
using ServerBent;
using UnityEngine;

public class CardGeneratorBent : MonoBehaviour
{
    [SerializeField] private GameObject _dataGameObjectBent;

    public static ModelsBent.ContentJsonBent ContentJsonBent = new ModelsBent.ContentJsonBent();
    public static ServerJsonBent ServerJsonBent = new ServerJsonBent();
    public static List<FavoriteCardBent> FavoriteCardsListBent = new List<FavoriteCardBent>();

    private async void Start()
    {
        FirebaseStorageControllerBent firebaseStorageControllerBent = new FirebaseStorageControllerBent();
        firebaseStorageControllerBent.FirebaseLinkBent(IPathControllerBent.ContentJsonNameBent, 
            linkBent => GetContentJsonBent(linkBent.ToString()));
        firebaseStorageControllerBent.FirebaseLinkBent(IPathControllerBent.ServersJsonNameBent, 
            linkBent => GetServersJsonBent(linkBent.ToString()));

        FavoriteCardsListBent = await FavoritesListOperationsBent.GetFavoritesListBent();
    }

    private async void GetContentJsonBent(string uriBent)
    {
        ContentJsonBent =
            await DownloaderJsonAsyncBent.DownloadJsonAsyncBent<ModelsBent.ContentJsonBent>(uriBent,
                IPathControllerBent.ContentJsonNameBent);
        _dataGameObjectBent.SetActive(true);
    }

    private async void GetServersJsonBent(string uriBent)
    {
        ServerJsonBent =
            await DownloaderJsonAsyncBent.DownloadJsonAsyncBent<ServerJsonBent>(uriBent,
                IPathControllerBent.ServersJsonNameBent);
        _dataGameObjectBent.GetComponent<ServersBent>().enabled = true;
    }
}