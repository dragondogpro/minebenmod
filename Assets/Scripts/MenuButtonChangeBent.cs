﻿using System;
using InterfacesBent;
using UnityEngine;
using UnityEngine.UI;

public class MenuButtonChangeBent : MonoBehaviour
{
    [SerializeField] private Button[] _menuButtonsBent;
    [SerializeField] private GameObject[] _categorysBent;
    [SerializeField] private Transform _menuPanelBent;
    [SerializeField] private Transform _menuPanelSkinMakerBent;
    
    [SerializeField] private Transform _skinMakerButtonBent;
    [SerializeField] private Transform _skinMakerPanelBent;
    [SerializeField] private Transform _canvasPanelBent;

    private void Start()
    {
        _skinMakerButtonBent.GetComponent<Button>().onClick.AddListener(delegate
        {
            PrintCategoryBent(_skinMakerButtonBent.GetComponent<Button>()); 
        });
    }

    public void PrintCategoryBent(Button clickedButtonBent)
    {
        foreach (var buttonBent in _menuButtonsBent)
            buttonBent.GetComponent<Outline>().enabled = false;

        
        if (clickedButtonBent.gameObject.name == "Skin MakerBent".Remove("Skin MakerBent".Length - 4))
        {
            _skinMakerPanelBent.gameObject.SetActive(true);
            _canvasPanelBent.gameObject.SetActive(false);
            _skinMakerButtonBent.GetComponent<Outline>().enabled = true;
        }
        else
        {
            clickedButtonBent.GetComponent<Outline>().enabled = true;
            _skinMakerButtonBent.GetComponent<Outline>().enabled = false;

            _skinMakerPanelBent.gameObject.SetActive(false);
            _canvasPanelBent.gameObject.SetActive(true);
            
            foreach (var categoryBent in _categorysBent)
            {
                categoryBent.SetActive(categoryBent.name == clickedButtonBent.GetComponent<Text>().text);
                if (categoryBent.name == ICategoryTypeHelperBent.MemoryGameCategoryNameBent)
                {
                    categoryBent.transform.GetChild(1).gameObject.SetActive(true);
                    categoryBent.transform.GetChild(2).gameObject.SetActive(false);
                    categoryBent.transform.GetChild(3).gameObject.SetActive(false);
                    categoryBent.transform.GetChild(4).gameObject.SetActive(false);
                    continue;
                }
                
                if (categoryBent.name == ICategoryTypeHelperBent.SkinRandomCategoryNameBent) continue;
                
                if (categoryBent.activeSelf)
                    categoryBent.transform.GetChild(0).transform.GetChild(1).gameObject.GetComponent<ScrollRect>()
                        .verticalNormalizedPosition = 1f;
            
                if (categoryBent.name == ICategoryTypeHelperBent.SkinsCategoryNameBent || 
                    categoryBent.name == ICategoryTypeHelperBent.MemoryGameCategoryNameBent) continue;
            
                var searchCloseButtonBent = categoryBent.transform.GetChild(0).transform.GetChild(0).transform
                    .GetChild(1).transform.GetChild(3).gameObject;
            
                if (searchCloseButtonBent.activeSelf)
                    searchCloseButtonBent.GetComponent<Button>().onClick.Invoke();
            }
            
            /*if (_skinMakerPanelBent.gameObject.activeSelf)
            {
                for (int i = 0; i < _menuButtonsBent.Length; i++)
                {
                    
                }
            }*/
        }

        _menuPanelBent.gameObject.SetActive(false);
        //_menuPanelSkinMakerBent.gameObject.SetActive(false);
    }
    
}
