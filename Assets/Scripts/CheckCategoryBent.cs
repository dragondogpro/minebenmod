using System.Collections.Generic;
using System.Linq;
using CategoriesBent;
using InterfacesBent;
using ModelsBent;
using UnityEngine;

public class CheckCategoryBent : MonoBehaviour
{
    [SerializeField] private GameObject dataCategoryBent;

    public List<Transform> GetCategoryListBent(string categoryNameBent)
    {
        if (dataCategoryBent != null)
        {
            return categoryNameBent switch
            {
                ICategoryTypeHelperBent.MapsCategoryNameBent => dataCategoryBent.GetComponent<MapsBent>().cardsListBent,
                ICategoryTypeHelperBent.ModsCategoryNameBent => dataCategoryBent.GetComponent<ModsBent>().cardsListBent,
                ICategoryTypeHelperBent.SkinsCategoryNameBent => dataCategoryBent.GetComponent<SkinsBent>().cardsListBent,
                ICategoryTypeHelperBent.ServersCategoryNameBent => dataCategoryBent.GetComponent<ServersBent>()
                    .cardsListBent,
                ICategoryTypeHelperBent.SeedsCategoryNameBent => dataCategoryBent.GetComponent<SeedsBent>().cardsListBent,
                _ => default
            };
        }
        else
        {
            return default;
        }
    }
    
    public List<Transform> GetNewCardListBent(string categoryNameBent)
    {
        if (dataCategoryBent != null)
        {
            return categoryNameBent switch
            {
                ICategoryTypeHelperBent.MapsCategoryNameBent => dataCategoryBent.GetComponent<MapsBent>()
                    .newCardsListBent,
                ICategoryTypeHelperBent.ModsCategoryNameBent => dataCategoryBent.GetComponent<ModsBent>()
                    .newCardsListBent,
                ICategoryTypeHelperBent.SkinsCategoryNameBent => dataCategoryBent.GetComponent<SkinsBent>()
                    .newCardsListBent,
                ICategoryTypeHelperBent.ServersCategoryNameBent => dataCategoryBent.GetComponent<ServersBent>()
                    .newCardsListBent,
                ICategoryTypeHelperBent.SeedsCategoryNameBent => dataCategoryBent.GetComponent<SeedsBent>()
                    .newCardsListBent,
                _ => default
            };
        }
        else
        {
            return default;
        }
    }

    public List<Transform> GetSubcategoryListBent(string subcategoryNameBent, string categoryNameBent)
    {
        if (dataCategoryBent != null)
        {
            var listBent = GetCategoryListBent(categoryNameBent);

            return subcategoryNameBent switch
            {
                ICategoryTypeHelperBent.AllSubcategoryNameBent => listBent,
                ICategoryTypeHelperBent.FavoritesSubcategoryNameBent => FavoritesListOperationsBent
                    .GetFavoritesTransformListBent(),
                ICategoryTypeHelperBent.NewSubcategoryNameBent => GetNewCardListBent(categoryNameBent),
                _ => listBent.Where(x => x.GetComponent<CardDefaultBent>().cardCategoryBent == subcategoryNameBent)
                    .ToList()
            };
        }
        else
        {
            return default;
        }
    }

    private static void TrashNullableMethodBent()
    {
        float numberBent = 1;
        if (numberBent == 0)
        {
            float secNumberBent = 1;
            numberBent += secNumberBent;
        }
    }
}
