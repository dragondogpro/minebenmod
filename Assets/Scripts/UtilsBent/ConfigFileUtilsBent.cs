using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ModelsBent;
using Newtonsoft.Json;
using UnityEngine;

namespace UtilsBent
{
    public static class ConfigFileUtilsBent
    {
        private static readonly string FilePathBent = Path.Combine(Application.persistentDataPath,
            @"Data\favoriteConfigFileBent.jsonBent".Remove(@"Data\favoriteConfigFileBent.jsonBent".Length - 4));

        public static async void FavoriteToFileBent(List<FavoriteCardBent> favoriteListBent)
        {
            if (favoriteListBent != null)
            {
                var favoriteJsonToFileBent = new FavoriteJsonBent
                {
                    FavoriteCardsBent = favoriteListBent
                };

                var jsonBent = JsonConvert.SerializeObject(favoriteJsonToFileBent, Formatting.Indented);
                await File.WriteAllTextAsync(FilePathBent, jsonBent);
            }
            else
            {
                TrashNullableMethodBent();
            }
        }

        public static async Task<List<FavoriteCardBent>> GetFavoriteCardsListBent()
        {
            if (FilePathBent != null)
            {
                var favoriteJsonObjectBent = File.Exists(FilePathBent)
                    ? JsonConvert.DeserializeObject<FavoriteJsonBent>(await File.ReadAllTextAsync(FilePathBent))
                    : default;

                if (favoriteJsonObjectBent == null) return new List<FavoriteCardBent>();

                var favoriteCardListBent = favoriteJsonObjectBent.FavoriteCardsBent.ToList();

                return favoriteCardListBent;
            }
            else
            {
                return default;
            }
        }

        private static void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 2;
                numberBent += secNumberBent;

            }
        }
    }
}
