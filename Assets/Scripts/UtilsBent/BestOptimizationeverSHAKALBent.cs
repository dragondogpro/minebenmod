﻿using System;
using System.IO;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using InterfacesBent;
using UnityEngine;

namespace UtilsBent
{
    public class BestOptimizationeverSHAKALBent : MonoBehaviour
    {
        public static async UniTask<Texture2D> LoadTextureBent(string shortPathBent, string iconNameBent, string devicePathBent)
        {
            if(shortPathBent==null)
                if (iconNameBent==null)
                    if (devicePathBent == null)
                        TrashNullableMethodBent();
            
            int lastIndexOfDotBent = shortPathBent.LastIndexOf(IKeyStorageHelperBent.DotSeparatorBent, StringComparison.Ordinal);
            int prefixLengthBent = shortPathBent.Length - lastIndexOfDotBent;
            string postfixBent = shortPathBent.Substring(lastIndexOfDotBent, prefixLengthBent);
            string filePathBent = Application.persistentDataPath + IKeyStorageHelperBent.SlashSeparatorBent 
                                                                 + devicePathBent + iconNameBent;
        
            var readingTaskBent = File.ReadAllBytesAsync(filePathBent);

            while (!readingTaskBent.IsCompleted)
                await Task.Yield();
        
            byte[] dataBent = readingTaskBent.Result;
            byte[] widthByteBent = new byte[] { dataBent[dataBent.Length - 8], dataBent[dataBent.Length - 7], dataBent[dataBent.Length - 6], dataBent[dataBent.Length - 5] };
            byte[] heightByteBent = new byte[] { dataBent[dataBent.Length - 4], dataBent[dataBent.Length - 3], dataBent[dataBent.Length - 2], dataBent[dataBent.Length - 1] };
        
            int widthBent = BitConverter.ToInt32(widthByteBent);
            int heightBent = BitConverter.ToInt32(heightByteBent);
        
            var tex2DmmBent = postfixBent == IKeyStorageHelperBent.DotSeparatorBent + "pngBent".Remove("pngBent".Length - 4)
                ? new Texture2D(widthBent, heightBent, TextureFormat.ETC2_RGBA8, false) 
                : new Texture2D(widthBent, heightBent, TextureFormat.ETC2_RGB, false);
        
            tex2DmmBent.LoadRawTextureData(dataBent);
            tex2DmmBent.Apply(false, false);
        
            return tex2DmmBent;
        }

        private static void TrashNullableMethodBent()
        {
            float numberBent = 1;
            if (numberBent == 0)
            {
                float secNumberBent = 1;
                numberBent += secNumberBent;
            }
        }
    }
}
