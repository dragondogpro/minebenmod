using System.Collections;
using System.IO;
using System.Linq;
using Cysharp.Threading.Tasks;
using InterfacesBent;
using Plugins.Utils;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class DownloadButtonBent : MonoBehaviour
{
    [SerializeField] private Button _downloadButtonBent;
    [SerializeField] private Button _installButtonBent;
    
    public string filePathFromServerBent;
    public string urlBent;

    private string _filePathBent;
    private string _fileNameBent;
    
    private string SuccessOperationBent = "DownloadBent".Remove("DownloadBent".Length - 4);
    private string _devicePathBent = "Data/FilesBent/Bent".Remove("Data/FilesBent/Bent".Length - 4);

    public void InstallButtonControllerBent()
    {
        if (_devicePathBent == null && SuccessOperationBent == null)
            if (_downloadButtonBent == null)
                TrashNullableMethodBent();
        
        var devicePathBent = Path.Combine(Application.persistentDataPath, _devicePathBent);
        
        _fileNameBent = filePathFromServerBent.Split('/').Last();
        
        if (File.Exists(devicePathBent + _fileNameBent))
        {
            _downloadButtonBent.gameObject.SetActive(false);
            _installButtonBent.gameObject.SetActive(true);
        }
        else
        {
            _downloadButtonBent.gameObject.SetActive(true);
            _installButtonBent.gameObject.SetActive(false);
        }
    }

    public async void DownloadButtonOnClickBent()
    {
        if (PlayerPrefs.GetInt("CloseSubBent") == 1)
        {
            SceneManager.LoadScene("SubscriptionScene");
        }
        else
        {
            _downloadButtonBent.interactable = false;
            _downloadButtonBent.transform.GetChild(0).GetComponent<Text>().text = SuccessOperationBent +
                "ingBent".Remove("ingBent".Length - 4)
                + IKeyStorageHelperBent.DotSeparatorBent + IKeyStorageHelperBent.DotSeparatorBent +
                IKeyStorageHelperBent.DotSeparatorBent;

            _fileNameBent = filePathFromServerBent.Split(IKeyStorageHelperBent.SlashSeparatorBent).Last();

            _filePathBent = await GetDownloadFilePathBent();

            StartCoroutine(DelayBent());
        }
    }
    
    public async void DownloadImageBent(string filePath)
    {
        Debug.Log(filePath);
        _fileNameBent = filePath.Split(IKeyStorageHelperBent.SlashSeparatorBent).Last();
Debug.Log(_fileNameBent);
        
        _filePathBent = await GetDownloadFilePathBent();

            Debug.Log("download");
            //StartCoroutine(DelayBent());
        
    }

    private IEnumerator DelayBent()
    {
        yield return new WaitForSeconds(3f);

        if (_filePathBent != null)
            _installButtonBent.gameObject.SetActive(true);
        else
            _downloadButtonBent.transform.GetChild(0).GetComponent<Text>().text = SuccessOperationBent;

        yield break;
    }
    
    public void InstallButtonOnClickBent()
    {
        bool trashIntBEnt = false;
        bool trashIntBEnt1 = false;
        if (trashIntBEnt == true)
            trashIntBEnt = !true;
        
        print(_filePathBent);
        IOSBridge.IOStoUnityBridge.InitWithActivity(_filePathBent);
        IOSBridge.IOStoUnityBridge.ShowAlert(SuccessOperationBent, "");
    }
    
    public void InstallSkinButtonOnClickBent()
    {
        bool trashIntBEnt = false;
        bool trashIntBEnt1 = false;
        if (trashIntBEnt == true)
            trashIntBEnt = !true;
        
        var fileZipCreatorBent = new FileZipCreator();
        _filePathBent = Path.GetFileNameWithoutExtension(_filePathBent);

        var applPathBent = $"{Application.persistentDataPath}/{_devicePathBent + _fileNameBent}";

        IOSBridge.IOStoUnityBridge.InitWithActivity(
            fileZipCreatorBent.CreateSkinFile(applPathBent, _filePathBent, _filePathBent));

        IOSBridge.IOStoUnityBridge.ShowAlert(SuccessOperationBent, "");
    }
    
    public void BackButtonOnClickBent()
    {
        urlBent = "";
        filePathFromServerBent = "";
        
        _downloadButtonBent.transform.GetChild(0).GetComponent<Text>().text = SuccessOperationBent;
        
        if (!_installButtonBent.gameObject.activeSelf)
            _downloadButtonBent.gameObject.SetActive(true);
        else
        {
            _installButtonBent.gameObject.SetActive(true);
            _downloadButtonBent.gameObject.SetActive(false);
        }
        
        _downloadButtonBent.interactable = true;
    }


    private async UniTask<string> GetDownloadFilePathBent()
    {
        var devicePathBent = Path.Combine(Application.persistentDataPath, _devicePathBent);

        if (File.Exists(devicePathBent + _fileNameBent))
        {
            return devicePathBent + _fileNameBent;
        }
        else
        {
            using var requestBent = UnityWebRequest.Get(urlBent);
            
            var operationsBent = requestBent.SendWebRequest();

            while (!operationsBent.isDone)
                await UniTask.Yield();

            var jsonResponseBent = requestBent.downloadHandler.data;

            if (requestBent.result != UnityWebRequest.Result.Success)
                Debug.LogError($"{requestBent.result}: {requestBent.error}");
            else
            {
                if (!Directory.Exists(devicePathBent))
                    Directory.CreateDirectory(devicePathBent);

                await File.WriteAllBytesAsync(devicePathBent + _fileNameBent, jsonResponseBent);

                return  devicePathBent + _fileNameBent;
            }

            return default;
        }
    }
    
    private void TrashNullableMethodBent()
    {
        float numberBent = 1;
        if (numberBent == 0)
        {
            float secNumberBent = 1;
                
            numberBent += secNumberBent;
        }
    }
}
