namespace InterfacesBent
{
    public interface IPathControllerBent
    {
        public const string ContentJsonNameBent = "content.json";
        public const string ServersJsonNameBent = "servers.json";

        public const string ContentJsonPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent;

        public const string ContentMapsPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent +
                                                  ICategoryTypeHelperBent.MapsCategoryNameBent +
                                                  IKeyStorageHelperBent.SlashSeparatorBent;

        public const string ContentModsPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent +
                                                  ICategoryTypeHelperBent.ModsCategoryNameBent
                                                  + IKeyStorageHelperBent.SlashSeparatorBent;

        public const string ContentSkinsPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent +
                                                   ICategoryTypeHelperBent.SkinsCategoryNameBent +
                                                   IKeyStorageHelperBent.SlashSeparatorBent + "Images" +
                                                   IKeyStorageHelperBent.SlashSeparatorBent;

        public const string ContentServersPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent +
                                                     ICategoryTypeHelperBent.ServersCategoryNameBent +
                                                     IKeyStorageHelperBent.SlashSeparatorBent;

        public const string ContentSeedsPathBent = _stringDataBent + IKeyStorageHelperBent.SlashSeparatorBent +
                                                   ICategoryTypeHelperBent.SeedsCategoryNameBent +
                                                   IKeyStorageHelperBent.SlashSeparatorBent;


        private const string _stringDataBent = "Data";
    }
}