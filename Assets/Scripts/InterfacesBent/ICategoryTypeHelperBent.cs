namespace InterfacesBent
{
    public interface ICategoryTypeHelperBent
    {
        public const string MapsCategoryNameBent = "Maps";
        public const string ModsCategoryNameBent = "Mods";
        public const string SkinsCategoryNameBent = "Skins";
        public const string ServersCategoryNameBent = "Servers";
        public const string SeedsCategoryNameBent = "Seeds";
        public const string SkinMakerCategoryNameBent = "Skin Maker";
        public const string MemoryGameCategoryNameBent = "Memory game";
        public const string SkinRandomCategoryNameBent = "Skin Randomizer";

        public const string AllSubcategoryNameBent = "all";
        public const string NewSubcategoryNameBent = "last added";
        public const string FavoritesSubcategoryNameBent = "favorites";
    }
}