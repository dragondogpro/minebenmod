namespace InterfacesBent
{
    public interface IKeyStorageHelperBent
    {
        public const string DotSeparatorBent = ".";
        public const string SlashSeparatorBent = "/";
    }
}