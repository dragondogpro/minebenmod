using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace SubUtil
{
    public class GeneratorRenderTextureVideo : MonoBehaviour
    {
        [SerializeField] private RawImage _subVideoBent;
        [SerializeField] private VideoClip _videoClipIpadBent;
        [SerializeField] private VideoClip _videoClipIphoneBent;
        
        private Material _renderMaterialBent;
        private RenderTexture _textureBent;
        private VideoPlayer _videoPlayerBent;

        private void Awake()
        {
            _videoPlayerBent = GetComponent<VideoPlayer>();
            _videoPlayerBent.clip = Screen.width < 1500
                ? _videoClipIphoneBent
                : _videoClipIpadBent;
            ;
            _renderMaterialBent = GetComponentInChildren<MeshRenderer>().material;
        }

        private void Start()
        {
            _textureBent = new RenderTexture(1284, 2778, 24);
            _videoPlayerBent.targetTexture = _textureBent;
            _renderMaterialBent.SetTexture("_MainTex", _textureBent);
            _subVideoBent.texture = _textureBent;
        }

        private void OnApplicationFocus(bool focusBent)
        {
            Application.focusChanged += inFocus =>
            {
                if (inFocus)
                {
                    _videoPlayerBent.Play();
                }
                else
                {
                    _videoPlayerBent.Pause();
                }
            };
        }
        
        private void OnDisable()
        {
            _textureBent.DiscardContents();
        }
    }
}
